<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-index.php");
$title=__("Project - INNTERESTING");
$description=__("INNTERESTING will accelerate wind energy technology development and extend the lifetime of future wind turbine components (2030-2050) by developing innovative testing methods for prototype validation of wind components such as pitch bearings and gearboxes.");
$js[]="ekko-lightbox.min.js";
$js[]="historia.js";
$js[]="about.js";
$js[]="scroll.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
$css[]="historia.css";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="overview" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior">
  <?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
    <div class="full-container">
      <div class="container">
        <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure">
                    <img src="<?=$URL_ROOT?>assets/img/project/hero.jpg" class="">
                  </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=__("PROJECT");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
          </article>
      </div>
    </div>
    <div class="container pt-3 pt-md-3">
      <article class="pl-1 pr-1 pl-md-3 pr-md-3">
        <p class="textDestacado col-md-6 p-0 mb-2"><?=__("INNTERESTING “Innovative Future-Proof Testing Methods for Reliable Critical Components in Wind Turbines” is the name of a three-year European project that has just started on 2020 by eight partners from three European countries: ");?></p>
        <div class="row">
          <div class="col-md-6 mb-2">
            <ul class="list-unstyled list-arrow">
              <li class="pb-1"><strong>4</strong> <?=__("R&D centres (IKERLAN, VTT, VITO and KU Leuven)");?></li>
              <li class="pb-1"><strong>2</strong> <?=__("manufacturers of wind turbine components (Laulagun and Moventas)");?></li>
              <li class="pb-1"><strong>1</strong> <?=__("global player in the field of computer-aided engineering simulation (Siemens Industry Software)");?></li>
              <li class="pb-1"><strong>1</strong> <?=__("non-profit industry-driven organization (Basque Energy Cluster).");?></li>
            </ul>
          </div>
          <div class="col-md-6 mb-2">
            <p class="introDestacado"><?=__("INNTERESTING will accelerate wind energy technology development and extend the lifetime of future wind turbine components (2030-2050) by developing innovative testing methods for prototype validation of wind components such as pitch bearings and gearboxes.");?></p>
          </div>
        </div>
      </article>
    </div>
    <div class="container pt-3 pt-md-5 pb-3" id="challenge">
      <article class="pl-1 pr-1 pl-md-3 pr-md-3">
        <h2><?=__("CHALLENGEs");?></h2>
        <p class="textDestacado col-md-6 p-0 mb-4"><?=__("Post-2020 Renewable Energy Directive includes a binding renewable energy target for the EU for 2030 of 32% with an upwards revision clause by 2023. To achieve the target, it is necessary to act both on new wind turbines and on already installed ones.");?></p>
        <div class="row">
          <div class="col-md-5 mb-2">
            <div class="bg-white shadow border border-secondary p-2">
              <h3><img src="<?=$URL_ROOT?>assets/img/project/ico-turbines.svg" alt="" class="float-right" style="margin-top: -4rem;"><?=__("Increase in wind turbines size and power");?></h3>
              <p><?=__("The current market trend is to develop bigger and more powerful wind turbines with a longer lifetime");?></p>
            </div>
          </div>
          <div class="col-md-5 offset-md-1 mb-2">
            <div class="bg-white shadow border border-secondary p-2">
              <h3><img src="<?=$URL_ROOT?>assets/img/project/ico-time.svg" alt="" class="float-right" style="margin-top: -2rem;"><?=__("Installed EU wind fleet near to end-of-life");?></h3>
              <p><?=__("Main proportion of the installed EU wind fleet's lifetime (mainly onshore) will come to the end between 2020/2030");?></p>
            </div>
          </div>
        </div>
        <p class="col-md-8 mt-3 mb-3"><?=__("The current product development process (PDP) relies on a validation method that combines physical and virtual testing. While more advanced virtual modelling techniques are becoming available, it is still necessary to perform large-scale (full-size) physical tests to demonstrate reliability of new and larger wind turbine components. The full-size physical tests are the final step and the most expensive and time-consuming part of the PDP. To deal with bigger wind turbines, such critical tests require increasingly larger and more expensive test benches.");?></p>
        <div class="row">
          <div class="col-md-8 mb-2">
            <div class="bg-white shadow border border-secondary p-2">
              <h3><?=__("More demanding requirements");?></h3>
              <div class="row icosDesc">
                <div class="col">
                  <div class="ico"><img src="<?=$URL_ROOT?>assets/img/project/ico-lifetime.svg" alt=""></div>
                  <p class="data"><?=__("Longer lifetime");?></p>
                </div>
                <div class="col">
                  <div class="ico"><img src="<?=$URL_ROOT?>assets/img/project/ico-capex.svg" alt=""></div>
                  <p class="data"><?=__("Capex/opex reduction");?></p>
                </div>
                <div class="col">
                  <div class="ico"><img  src="<?=$URL_ROOT?>assets/img/project/ico-reliability.svg" alt=""></div>
                  <p class="data"><?=__("Better reliability");?></p>
                </div>
                <div class="col">
                  <div class="ico"><img  src="<?=$URL_ROOT?>assets/img/project/ico-environment.svg" alt=""></div>
                  <p class="data"><?=__("environment performance");?></p>
                </div>
                <div class="col">
                  <div class="ico"><img  src="<?=$URL_ROOT?>assets/img/project/ico-social.svg" alt=""></div>
                  <p class="data"><?=__("Social acceptance");?></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 offset-md-1 mb-2">
            <div class="bg-white shadow border border-secondary p-2">
              <h3><img src="<?=$URL_ROOT?>assets/img/project/ico-larger.svg" alt="" class="float-right"><?=__("Need of larger/more expensive test benches");?></h3>
            </div>
          </div>
        </div>
      </article>
    </div>

    <div class="full-container bg-greenLight  mt-3 mt-md-5 pt-5 pb-2" id="objectives">
      <div class="container pb-2">
      <article class="pl-1 pr-1 pl-md-3 pr-md-3">
          <h2><?=__("Objectives");?></h2>
          <div class="row">
            <div class="col-md-6">
              <ul class="list-unstyled list-arrow">
                <li class="pb-4"><strong class="text-primary">01</strong> <?=__("To develop a hybrid testing methodology able to robustly predict the expected reliability and lifetime of large wind turbine components (up to 20 MW12) without the need of performing physical tests of full-size prototypes (thus removing the need of large-scale testing infrastructures in the future).");?> </li>
                <li class="pb-4"><strong class="text-primary">02</strong> <?=__("To develop design tools integrating cutting-edge models and technologies supporting the INNTERESTING methodology. ");?> </li>
                <li class="pb-4"><strong class="text-primary">03</strong> <?=__("To bring two new ground-breaking designs of real wind turbine components to a TRL-4, proposing much higher load capacities and increased lifetime.");?> </li>
              </ul>
            </div>
            <div class="col-md-6 mb-2">
              <ul class="list-unstyled list-arrow">
                <li class="pb-4"><strong class="text-primary">04</strong> <?=__("To validate the methodology in the assessment of a novel repair solution (in process of patenting) addressed to increase lifetime extension of already installed pitch bearing.");?> </li>
                <li class="pb-4"><strong class="text-primary">05</strong> <?=__("To reduce environmental and economic impact and to improve social acceptance of the newly developed designs, concepts and testing methods.");?> </li>
                <li class="pb-4"><strong class="text-primary">06</strong> <?=__("Replication of project results to other components and sectors.");?> </li>
              </ul>
            </div>
          </div>
        </article>
      </div>
    </div>
    
    <div class="container pt-5 mb-4" id="impacts">
      <article class="pl-1 pr-1 pl-md-3 pr-md-3">
        <p class="float-md-right text-primary small"><?=__("PDP* [Product Development Process]");?></p>
        <h2><?=__("Main expected impacts ");?></h2>
        <div class="row mt-4">
          <div class="col text-center">
            <p class="data m-0"><?=__("Up to");?></p>
            <p class="number m-0"><?=__("70%");?></p>
            <p class="data mb-4 mb-md-0"><?=__("PDP* cost<br> reduction");?></p>
          </div>
          <div class="col text-center">
            <p class="data m-0"><?=__("Up to");?></p>
            <p class="number m-0"><?=__("50%");?></p>
            <p class="data mb-4 mb-md-0"><?=__("PDP* time<br>savings");?></p>
          </div>
          <div class="col text-center">
            <p class="data m-0"><?=__("Up to");?></p>
            <p class="number m-0"><?=__("25%");?></p>
            <p class="data mb-4 mb-md-0"><?=__("Lifetime<br> extension");?></p>
          </div>
          <div class="col text-center">
            <p class="data m-0"><?=__("Up to");?></p>
            <p class="number m-0"><?=__("40%");?></p>
            <p class="data"><?=__("Reliability<br> increase");?></p>
          </div>
        </div>
        <div class="row mt-5">
          <div class="bg-greenLight pt-3 pl-1 pl-md-3 pr-1 pr-md-3 w-100">
            <img src="<?=$URL_ROOT?>assets/img/project/sustainability.svg" alt="" class="float-right" style="margin-top: -7rem;">
            <p><?=__("During the project, the life cycle impact on sustainability will also be analysed on three axes: social, environmental and economic.");?></p>
            <ul class="list-tick mt-3 mt-md-4 mb-4 pl-0 pl-md-2">
              <li class="mb-2"><h4><?=__("Environmental, economic and social benefits");?></h4></li>
              <li><h4><?=__("Enhanced social acceptance and livelihood");?></h4></li>
            </ul>
          </div>
        </div>
      </article>
    </div>
    <article class="partners" id="partners">
      <div class="full-container mt-5 mb-4 bg-corporate2">
        <div class="container pt-4 pb-4">
            <div class="headerPertners pl-1 pl-md-3">
              <div class="row mt-4">
                <div class="col-md-4">
                  <h2 class="text-white"><?=__("Partners");?></h2>
                </div>
                <div class="col-md-8 text-right">
                  <img src="<?=$URL_ROOT?>assets/img/project/map.svg" alt="" class="partnerMap">
                </div>
              </div>
            </div>
        </div>
      </div>
      <div class="container pl-1 pr-1 pl-md-3 pr-md-3">
       <div class="row">
          <div class="col-md-5 mt-1 mt-md-3 mb-2">
            <a data-event="link" data-category="partner" data-label="ikerlan" href="http://www.ikerlan.es" target="_blank" class="text-secondary d-flex align-items-end mb-1 logo"><img src="<?=$URL_ROOT?>assets/img/all/logo-ikerlan-brta.svg"></a>
            <div> <a data-event="link" data-category="partner" data-label="estia" href="http://www.ikerlan.es" target="_blank" class="text-secondary d-block"><?=__("www.ikerlan.es");?></a>
              <strong class="text-primary d-block"><?=__("IKERLAN (LEADER)");?></strong>
                <?=__("<p>IKERLAN is a leading knowledge transfer technological centre providing competitive value to companies since 1974. Thanks to a unique cooperation model, which combines technology transfer activities, internal research and training of highly qualified personnel, IKERLAN is currently the trusted technological partner of major companies. With a long background in European projects, it coordinates the INNTERESTING project, where it will also contribute to the development of innovative virtual reliability methodologies, simplified testing procedures and demonstrate the feasibility of new lifetime extension concepts for pitch bearings.</p>");?>
              </div>
            </div>
            <div class="col-md-5 offset-md-1 mt-1 mt-md-3 mb-2">
              <a data-event="link" data-category="partner" data-label="kuleuven" href="http://www.mech.kuleuven.be/lmsd" target="_blank" class="text-secondary  d-flex align-items-end mb-1 logo"><img src="<?=$URL_ROOT?>assets/img/all/logo-kuleuven.png"></a>
              <div> <a data-event="link" data-category="partner" data-label="aleriontec" href="http://www.mech.kuleuven.be/lmsd" target="_blank" class="text-secondary d-block"><?=__("www.mech.kuleuven.be/lmsd");?></a>
                <strong class="text-primary d-block"><?=__("KU Leuven");?></strong>
                <?=__("<p>KU Leuven brings into INNTERESTING its expertise in flexible multibody dynamics and smart system dynamics. High fidelity modelling of bearings, model order reduction techniques, and virtual sensing (techniques and sensor sets for parameter estimation, load estimation and crack estimation) will be instrumental to advanced tools for ensuring reliability of pitch bearings, and will pave the way to extrapolations towards upscaling for hybrid accelerated testing.</p>");?>
              </div>
            </div>
          </div><!-- / .row -->
        <div class="row">
          <div class="col-md-5 mt-1 mt-md-3 mb-2">
            <a data-event="link" data-category="partner" data-label="laulagun" href="http://www.laulagun.com" target="_blank" class="text-secondary  d-flex align-items-end mb-1 logo"><img src="<?=$URL_ROOT?>assets/img/all/logo-laulagun.png"></a>
            <div> <a data-event="link" data-category="partner" data-label="estia" href="http://www.laulagun.com" target="_blank" class="text-secondary d-block"><?=__("www.laulagun.com");?></a>
              <strong class="text-primary d-block"><?=__("Laulagun");?></strong>
                <?=__("<p>Laulagun Bearings has designed, manufactured and marketed special large diameter bearings and slewing rings since 1973. Laulagun is one of the two industrial partners included in the consortium and leads the case study one, which is the pitch bearing on a wind turbine and will be used during the different work packages of the project. It also provides the patent for the case study 3 (pitch bearing lifetime extension concept), which will be led and developed by IKERLAN. Besides this, Laulagun is responsible of the work package where physical testing will be carried out for the validation of the proposed new approach.</p>");?>
              </div>
            </div>
            <div class="col-md-5 offset-md-1 mt-1 mt-md-3 mb-2">
              <a data-event="link" data-category="partner" data-label="moventas" href="http://www.moventas.com" target="_blank" class="text-secondary  d-flex align-items-end mb-1 logo"><img src="<?=$URL_ROOT?>assets/img/all/moventas-logo.svg"></a>
              <div> <a data-event="link" data-category="partner" data-label="aleriontec" href="http://www.moventas.com" target="_blank" class="text-secondary d-block"><?=__("www.moventas.com");?></a>
                <strong class="text-primary d-block"><?=__("MOVENTAS GEARS");?></strong>
                <?=__("<p>MOVENTAS GEARS OY is a Finland based company who develops and manufactures wind turbine gearboxes and related technologies and offers service for wide range of different brand products on the market. In INNTERESTING project, role of Moventas will be to develop a new onshore wind gearbox concept including novel gearing and bearing technologies. Based on this concept a virtual and actual prototype demonstrator gearbox will be build, and both will be tested.</p>");?>
              </div>
            </div>
          </div><!-- / .row -->
        <div class="row">
          <div class="col-md-5 mt-1 mt-md-3 mb-2">
            <a data-event="link" data-category="partner" data-label="siemens" href="http://www.plm.automation.siemens.com" target="_blank" class="text-secondary  d-flex align-items-end mb-1 logo"><img src="<?=$URL_ROOT?>assets/img/all/logo-siemens.png"></a>
            <div> <a data-event="link" data-category="partner" data-label="estia" href="http://www.plm.automation.siemens.com" target="_blank" class="text-secondary d-block"><?=__("www.plm.automation.siemens.com");?></a>
              <strong class="text-primary d-block"><?=__("SIEMENS INDUSTRY SOFTWARE NV");?></strong>
                <?=__("<p>SIEMENS INDUSTRY SOFTWARE NV is a leading provider of simulation and test software, hardware and services. The Simcenter portfolio includes a complete suite for test-based engineering, an integrated 3D simulation and analysis platform and a multi-disciplinary system performance simulation platform, complemented by Simcenter Engineering Services. In the framework of the INNTERESTING project, Siemens Industry Software is a key contributor with respect to development of advanced CAE techniques, in particular for multibody dynamics and acoustics. The combination with Test expertise makes SISW in the perfect spot to contribute to the development of advanced virtual sensing techniques.</p>");?>
              </div>
            </div>
            <div class="col-md-5 offset-md-1 mt-1 mt-md-3 mb-2">
              <a data-event="link" data-category="partner" data-label="vito" href="http://www.vito.be" target="_blank" class="text-secondary  d-flex align-items-end mb-1 logo"><img src="<?=$URL_ROOT?>assets/img/all/logo-vito.png"></a>
              <div> <a data-event="link" data-category="partner" data-label="aleriontec" href="http://www.vito.be" target="_blank" class="text-secondary d-inline-block"><?=__("www.vito.be");?></a> - <a data-event="link" data-category="partner" data-label="aleriontec" href="http://www.energyville.be" target="_blank" class="text-secondary d-inline-block"><?=__("www.energyville.be");?></a>
                <strong class="text-primary d-block"><?=__("VITO/EnergyVille");?></strong>
                <?=__("<p> VITO is a leading European independent research and technology organisation in the areas of cleantech and sustainable development, elaborating solutions for the large societal challenges of today. VITO provides innovative and high-quality solutions, whereby large and small companies can gain a competitive advantage, and advises industry and governments on determining their policy for the future. The business units “Sustainable Energy and Built Environment” and “Sustainable Materials” of VITO are involved in the INNTERESTING project in order to assess the sustainability of the newly to be developed components and testing methods. The unit “Sustainable Energy and Built Environment” is part of the “EnergyVille” collaboration on sustainable energy with the University of Leuven (KU Leuven), University of Hasselt (U Hasselt) and the R&D centre IMEC.</p>");?>
              </div>
            </div>
          </div><!-- / .row -->
        <div class="row">
          <div class="col-md-5 mt-1 mt-md-3 mb-2">
            <a data-event="link" data-category="partner" data-label="vtt" href="http://www.vttresearch.com" target="_blank" class="text-secondary  d-flex align-items-end mb-1 logo"><img src="<?=$URL_ROOT?>assets/img/all/logo-vtt.png"></a>
            <div> <a data-event="link" data-category="partner" data-label="estia" href="http://www.vttresearch.com" target="_blank" class="text-secondary d-block"><?=__("www.vttresearch.com");?></a>
              <strong class="text-primary d-block"><?=__("VTT");?></strong>
                <?=__("<p>VTT Technical Research Centre of Finland Ltd is a state owned non-profit company. VTT is an RTO whose activities are focused on three areas: Knowledge intensive products and services, Smart industry and energy systems, and Solutions for natural resources and environment. VTT is impact-driven and takes advantage from its wide multi-technological knowledge base to strengthen Finnish and European industrial competitiveness. VTT has a staff of 2054, net turnover in 2018 was 159,7M€ and other operational incomes 81,2M€. Over the years, VTT has gained vast experience from participation and coordination of numerous European projects including R&D Framework Programme projects and other thematic frameworks. In December 2017, VTT has been recognised with the “HR Excellence in Research” award by the European Commission.</p>");?>
              </div>
            </div>
            <div class="col-md-5 offset-md-1 mt-1 mt-md-3 mb-2">
              <a data-event="link" data-category="partner" data-label="cluster-energia" href="http://www.clusterenergia.com" target="_blank" class="text-secondary  d-flex align-items-end mb-1 logo"><img src="<?=$URL_ROOT?>assets/img/all/logo-cluster-energia.png"></a>
              <div> <a data-event="link" data-category="partner" data-label="aleriontec" href="http://www.clusterenergia.com" target="_blank" class="text-secondary d-inline-block"><?=__("www.clusterenergia.com");?></a></a>
                <strong class="text-primary d-block"><?=__("BASQUE ENERGY CLUSTER");?></strong>
                <?=__("<p>The Basque Energy Cluster is an association formed by companies, RTOs and public administration of the Basque energy sector, with over 100 organisations working on wind energy. The Basque Country is one of the largest industrial clusters in Europe, with a long tradition in the wind energy sector, making it one of the regions of the world with a highest concentration of companies working in this field. In INNTERESTING, it will be responsible for the implementation of the Communication and Dissemination Strategy.</p>");?>
              </div>
            </div>
          </div><!-- / .row -->
      </div>
    </article>

    <div class="container pt-5 mb-4 wp-structure" id="wpstructure">
      <article class="pl-1 pr-1 pl-md-3 pr-md-3">
        <h2><?=__("Wp Structure ");?></h2>
        <div class="row justify-content-center mt-3">
          <div class="col-md-5 shadow bg-white border border-secondary text-center pt-1 pr-2 pb-1 pl-2"><h5 class="m-0"><?=__("WP6. Environmental, social and economic assessment");?></h5></div>
        </div>
        <div class="bg-greenLight p-3 mt-3 mb-3">
          <div class="row">
            <div class="col shadow bg-white border border-secondary mb-1 mr-2 pt-1 pr-2 pb-1 pl-2 arrow-r">
              <h5 class="m-0"><?=__("WP1<br> Requirements and concept development");?></h5>
            </div>
            <div class="col shadow bg-white border border-secondary mb-1 mr-sm-2 pt-1 pr-2 pb-1 pl-2 no-arrow">
              <h5 class="m-0"><?=__("WP2<br> Advanced virtual testing and design tools for ensuring reliability");?></h5>
            </div>
            <div class="col shadow bg-white border border-secondary mb-1 ml-sm-2 pt-1 pr-2 pb-1 pl-2 no-arrow">
              <h5 class="m-0"><?=__("WP3<br> Simplified tailored physical testing");?></h5>
            </div>
            <div class="col shadow bg-white border border-secondary mb-1 ml-2 pt-1 pr-2 pb-1 pl-2 arrow-l">
              <h5 class="m-0"><?=__("WP5<br> Full-scale physical validation of INNTERESTING solutions");?></h5>
            </div>
          </div>
          <div class="row  justify-content-center">
            <div class="col-md-3 mt-4 shadow bg-white border border-secondary pt-1 pr-2 pb-1 pl-2 arrow-t">
              <h5 class="m-0"><?=__("WP4<br> Smart fusion towards upscalling for hybrid accelerated testing ");?></h5>
            </div>
          </div>
          <div class="row">
            <h5 class="mt-2 mb-0 text-center w-100"><?=__("Case studies (CS1 – CS2 – CS3)");?></h5>
          </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-5 mt-md-1 shadow bg-white border border-secondary text-center pt-1 pr-2 pb-1 pl-2">
              <h5 class="m-0"><?=__("WP7. Dissemination and exploitation activities");?></h5>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-5 mt-1 mt-md-3 shadow bg-white border border-secondary text-center pt-1 pr-2 pb-1 pl-2">
              <h5 class="m-0"><?=__("WP8. Project coordination and management");?></h5>
            </div>
         </div>
      </article>
    </article>

    <div class="container pt-5 mb-5" id="collaboration">
      <article class="pl-1 pr-1 pl-md-3 pr-md-3">
        <h2><?=__("Collaboration");?></h2>
        <p class="col-8 pl-0"><?=__("INNTERESTING will be in close contact with different project and initiatives that lead to effective collaboration by different means (e.g. synergies regarding wind energy technology development or life cycle analysis).");?></p>
        <p class="col-8 pl-0"><?=__("Some of the initiatives that have been identified to match this profile are:");?></p>
        <ul class="list-unstyled list-arrow">
          <li>COREWIND (<a href="http://corewind.eu/">http://corewind.eu/</a>, 2019-2023)</li>
          <li>I4OFFSHORE (<a href="https://i4offshore-project.eu/">https://i4offshore-project.eu/</a>, 2018-2023)</li>
          <li>ROMEO (<a href="https://www.romeoproject.eu/">https://www.romeoproject.eu/</a>, 2017-2022)</li>
          <li>SETWIND (<a href="https://setwind.eu/">https://setwind.eu/</a>, 2019-2022)</li>
          <li>WATEREYE (<a href="https://watereye-project.eu/">https://watereye-project.eu/</a>, 2020-2022)</li>
          <li>WINWIND (<a href="https://winwind-project.eu/">https://winwind-project.eu/</a>, 2017-2020)</li>
        </ul>
      </article>
    </div>

    <div class="container pb-2">
      <article class="banner bannerStakeholders mt-1 mb-1 p-2 p-md-4" style="background-image: url(<?=$URL_ROOT?>assets/img/all/bg-stakeholders.jpg);background-size: cover; background-position: center">
        <div class="text">
          <h3 class="text-gray text-white"><?=__("STAKEHOLDERS ADVISORY BOARD");?></h3>
          <p class="text-white"><?=__("TO SUPPORT THE CONSORTIUM AND GIVE EXPERT ADVICE");?></p>
        </div>
        <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->advisory->url?>/" class="btn btn-corporate1 shadow"><?=__("join us!");?></a>
      </article>
    </div>
  </main>
  <?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>