<?php
/* INICIALIZO VARIABLES */
$_GET['page'] = isset($_GET['page'])?(int)$_GET['page']:0;
$total_pages = 0;
$actual_page=0;
//

//------------------ sidebar en events y category pero no en detalle noticia------------------
	if ($s2=="event" OR $s2=="category"){
		$categories=$db->get_events_categories($language, $s1);
		//$latest_events=$db->get_latest_events($language);
		
	}

//------------------ consultas para cada subseccion ------------------	
	if ($s2=="category"){
		$category=$_GET["category"];
		$eventspage=6;
		$eventscount=count($db->count_category_events_list($language, $category, $s1));
		$total_pages=ceil($eventscount/$eventspage);
		if ($_GET["page"]){$actual_page=$_GET["page"];}else{$actual_page=1;}
		$limit="LIMIT ".$eventspage*($actual_page-1)." , ".$eventspage;
		$category_events = $db->get_category_events_list($language, $category, $limit, $s1);	
		if (!empty($category_events)){
			$category_events = $db->get_events_images_list($category_events);
			$category_events = $db->get_events_files_list($category_events);
			$lastArticleIndex = count($category_events)-1;
		}
		else{
			header("HTTP/1.0 404 Not Found");
			$PRE = '../../';
			unset($_GET);
			$_GET['s1'] = 'error';
			$s1='error';
			$_GET['error'] = '404';
			$error = '404';
			include("../error.php");
			exit();
		}
	}

//----------------- events home --------------------------------	
	//if(!$s2 OR $s2=='past'){
		$eventspage=6;
		$eventscount = count($db->count_front_events_list($language, $s1));
        $total_pages=ceil($eventscount/$eventspage);
		if ($_GET["page"]){$actual_page=$_GET["page"];}else{$actual_page=1;}
		$limit="LIMIT ".$eventspage*($actual_page-1)." , ".$eventspage;
		$front_events= $db->get_front_events_list($language, $limit, $s1);
		$front_events = $db->get_events_images_list($front_events);
		$front_events = $db->get_events_files_list($front_events);
		$lastArticleIndex = count($front_events)-1;
	//}
//----------------- events article sheet -----------------------	
	if($s2=="event"){
		$slug=$_GET["slug"];
		$article=$db->get_events_article($slug, $language, $s1);
		if (!empty($article)){
			$article_images_array = $db->get_events_images_list(array($article));
			$article = reset($article_images_array);
			$article_files_array = $db->get_events_files_list(array($article));
			$article = reset($article_files_array);
		}
		else{
			header("HTTP/1.0 404 Not Found");
			$PRE = '../../';
			unset($_GET);
			$_GET['s1'] = 'error';
			$s1='error';
			$_GET['error'] = '404';
			$error = '404';
			include("../error.php");
			exit();
		}
	}
?>

