<?php
/* INICIALIZO VARIABLES */
$_GET['status'] = isset($_GET['status'])?$_GET['status']:'';
$contact = array_fill_keys(array('firstname', 'email', 'dataid', 'consent', 'id', 'address', 'serial', 'requesttype'),'');
$validate = array_fill_keys(array('developerTest', 'firstname', 'email', 'consent', 'alreadysent', 'recaptcha'),0);
$error = 0;
$sent='';

if (!empty($_POST["email"]) AND $_POST["email"]==DEVELOPER_EMAIL_ADDRESS){
	$developerTest = true;
}
// ------------- AJAX --------------
$functionName=isset($_GET['fn'])?$_GET['fn']:'';
if ($functionName){
	echo call_user_func('_'.$functionName);
	exit();
}

if (!empty($_POST["send"])){
	//addToLog(' INICIO SEND CONTACT ::', 'SEND');
	$contact=array(
				"firstname"			=>	(isset($_POST["firstname"])?$_POST["firstname"]:''),
				"email"				=>	(isset($_POST["email"])?str_replace(' ', '', trim($_POST["email"])):''),
				"dataid"			=>	(isset($_POST["dataid"])?$_POST["dataid"]:''),
				"googlecookie"		=>	(isset($_POST["googlecookie"])?$_POST["googlecookie"]:''),
				"gclid"				=>	(isset($_POST["gclid"])?$_POST["gclid"]:''),
				"gmode"				=>	(isset($_POST["gmode"])?$_POST["gmode"]:''),
				"request"			=>	(isset($_POST["request"])?$_POST["request"]:''),
				"requesttype"		=>	(isset($_POST["requesttype"])?$_POST["requesttype"]:''),
				"consent"			=>	(isset($_POST["consent"])?$_POST["consent"]:''),
				"id"				=>	"",
				"address"			=>	"",
				"serial"			=>	""
	);
	
	//---------- validaciones ----------------------
	$error=0;
	
	//-- recaptcha --
	/*
	When your users submit the form where you integrated reCAPTCHA, you'll get as part of the payload a string with the name "g-recaptcha-response". 
	In order to check whether Google has verified that user, send a POST request with these parameters:
	URL: https://www.google.com/recaptcha/api/siteverify
	secret (required)	6LfqYzEUAAAAAJHhRnXBqKOsfSGwEtIWKxK10w3w
	response (required)	El valor de "g-recaptcha-response".
	remoteip	The end user's ip address.
    */
	if (empty($contact['requesttype']) AND (CAPTCHA_ACTIVE == '1' || CAPTCHA_ACTIVE == '3')){
    //addToLog("prueba recaptcha 3:".print_r($_POST, true));
		if (isset($_POST['token'])){
			$response = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.CAPTCHA3_SECRET_KEY.'&response='.$_POST['token'].'&remoteip='.$_SERVER['REMOTE_ADDR']);
            $responseDecoded  = json_decode($response);
			addToLog("prueba recaptcha 3:".print_r($responseDecoded, true)); //$error++;
			if ( $responseDecoded->success != false ){
				 if($responseDecoded->score <= CAPTCHA3_SCORE){
					 $validate["recaptcha"]=1;$error++;	
				 }
			}else{
				$validate["recaptcha"]=1;$error++;	
			}
		}
		else{
			 $validate["recaptcha"]=1;$error++;	
		}
	}
	if (!empty($contact['requesttype']) AND (CAPTCHA_ACTIVE == '2' || CAPTCHA_ACTIVE == '1') ){
		require_once($DOC_ROOT."site/includes/recaptcha-master/src/autoload.php");
		if (CAPTCHA_MODE!='CURL'){
			$recaptcha = new \ReCaptcha\ReCaptcha(CAPTCHA_SECRET_KEY);
		}
		else{
			$recaptcha = new \ReCaptcha\ReCaptcha(CAPTCHA_SECRET_KEY, new \ReCaptcha\RequestMethod\CurlPost());
		}
        $resp = $recaptcha->verify($_POST["g-recaptcha-response"], $_SERVER['REMOTE_ADDR']);
		if ($resp->isSuccess()) {
			// verified!
			// if Domain Name Validation turned off don't forget to check hostname field
			// if($resp->getHostName() === $_SERVER['SERVER_NAME']) {  }
		} else {
			//$errors = $resp->getErrorCodes();
			$validate["recaptcha"]=1;$error++;
		}
    }
    
	//-- fin recaptcha --

	if ($contact["firstname"]==""){$validate["firstname"]=1;$error++;}
	elseif (strlen(trim($contact["firstname"])) < 2 ) {$validate["firstname"]=2;$error++;}
	elseif (strlen(trim($contact["firstname"])) > 30 ) {$validate["firstname"]=2;$error++;};

	if ($contact["email"]==""){$validate["email"]=1;$error++;};
	if(!preg_match("/^([a-zA-Z0-9\._-])+@([a-zA-Z0-9\._-])+\.[a-zA-Z]{2,4}$/", $contact["email"])){$validate["email"]=1;$error++;};
	//consent
	if ($contact["consent"]!="Yes"){$validate["consent"]=1;$error++;};
			
	if ($error==0 AND !empty($developerTest)){
		$validate["developerTest"]=1;
		$error++;
		//addToLog(" DEVELOPER TEST: NOT SENDED");
	}
	
	//check if form is already sent
	if ($error == 0){
		if ($db->formContactExists($contact['dataid'])){
			unset($contact["firstname"]);
			unset($contact["email"]);
			unset($contact["dataid"]);
			unset($contact["googlecookie"]);
			unset($contact["gclid"]);
			unset($contact["gmode"]);
			//mantengo el about y el request (url inicial)
			
			$contact['dataid'] = randVal(); //nuevo id
			$validate['alreadysent'] = 1;
			$error++;
		}
		else{
			$contact['id'] = $db->formContactCreate($contact['dataid']);
		}
	}
	else{
		$contact['dataid'] = randVal(); //nuevo id
	}
	
	//---------------- email body (client & user)----------------		
	$contact_insert = array();
	$current_location = (!empty($country_data['Name'])?$country_data['Name']:$db->get_country_from_localization_code($_SESSION['lang-location']));
	$language_data = $db->get_lang_by_code($language);
	if ($error==0){
		$fields= array("{firstname}","{email}", "{consent}", "../../", "{legalinfo}", "{date}");
		$replace_fields= array($contact["firstname"],$contact["email"], $language_data['lang_name'].(($language_data['lang_name'] AND $current_location)?' - ':'').$current_location, '<p>V. '.$policies['added'].'</p>'.$policies['body'], HTTPHOST.$URL_ROOT, (string)$txt->form->legalinfo->contact, date("Y-m-d H:i:s"));
		
		$email_client=file_get_contents("$DOC_ROOT"."alerts/client/contact.html");
		$email_client=str_replace($fields, $replace_fields, $email_client);
		
		$email_user=file_get_contents("$DOC_ROOT"."alerts/user/newsletter.html");
		$email_user=str_replace($fields, $replace_fields, $email_user);
		
		//---------- email client ----------------------
		$to_email = ADMIN_EMAIL_ADDRESS_NEWSLETTER;
		if(sendEmail($to_email, 'Innteresting S.L.', CONTACT_ADMIN_EMAIL_ADDRESS, (string)$txt->form->from->name, (string)$txt->form->subject->client->contact, $email_client, true, $contact["email"], $contact['firstname'])){
			$sent = "success";
		}
		else{
			addToLog("CLIENT: FALLO AL ENVIAR EL EMAIL");
		}
		$sent = "success";
		if ($sent=="success"){
			$contact_insert = $contact;
			
			//Save Google Cookies if not empty
			if (empty($contact_insert['googlecookie'])){
				unset($contact_insert['googlecookie']);
			}
			if (empty($contact_insert['gclid'])){
				unset($contact_insert['gclid']);
			}
			if (empty($contact_insert['gmode'])){
				unset($contact_insert['gmode']);
			}
			if (empty($contact_insert['request'])){
				unset($contact_insert['request']);
			}
			//
			
			$contact_insert['phone'] = $contact_insert['phonecode'].$contact_insert['phone'];
			$contact_insert['lang_name'] = $language_data['lang_name'];
			$contact_insert['location_name'] = $current_location;
			$contact_insert['type'] = 'newsletter';
			$contact_insert['ip'] = get_ip_address();
			$contact_insert['policies_code'] = $policies['code'];
			$contact_insert['policies_revision_added'] = $policies['added'];
			$db->add_contacts(array_map('addslashes', $contact_insert));
		}
		else{
			$db->clearContact($contact['dataid']);
		}

		//---------- email user ----------------------
		if(sendEmail($contact["email"], $contact["firstname"], CONTACT_ADMIN_EMAIL_ADDRESS, (string)$txt->form->from->name, (string)$txt->form->subject->user, $email_user, true, CONTACT_REPLY_TO_EMAIL_ADDRESS, 'Innteresting S.L.')){
			$sent = "success";
		}
		else{
			addToLog("USER: FALLO AL ENVIAR EL EMAIL");
		}
		unset($contact);
	}
	
	if ($sent=="success"){ // succes, le mando a la página de enviado
		$_SESSION['newsletter'] = $contact_insert;
		session_write_close();
		header('Location:'.$URL_ROOT_BASE.'/'.$txt->form->url.'/');
		exit();
	}
}
else{
	$contact['dataid'] = randVal();
}
if ($_GET['status']=='success' AND empty($_SESSION['newsletter'])){ //Compruebo que han accedido a la página de enviado tras enviar un formulario, sino redirijo a la principal
	session_write_close();
	header('Location:'.$URL_ROOT_BASE.'/');
	exit();
}

unset($_SESSION['newsletter']);
session_write_close();

if ($_GET['status']=="success" OR $error>0){
	$js[]="scroll-to-newsletter.js";
}

// si se ha enviado correctamente, enviamos el evento
if ((strpos($_GET['status'], 'success')!==false) AND defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="sendcontactevent.js";
}
?>