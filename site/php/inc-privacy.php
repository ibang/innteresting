<?php
//----------------- privacy policies sheet -----------------------	
$policies=$db->get_policies_article($language);
if (empty($policies)){
	
	$policies = array(
	'meta-title' => (string)$txt->privacy->title,
	'meta-description' => (string)$txt->privacy->description,
	'meta-keywords' => (string)$txt->privacy->keywords,
	'headline' => (string)$txt->privacy->h1,
	'body' => (string)$txt->privacy->text,
	'added' => (string)$txt->privacy->revision,
	);
	
	/*
	header("HTTP/1.0 404 Not Found");
	$PRE = '../../';
	unset($_GET);
	$_GET['s1'] = 'error';
	$s1='error';
	$_GET['error'] = '404';
	$error = '404';
	include("../error.php");
	exit();
	*/
}
?>