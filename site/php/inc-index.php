<?php
$last_news = $db->get_news_home($language, 'news');
if (!empty($last_news)){
	$last_news = $db->get_news_images_list($last_news);
}

$last_eventos = $db->get_news_home($language, 'eventos');
if (!empty($last_eventos)){
	$last_eventos = $db->get_news_images_list($last_eventos);
}

$last_newsevents = array();
$last_ind = max(array(count($last_news), count($last_eventos)));
if ($last_ind){
	for ($i=0;$i<$last_ind;$i++){
		if (!empty($last_news[$i])){
			$last_newsevents[] = $last_news[$i];
		}
		if (!empty($last_eventos[$i])){
			$last_newsevents[] = $last_eventos[$i];
		}
	}
}	
?>