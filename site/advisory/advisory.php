<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-index.php");
require_once("../../site/php/inc-privacy.php");
require_once("../../site/php/inc-advisory.php");
$title=__("Advisory group - INNTERESTING");
$description=__("INNTERESTING research and innovation activities will be driven by the opinion of stakeholders involved providing expert advice to ensure consistency in the project outcomes");
$js[]="ekko-lightbox.min.js";
$js[]="historia.js";
$js[]="about.js";
$js[]="scroll.js";
$js[]="advisory-joingroup-select.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
if(CAPTCHA_ACTIVE == '3' OR (CAPTCHA_ACTIVE == '1' AND (!isset($validate["recaptcha"]) OR $validate["recaptcha"]!=1) )){
	$js_lang[]="recaptcha3.js";
}
$css[]="historia.css";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="overview" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior">
  <?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
    <div class="full-container">
      <div class="container">
        <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure">
                    <img src="<?=$URL_ROOT?>assets/img/stakeholders/hero.jpg" class="">
                  </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=__("STAKEHOLDERS ADVISORY BOARD");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
          </article>
      </div>
    </div>
    <div class="container pt-3 pt-md-3">
      <article class="pl-1 pr-1 pl-md-3 pr-md-3">
        <p class="textDestacado col-md-6 p-0 mb-2"><?=__("In order to maximize the success potential of the project, INNTERESTING research and innovation activities will be driven by the opinion of stakeholders involved in the Stakeholder Advisory Board, which will be the main group supporting the consortium in providing expert advice to ensure consistency in the project outcomes in accordance with market expectations. The advisory board will be divided into two main groups:");?></p>
        <div class="row">
          <div class="col-md-6 mt-3 mb-2">
            <figure class="figure">
              <img src="<?=$URL_ROOT?>assets/img/stakeholders/sustanability-advisory-group.jpg" class="">
            </figure>
            <ul class="list-arrow p-0 mr-md-4">
              <li>
              <h3><?=__("Sustainability Advisory Group");?></h3>
              <p class="mb-2"><?=__("The Sustainability Advisory Group will mostly help on defining necessary requirements for future wind turbines, to improve the social acceptance and to lower the environmental impacts of newly developed wind energy technology.");?></p>
              <a id="btn-sustainability-advisory" href="#form" class="btn btn-corporate1 arrow-down shadow"><?=__("Join the group");?></a></li>
            </ul>
          </div>
          <div class="col-md-6 mt-3 mb-2">
            <figure class="figure">
              <img src="<?=$URL_ROOT?>assets/img/stakeholders/technical-advisory-group.jpg" class="">
            </figure>
            <ul class="list-arrow p-0 mr-md-4">
              <li>
              <h3><?=__("Technical Advisory Group");?></h3>
              <p class="mb-2"><?=__("The Technical Advisory Group will be formed by a limited number of experts that will overview the technical advancement of the project and will support the consortium on its development.");?></p>
              <a id="btn-technical-advisory"  href="#form" class="btn btn-corporate1 arrow-down shadow"><?=__("Join the group");?></a></li>
          </div>
        </div>
      </article>
    </div>
    <div class="full-container bg-gray mt-4" id="form">
      <div class="container pt-5 pb-5">
       <article class="pl-1 pr-1 pl-md-3 pr-md-3">
         <h2><?=__("BECOME A PART OF IT!");?></h2>
         <p class="mb-2"><?=__("Join the INNTERESTING Sustainability or Technical Advisory Group");?></p>
         <div class="row">
          <div class="col-md-7 mb-3">
            <?require("{$DOC_ROOT}site/includes/widget-advisoryform.php")?>
          </div>
          <div class="col-md-3 offset-md-2 text-center">
            <img src="<?=$URL_ROOT?>assets/img/stakeholders/logo-ge.png" class="mr-3 mb-3"> <img src="<?=$URL_ROOT?>assets/img/stakeholders/logo-lindo.png" class="mr-3 mb-3"> <img src="<?=$URL_ROOT?>assets/img/stakeholders/logo-catapult.png" class="mr-3 mb-3"> <img src="<?=$URL_ROOT?>assets/img/stakeholders/logo-siemens.png" class="mr-3 mb-3"> <img src="<?=$URL_ROOT?>assets/img/stakeholders/logo-dnv.png" class="mr-3 mb-2"> <img src="<?=$URL_ROOT?>assets/img/stakeholders/logo-iberdrola.png" class="mr-3 mb-3"> <img src="<?=$URL_ROOT?>assets/img/stakeholders/logo-vestas.png" class="mr-3 mb-3"> <img src="<?=$URL_ROOT?>assets/img/stakeholders/logo-sirris-owi.png" class="mr-3 mb-3">
          </div>
        </div>
      </article>
      </div>
    </div>
    <div class="container pb-2">
      <article class="banner bannerStakeholders mt-1 mb-1 p-2 p-md-4" style="background-image: url(<?=$URL_ROOT?>assets/img/stakeholders/banner-technological.jpg);background-size: cover; background-position: center">
        <div class="text">
          <h3 class="text-gray text-white"><?=__("KNOW MORE ABOUT THE TECHNOLOGICAL APPROACH");?></h3>
          <p class="text-white"><?=__("Discover the key phase of the project, where we will lead the way.");?></p>
        </div>
        <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->technological->url?>/" class="btn btn-corporate1 shadow"><?=__("TECHNOLOGICAL <br>
APPROACH");?></a>
      </article>
    </div>
  </main>
  <?require("{$DOC_ROOT}site/includes/footer.php")?>

  <script></script>
</body>
</html>