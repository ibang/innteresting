<?
require_once("../php/init.php");
require_once("../site/php/inc-functions.php");
require_once("../site/php/inc-index.php");
require_once("../site/php/inc-newsletter.php");
$title=__("Home - INNTERESTING");
$description=__("INNTERESTING will accelerate wind energy technology development and extend the lifetime of future wind turbine components");
$js[]="ekko-lightbox.min.js";
$js[]="lightbox.js";
$js[]="about.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
if(CAPTCHA_ACTIVE == '3' OR (CAPTCHA_ACTIVE == '1' AND (!isset($validate["recaptcha"]) OR $validate["recaptcha"]!=1) )){
	$js_lang[]="recaptcha3.js";
}
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="home" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?>">
<?require("{$DOC_ROOT}site/includes/header.php")?>
    <main role="main">
    <div class="full-container">
        <div class="container">
          <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure">
                    <img src="<?=$URL_ROOT?>assets/img/home/hero.jpg" class="">
                    <figcaption class="figure-caption"><?=__("source: HINE");?></figcaption>
                  </figure>
                  <div class="text-box pr-2">
                    <h1><span class="small">INNTERESTING </span><?=__("Innovative Future-Proof Testing Methods for Reliable Critical Components in Wind Turbines");?></h1>
                    <p class="contact"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->contact->url?>/" class=" pl-1 pl-md-4"><?=__("Contact");?></a></p>
                    <p class="scroll data"><?=__("Scroll to see more");?></p>
                  </div>
                </div>
            </div>
          </article>
      </div>
    </div>
    
    <!-- <div class="full-container mt-lg-3 mb-lg-3">
    </div> -->
    <div class="full-container">
      <div class="container pt-3 pb-3 pt-md-5 pb-md-5">
        <article class="pl-1 pr-1 pl-md-3 pr-md-3">
            <h2 class=""><?=__("Project");?></h2>
            
            <div class="col-lg-12 pr-3 mb-4">
                <p class="introDestacado p-0"><?=__("INNTERESTING will accelerate wind energy technology development and extend the lifetime of future wind turbine components (2030-2050) by developing innovative testing methods for prototype validation of wind components such as pitch bearings and gearboxes.");?></p>
            </div>
            <div class="col-leg-12 embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/aU0jipcgpbI?rel=0,autoplay=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="row numbers text-center mt-2 mb-4">
                <div class="col-6 col-md-3">
                  <span class="number">3</span>
                  <span class="data"><?=__("COUNTRIES");?></span>
                </div>
                <div class="col-6 col-md-3">
                  <span class="number">4.8</span>
                  <span class="data"><?=__("M€ BUDGET");?></span>
                </div>
                <div class="col-6 col-md-3">
                  <span class="number">8</span>
                  <span class="data"><?=__("PARTners");?></span>
                </div>
                <div class="col-6 col-md-3">
                  <span class="number">36</span>
                  <span class="data"><?=__("MONTHS");?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <iframe width="100%" height="350"  src="https://www.youtube.com/embed/sBpHiHjtRkU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  
              
                </div>
                <div class="col-lg-6 pl-2 pt-2">
                    <p><?=__("INNTERESTING project pursues the validation of the developments through 3 different case studies dealing with innovative pitch bearing concept (CS1), new gearbox component design (CS2) and innovative repairing solution for lifetime extension of pitch bearings (CS3). ");?></p>
                    <p><?=__("In order to maximise the innovation potential of INNTERESTING technology developments, without losing the potential of lowering environmental, social and economic impacts, a life cycle sustainability assessment (LCSA) will be performed iteratively throughout the project.");?></p>
                    
                    <div class="mt-4">
                        <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->project->url?>/" class="btn btn-corporate1 shadow"><?=__("The Project");?></a>
                    </div>
                
                </div>
                
            </div>
            
            
          
            
        </article>
      </div>
    </div>
   
    
    <div class="full-container bg-greenLight">
          <div class="container pt-5">
            <article class="project pb-1 pl-1 pr-1 pb-md-3 pl-md-3 pr-md-3">
              <div class="row">
                <div class="col-md-3 mb-4">
                  <h2><?=__("PARTNERS");?></h2>
                  <p><?=__("Meet the partners participating in the project.");?></p>
                  <a href="<?=$URL_ROOT_BASE?>/<?=$txt->project->url?>/#partners" class="btn btn-corporate1 shadow"><?=__("the partners");?></a>
                </div>
                <div class="col-md-6 offset-md-1">
                  <ul class="listIcos list-unstyled flex-wrap">
                    <li class="col-md-6"><a data-event="link" data-category="partner" data-label="ikerlan" href="http://www.ikerlan.es" target="_blank" class="text-secondary d-flex align-items-end mb-1 logo"><img src="<?=$URL_ROOT?>assets/img/all/logo-ikerlan-brta.svg"></a></li>
                    <li class="col-md-6"><a data-event="link" data-category="partner" data-label="kuleuven" href="http://www.mech.kuleuven.be/lmsd" target="_blank" class="text-secondary  d-flex align-items-end mb-1 logo"><img src="<?=$URL_ROOT?>assets/img/all/logo-kuleuven.png"></a></li>
                    <li class="col-md-6"><a data-event="link" data-category="partner" data-label="laulagun" href="http://www.laulagun.com" target="_blank" class="text-secondary  d-flex align-items-end mb-1 logo"><img src="<?=$URL_ROOT?>assets/img/all/logo-laulagun.png"></a></li>
                    <li class="col-md-6"><a data-event="link" data-category="partner" data-label="moventas" href="http://www.moventas.com" target="_blank" class="text-secondary  d-flex align-items-end mb-1 logo"><img src="<?=$URL_ROOT?>assets/img/all/moventas-logo.svg"></a></li>
                    <li class="col-md-6"><a data-event="link" data-category="partner" data-label="siemens" href="http://www.plm.automation.siemens.com" target="_blank" class="text-secondary  d-flex align-items-end mb-1 logo"><img src="<?=$URL_ROOT?>assets/img/all/logo-siemens.png"></a></li>
                    <li class="col-md-6"><a data-event="link" data-category="partner" data-label="vito" href="http://www.vito.be" target="_blank" class="text-secondary  d-flex align-items-end mb-1 logo"><img src="<?=$URL_ROOT?>assets/img/all/logo-vito.png"></a></li>
                    <li class="col-md-6"><a data-event="link" data-category="partner" data-label="vtt" href="http://www.vttresearch.com" target="_blank" class="text-secondary  d-flex align-items-end mb-1 logo"><img src="<?=$URL_ROOT?>assets/img/all/logo-vtt.png"></a></li>
                    <li class="col-md-6"><a data-event="link" data-category="partner" data-label="cluster-energia" href="http://www.clusterenergia.com" target="_blank" class="text-secondary  d-flex align-items-end mb-1 logo"><img src="<?=$URL_ROOT?>assets/img/all/logo-cluster-energia.png"></a></li>
                  </ul>
                </div>
              </div>
            </article>
          </div>
    </div>
    <div class="container">
      <article class="banner bannerStakeholders mt-1 mb-1 p-2 p-md-4" style="background-image: url(<?=$URL_ROOT?>assets/img/all/bg-stakeholders.jpg);background-size: cover; background-position: center">
        <div class="text">
          <h3 class="text-gray text-white"><?=__("STAKEHOLDERS ADVISORY BOARD");?></h3>
          <p class="text-white"><?=__("TO SUPPORT THE CONSORTIUM AND GIVE EXPERT ADVICE");?></p>
        </div>
        <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->advisory->url?>/" class="btn btn-corporate1 shadow"><?=__("join us!");?></a>
      </article>
    </div>
    <div class="full-container">
          <div class="container">
            <article class="newslist pb-5 pt-5 pl-1 pr-1 pl-md-3 pr-md-3"  style="background-image: url(<?=$URL_ROOT?>assets/img/home/bg-news.jpg);background-size: cover; background-position: center; background-repeat: no-repeat;">
              <div class="row">
                <div class="col-md-3 mb-3">
                  <h2><?=__("SECTORIAL NEWS & PROJECT EVENTS");?></h2>
                  <p><?=__("Keep updated with our latest news section.");?></p>
                  <a href="<?=$URL_ROOT_BASE?>/<?=$txt->sectorialnews->url?>/" class="btn btn-corporate1 shadow"><?=__("All news");?></a>
                </div>
                <div class="col-md-4">
                  <div id='scoopit-container-3997583'>
                      <a href='https://www.scoop.it/topic/eolicaoffshore'>Eólica + Offshore</a>
                    </div>
                    <script type="text/javascript">
                     var scitOpt = {
                      displayInsight: "no",
                      removeHeadingInHtml: "yes",
                      removeScoopItFont: "yes",
                      nbPostPerPage: 2
                     };
                    </script>
                    <script type='text/javascript' src='https://www.scoop.it/embed-full-topic/3997583.js' ></script>
                </div>
                <div class="col-md-3 offset-md-1">
                  <a class="twitter-timeline" data-width="320" data-height="1100" href="https://twitter.com/INNTERESTING_eu?ref_src=twsrc%5Etfw">Tweets by INNTERESTING_eu</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
              </div>
            </article>
          </div>
    </div>

    </main>
    <div class="full-container">
        <div class="container">
            <?require("{$DOC_ROOT}site/includes/widget-newsletterform.php")?>
        </div>
    </div>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>