<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-events.php");
$title=htmlspecialchars(strip_tags($article["meta-title"]));
$description=htmlspecialchars(strip_tags($article["meta-description"]));
$keywords=htmlspecialchars(strip_tags($article["meta-keywords"]));
$shareimage = 'uploads/events/'.$article["image1"];
$js[]="jquery.prettySocial.min.js";
$js[]="share.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="events" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?>">
	<?require("{$DOC_ROOT}site/includes/header.php")?>
	<main>
	<div class="full-container hidden-xs">
		<div class="container">
			<article class="hero">
				<div class="row w-100 no-gutters">
					<div class="col-md-4">
                        <div class="">
						  <?if(!empty($article["image1"])){?>
							<img class="w-100 col-md-12 col-sm-6 pdr0" src="<?=$URL_ROOT?>uploads/events/<?=$article["image1"]?>" alt="<?=$article["alt-image1"];?>" />
                            <div class="keep">
                                <h2 class="events ml-2 mr-2">EVENTS</h2>
                                <p class="ml-2 mr-2 ">Keep up to date with our latest events and events.</p>
                                <p class="back float-left ml-2 mb-2"><a href="<?=$URL_ROOT_BASE?>/events/"> <i class="fa fa-angle-left"></i> <?=__("Back to Events");?></a></p>
                            </div>
							<?}?>
                        </div>
					</div>
                    <div class="col-md-8 bg-gray">
					<article class="main-events ml-3 mr-3">
						<div class="meta clearfix mt-4">
							<h1 class="text-primary mb-1"><?=$article["headline"]?></h1>
							<p class="date d-inline-block text-primary"><span <?if($article['type']=='eventos' AND !empty($article["date"])){?>class="date2"<?}?>><?=parsedate($article["date"],$language);?></span> <?if($article['type']=='eventos' AND !empty($article["location"])){?><span class="place"><?=$article["location"];?></span> <?}?></p>
							<p class="social d-inline-block"> | <span class="hidden-xs sharelink"><?=__("Share");?></span> <a href="#" data-type="facebook" data-url="<?=HTTPHOST;?><?=$URL_ROOT_BASE?>/<?=$txt->{$article['type']}->url?>/<?=$article["slug"]?>/" data-title="<?=$title?>" data-description="<?=$description?>" data-media="<?=HTTPHOST;?><?=$URL_ROOT?>uploads/events/<?=$article["image1"]?>" class="share fa fa-facebook"></a> <a href="#" data-type="twitter" data-url="<?=HTTPHOST;?><?=$URL_ROOT_BASE?>/<?=$txt->{$article['type']}->url?>/<?=$article["slug"]?>/" data-description="<?=$title?>" data-via="innteresting" class="share fa fa-twitter"></a> <a href="#" data-type="linkedin" data-url="<?=HTTPHOST;?><?=$URL_ROOT_BASE?>/<?=$txt->{$article['type']}->url?>/<?=$article["slug"]?>/" data-title="<?=$title?>" data-description="<?=$description?>" data-via="innteresting" data-media="<?=HTTPHOST;?><?=$URL_ROOT?>uploads/events/<?=$article["image1"]?>" class="share fa fa-linkedin"></a></p>
						</div>
						<?if(!empty($article["intro"])){?>
						<p class="summary"><?=$article["intro"]?></p>
						<?}?>
						<div class="extras">
							<?if(!empty($article['file1'])){?>
								<a target="_blank" class="descarga" href="<?=$URL_ROOT?>uploads/events/<?=$article["file1"]?>"><?=$article["link-file1"]?></a>
							<?}?>
							<?if(!empty($article['files'])){?>
								<?foreach($article['files'] as $current){?>
								<a target="_blank" class="descarga" href="<?=$URL_ROOT?>uploads/events/<?=$current["file"]?>"><?=$current["link-file"]?></a>
								<?}?>
							<?}?>
							<?if(!empty($article['link'])){?>
								<a target="_blank" class="link" href="<?=$article["link"]?>"><?=$article["link-text"]?></a>
							<?}?>
						</div>
						<div class="desc">
							<?=transformAllInLocalizedURL($article["body"])?>
						</div>
					</article>
				</div>
				</div>
			</article>
		</div>
	</div>
</main>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>