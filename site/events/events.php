<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-events.php");
$title = __("events - INNTERESTING");
$description = __("Keep up to date with our latest events and events.");
$keywords = __("events, events,");
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="events" class="interior <?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?>">
	<?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
	<div class="full-container">
		<div class="container">
			 <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure">
                    <img src="<?=$URL_ROOT?>assets/img/events/hero.jpg" class="">
                        <figcaption class="figure-caption"><?=__("source: LAULAGUN");?></figcaption>
                 </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=__("Events");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
          </article>
		</div>
	</div>
    <div class="full-container bg-gray pt-3 pt-md-5 pb-3">
    	<div class="container box">
    		<section class="ml-0 ml-md-3 mr-3">
				
				<?if(!empty($front_events)){?>
				<div class="row">
					<?foreach ($front_events as $ind => $article){?>
						<div class="col-sm-6">
							<article class="item-events bg-white shadow">
								<div class="object">
									<?if ($article["image1"]){?>
										<p class="photo"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->{$article['type']}->url?>/<?=$article["slug"]?>/"><img class="img-responsive" src="<?=$URL_ROOT?>uploads/events/<?=$article["image1"]?>" alt="<?=($article["alt-image1"]?htmlspecialchars(strip_tags($article["alt-image1"])):str_replace("\"","'", $article["headline"]))?>" /></a></p>
									<?}?>
									<div class="content pt-3 pr-3 pl-3">
										<p class="date"><span <?if($article['type']=='events' AND !empty($article["date"])){?>class="date2"<?}?>><?=parsedate($article["date"],$language);?></span><?if($article['type']=='events' AND !empty($article["location"])){?> <span class="place"><?=$article["location"];?></span><?}?></p>
										<h3 class="title"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->{$article['type']}->url?>/<?=$article["slug"]?>/"><?=$article["headline"]?></a></h3>
									</div>
								</div>
								<p class="summary hidden-xs pr-3 pb-3 pl-3"><?=$article["intro"]?> <a class="link text-orange" href="<?=$URL_ROOT_BASE?>/<?=$txt->{$article['type']}->url?>/<?=$article["slug"]?>/"><?=__("Read more");?> <i class="fa fa-angle-right"></i></a></p>
							</article>
						</div>
					<?if(($ind%2)==1 AND $ind!=$lastArticleIndex){?>
				</div>
				<div class="row">
						<?}?>
					<?}?>
				</div>
				<?}?>
				<?if ($actual_page>1 or $actual_page<$total_pages){?>
				<div class="row">
					<div class="col-md-12">
						<nav>
							<ul class="pager list-unstyled text-center mt-3 mt-md-4">
							<? if ($actual_page>1){?><li class="previous"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->{$s1}->url?>/<?if($s2=='past'){?><?=$txt->{$s1}->{$s2}->url;?>/<?}?><?if (($actual_page-1)>1){echo ($actual_page-1).'/';};?>" role="button"><i class="fa fa-angle-left"></i> <?=__("Previous");?></a></li><?}?>
							<? if ($actual_page<$total_pages){?><li class="next"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->{$s1}->url?>/<?if($s2=='past'){?><?=$txt->{$s1}->{$s2}->url;?>/<?}?><?=($actual_page+1);?>/" role="button"><?=__("Next");?> <i class="fa fa-angle-right"></i></a></li><?}?>
							</ul>
						</nav>
					</div>
				</div>
				<?}?>
			</section>
		</div> <!-- /.container -->
	</div> <!-- /.full-container -->
  </main>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>