<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-index.php");
require_once("../../site/php/inc-privacy.php");
$title=__("Related Projects - INNTERESTING");
$description=__("INNTERESTING research and innovation activities will be driven by the opinion of stakeholders involved providing expert advice to ensure consistency in the project outcomes");
$js[]="ekko-lightbox.min.js";
$js[]="historia.js";
$js[]="about.js";
$js[]="scroll.js";
$js[]="advisory-joingroup-select.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
if(CAPTCHA_ACTIVE == '3' OR (CAPTCHA_ACTIVE == '1' AND (!isset($validate["recaptcha"]) OR $validate["recaptcha"]!=1) )){
	$js_lang[]="recaptcha3.js";
}
$css[]="historia.css";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="overview" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior">
  <?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
    <div class="full-container">
      <div class="container">
        <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure">
                    <img src="<?=$URL_ROOT?>assets/img/relatedprojects/hero.jpg" class="">
                  </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=__("Related Projects");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
          </article>
      </div>
    </div>
    <div class="container pt-3 pt-md-3">
      <article class="pl-1 pr-1 pl-md-3 pr-md-3">
            <ul class="row p-0 mb-4">
              <li class="col-lg-6 d-table-cell mb-2">
                <div class="row bg-white shadow pt-3 pb-2 h-100 mr-md-1 align-baseline">
                  <div class="col-lg-4 ml-md-1">
                    <img src="<?=$URL_ROOT?>assets/img/relatedprojects/logo-corewind.png" width="150" class="">
                    <p class="data mt-2">2019 - 2023</p>
                  </div>
                  <div class="col-lg-7">
                    <p class="mb-2"><?=__("The <span class='bold'>COREWIND</span> project aims to achieve significant cost reductions and enhance performance of floating wind technology through the research and optimization of mooring and anchoring systems and dynamic cables.");?></p>
                  </div>
                  <a href="http://corewind.eu/" target="_blank" title="visit COREWIND project" class="row btn btn-corporate1 arrow shadow flex-end"><?=__("Visit the project");?></a>
                </div>
              </li>
              <li class="col-lg-6 d-table-cell mb-2">
                <div class="row bg-white shadow pt-3 pb-2">
                  <div class="col-lg-4 ml-md-1">
                    <img src="<?=$URL_ROOT?>assets/img/relatedprojects/logo-romeo.png" width="170" class="">
                    <p class="data mt-2">2017 - 2022</p>
                  </div>
                  <div class="col-lg-7">
                    <p class="mb-2"><?=__("<span class='bold'>ROMEO</span> (Reliable O&M decision tools and strategies for high LCOE reduction on Offshore wind) seeks to reduce offshore O&M costs through the development of advanced monitoring systems and strategies, aiming to move from corrective and calendar based maintenance to a condition based maintenance, through analysing the real behaviour of the main components of wind turbines.");?></p>
                  </div>
                  <a href="https://www.romeoproject.eu/" target="_blank" title="visit ROMEO project" class="row btn btn-corporate1 arrow shadow flex-end"><?=__("Visit the project");?></a>
                </div>
              </li>
              <li class="col-lg-6 d-table-cell mb-2">
                <div class="row bg-white shadow pt-3 pb-2 h-100 mr-md-1">
                  <div class="col-lg-4 ml-md-1">
                    <img src="<?=$URL_ROOT?>assets/img/relatedprojects/logo-setwind.png" width="150" class="">
                    <p class="data mt-2">2019 - 2022</p>
                  </div>
                  <div class="col-lg-7">
                    <p class="mb-2"><?=__("The <span class='bold'>SETWIND</span> project supports the implementation of the SET-Plan Implementation Plan for Offshore Wind. It will update and work with the Implementation Plan to maintain it as a dynamic reference point for offshore wind energy research and innovation; it will monitor and report on progress towards the Implementation Plan targets of 1,090 million € to be invested in R&I in the offshore sector until 2030; it will strengthen policy coordination in European offshore wind energy R&I policy by supporting the work of the SET-Plan Implementation Group for Offshore Wind; and it will facilitate a breakthrough in the coordination across borders of nationally funded R&I projects.");?></p>
                  </div>
                  <a href="https://setwind.eu/" target="_blank" title="visit SETWIND project" class="row btn btn-corporate1 arrow shadow flex-end"><?=__("Visit the project");?></a>
                </div>
              </li>
              <li class="col-lg-6 d-table-cell mb-2">
                <div class="row bg-white shadow pt-3 pb-2 h-100">
                  <div class="col-lg-4 ml-md-1">
                    <img src="<?=$URL_ROOT?>assets/img/relatedprojects/logo-watereye.png" width="170" class="">
                    <p class="data mt-2">2019 - 2022</p>
                  </div>
                  <div class="col-lg-7">
                    <p class="mb-2"><?=__("The highest critical cost related to O&M in offshore wind turbines is caused by structural failures that mainly occur due to corrosion. The <span class='bold'>WATEREYE</span> project aims to reduce O&M cost, by developing a solution to monitor corrosion in the structure level that will allow predictive maintenance, with the objective of preventing major failures or even the breakdown. This objective will be reached through monitoring critical points of WT, analysing the data to generate a diagnosis and a prognosis maintenance strategy. <span class='bold'>WATEREYE</span> addresses the whole study of corrosion and its consequences in the different parts of a WT structure. The project develops a monitoring system capable of remotely estimating the corrosion level in exact critical locations of WT structure as a supporting tool for predictive maintenance. In addition, <span class='bold'>WATEREYE</span> will develop technologies for data analytics, modelling, and diagnosis for WT as well as WF O&M advanced control strategies. These technologies will contribute to significant OPEX reductions and to improve the efficiency and profitability of offshore energy resources.");?></p>
                  </div>
                  <a href="https://watereye-project.eu/about/" target="_blank" title="visit WATEREYE project" class="row btn btn-corporate1 arrow shadow flex-end"><?=__("Visit the project");?></a>
                </div>
              </li>
              <li class="col-lg-6 d-table-cell mb-2">
                <div class="row bg-white shadow pt-3 pb-2 h-100 mr-md-1">
                  <div class="col-lg-4 ml-md-1">
                    <img src="<?=$URL_ROOT?>assets/img/relatedprojects/logo-totalcontrol.jpeg" width="170" class="">
                    <p class="data mt-2">2018 - 2021</p>
                  </div>
                  <div class="col-lg-7">
                    <p class="mb-2"><?=__("The ambition of the <span class='bold'>TotalControl</span> project is to develop the next generation of wind power plant (WPP) control tools, improving both WPP control itself and the collaboration between wind turbine (WT) and WPP control. To do this, TotalControl will use high-fidelity simulation and design environments that include detailed time resolved flow field modelling, nonlinear flexible multi-body representations of turbines, and detailed power grid models.");?></p>
                  </div>
                  <a href="https://www.totalcontrolproject.eu/" target="_blank" title="visit TOTAL CONTROL project" class="row btn btn-corporate1 arrow shadow flex-end"><?=__("Visit the project");?></a>
                </div>
              </li>
              <li class="col-lg-6 d-table-cell mb-2">
                <div class="row bg-white shadow pt-3 pb-2 h-100">
                  <div class="col-lg-4 ml-md-1">
                    <img src="<?=$URL_ROOT?>assets/img/relatedprojects/logo-flotant.png" width="150" class="">
                    <p class="data mt-2">2019 - 2022</p>
                  </div>
                  <div class="col-lg-7">
                    <p class="mb-2"><?=__("Installing, operating and maintaining marine wind turbines becomes more difficult and more expensive as water depth increases. The EU-funded <span class='bold'>FLOTANT</span> project aims to build and demonstrate a scale model of an offshore floating platform to support a wind turbine in deep waters. The newly designed components will include a hybrid concrete-plastic platform, optimised mooring and anchoring systems, high-performance lightweight power cables, and a high-tech monitoring system for operation and maintenance. The project will further assess the technological, economic, environmental and social impact of the floating platform. Ultimately, it will contribute to the advancement of renewable technologies, and is estimated to reduce future capital and operational expenses by more than 50%.");?></p>
                  </div>
                  <a href="https://flotantproject.eu/" target="_blank" title="visit FLOTANT project" class="row btn btn-corporate1 arrow shadow flex-end"><?=__("Visit the project");?></a>
                </div>
              </li>
            </ul>
      </article>
    </div>

    <div class="container pb-2">
      <article class="banner mt-1 mb-3 p-2 p-md-4" style="background-image: url(<?=$URL_ROOT?>assets/img/technological/banner-events.jpg);background-size: cover; background-position: center">
        <div class="text">
          <h3 class="text-gray text-white"><?=__("DISCOVER THE PAST AND FUTURE EVENTS");?></h3>
          <p class="text-white"><?=__("Discover the evolution and all off the latest information about the Docc-off project.");?></p>
        </div>
        <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->events->url?>/" class="btn btn-corporate1 shadow"><?=__("EVENTS");?></a>
      </article>
    </div>
  </main>
  <?require("{$DOC_ROOT}site/includes/footer.php")?>

  <script></script>
</body>
</html>