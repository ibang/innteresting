<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-index.php");
$title=__("News - INNTERESTING");
$description=__("All the curated information about the wind energy sector");
$js[]="ekko-lightbox.min.js";
$js[]="historia.js";
$js[]="about.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
$css[]="historia.css";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="nosotros" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior">
  <?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
    <div class="full-container">
      <div class="container">
        <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure">
                    <img src="<?=$URL_ROOT?>assets/img/sectorialnews/hero.jpg" class="">
                       <figcaption class="figure-caption"><?=__("source: MOVENTAS");?></figcaption>
                 </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=__("Sectorial news");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
          </article>
      </div>
    </div>
    <div class="container">
      <article class="scoopit pl-1 pr-1 pl-md-3 pr-md-3">
        <div class="row">
          <div class="col-md-8">
            <div id='scoopit-container-3997583'>
                <a href='https://www.scoop.it/topic/eolicaoffshore'>Eólica + Offshore</a>
              </div>
              <script type="text/javascript">
               var scitOpt = {
                displayInsight: "no",
                removeHeadingInHtml: "yes",
                removeScoopItFont: "yes",
                nbPostPerPage: 8
               };
              </script>
              <script type='text/javascript' src='https://www.scoop.it/embed-full-topic/3997583.js' ></script>
          </div>
           <div class="col-md-3 offset-md-1">
                  <a class="twitter-timeline" data-width="320" data-height="1100" href="https://twitter.com/INNTERESTING_eu?ref_src=twsrc%5Etfw">Tweets by INNTERESTING_eu</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
            </div>
          </div>
      </article>
    </div>
  </main>
  <?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>