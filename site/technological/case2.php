<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-index.php");
$title=__("Case study 2 - INNTERESTING");
$description=__("Design and verify a next generation wind turbine main gearbox GBX concept including novel gearing and bearing systems to increase torque density and reliably.");
$js[]="ekko-lightbox.min.js";
$js[]="historia.js";
$js[]="about.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
$css[]="historia.css";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="overview" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior">
  <?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
    <div class="full-container">
      <div class="container">
        <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure">
                    <img src="<?=$URL_ROOT?>assets/img/technological/cs2-hero.jpg" class="">
                      <figcaption class="figure-caption"><?=__("source: MOVENTAS");?></figcaption>
                 </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=__("CS2 Novel gear design concept");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
          </article>
      </div>
    </div>
    <div class="container">
      <a href="<?=$URL_ROOT_BASE?>/<?=$txt->technological->url?>/#casestudies"><?=__("< Back to CS overview");?></a>
    </div>
    <div class="full-container bg-gray mt-1 mb-4 pt-4 pb-4">
      <div class="container cs1">
        <article class="pl-1 pr-1 pl-md-3 pr-md-3 development">
          <p class="col-md-6 p-0 mb-3 textDestacado"><?=__("GBX is an essential part of the turbine’s drivetrain converting torque and speed to be better suitable for generator.");?></p>
          <div class="row">
            <div class="col-md-6">
              <p class="p-0 mb-1"><?=__("GBX is an essential part of the turbine’s drivetrain converting torque and speed to be better suitable for generator.");?></p>
              <ul class="list-unstyled list-arrow">
                <li class="pb-1"><?=__("Novel gear materials that will allow higher load carrying capacity for the geared components. ");?></li>
                <li class="pb-1"><?=__("Novel hydrodynamic plain bearings in selected positions. ");?></li>
              </ul>
              <p class="p-0 mb-1"><?=__("These two new technologies are the enablers to increase the load carrying capacity with less material spend (torque density Nm/kg) compared to current designs targeting the level of 200Nm/kg. ");?></p>
              <ul class="list-unstyled list-arrow">
                <li class="pb-1"><?=__("Third key factor is to build up a virtual prototype and hybrid testing method, which will bring significant advantages to design and verification process.");?></li>
              </ul>
            </div>
            <div class="col-md-5 offset-md-1">
              <figure class="figure">
                <img src="<?=$URL_ROOT?>assets/img/technological/cs2-bearings.png">
                <figcaption class="figure-caption"><?=__("source: MOVENTAS");?></figcaption>
              </figure>
            </div>
          </div>
            <div class="row">
              <div class="col-lg-7 shadow bg-white mt-2 mb-3 p-2 d-sm-flex">
                <img src="<?=$URL_ROOT?>assets/img/technological/cs2-bearing1.jpg" class="mr-2 mb-1 mb-sm-0">
                <p class="m-0"><?=__("Wind turbine gearboxes are mostly using roller element bearings nowadays. ");?></p>
              </div>
              <div class="col-lg-7 offset-lg-5 shadow bg-white mb-2 mb-md-3 p-2 d-sm-flex">
                <p class="m-0 mb-1 mb-sm-0"><?=__("Hydrodynamic journal plain bearing (HBP) solutions have been used in the latest high torque density designs. ");?></p>
                <img src="<?=$URL_ROOT?>assets/img/technological/cs2-bearing2.jpg" class="ml-2">
              </div>
            </div>
            <div class="row">
                <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->technological->url?>/#casestudies" class="btn btn-corporate1 back shadow"><?=__("back to cs overview");?></a>
            </div>
        </article>
      </div>
    </div>
  </main>
  <?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>