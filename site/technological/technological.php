<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-index.php");
$title=__("Technological approach - INNTERESTING");
$description=__("INNTERESTING proposes a breakthrough hybrid methodology and disruptive design tools to demonstrate reliability and lifetime extension of large wind turbine components");
$js[]="ekko-lightbox.min.js";
$js[]="historia.js";
$js[]="about.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
$css[]="historia.css";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="overview" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior">
  <?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
    <div class="full-container">
      <div class="container">
        <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure">
                    <img src="<?=$URL_ROOT?>assets/img/technological/hero.jpg" class="">
                    <figcaption class="figure-caption"><?=__("source: IKERLAN");?></figcaption>
                  </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=__("Technological approach");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
          </article>
      </div>
    </div>
    <div class="container pt-3 pt-md-3">
      <article class="pl-1 pr-1 pl-md-3 pr-md-3">
        <p class="textDestacado col-md-6 p-0 mb-2"><?=__("It is essential to provide European companies with the most advanced testing methodologies and to design tools to help them find innovative solutions and bring them faster to commercialization. INNTERESTING proposes a breakthrough hybrid methodology and disruptive design tools to demonstrate reliability and lifetime extension of large wind turbine components, eliminating the need of larger test-benches in the future. This paradigm shift aims to strengthen the European leadership in renewables, achieving increased power capacity and accelerating time to market.");?></p>
        </article>
    </div>
    <div class="container pt-4" id="testingmethod">
      <article class="pl-1 pr-1 pl-md-3 pr-md-3">
        <h2><?=__("INNTERESTING hybrid testing methodology");?></h2>
        <p class="col-md-6 p-0 mb-4"><?=__("INNTERESTING will develop an innovative hybrid testing methodology that will be not only useful for validating large wind turbine components, but also for evaluating lifetime extension concepts that must be implemented in already existing wind farms in order to extend the remaining life of the structure.");?></p>
        <div class="row">
          <div class="col-md-6 mb-4">
            <h4><?=__("CURRENT TESTING PYRAMID");?></h4>
            <p><?=__("COMBINING PHYSICAL AND VIRTUAL TESTING");?></p>
            <img src="<?=$URL_ROOT?>assets/img/technological/current-testing.svg" class="mt-3">
          </div>
          <div class="col-md-6">
            <h4><?=__("INNTERESTING HYBRID TESTING METHODOLOGY");?></h4>
            <p><?=__("WITH THE HYBRID TEST CONCEPT");?></p>
            <img src="<?=$URL_ROOT?>assets/img/technological/innteresting-testing.svg" class="mt-3">
          </div>
        </div>
      </article>
    </div>
    <div class="full-container bg-greenLight mt-4 mb-4 pt-4 pb-4">
      <div class="container">
        <article class="pl-1 pr-1 pl-md-3 pr-md-3 development">
          <p class="col-md-6 p-0 mb-2"><?=__("INNTERESTING aims to eliminate the need of building large test benches in the future by simplifying the product development process (PDP) of new wind turbine components, reducing costs and time. ");?></p>
            <h3 class="mt-4"><?=__("CURRENT PRODUCT DEVELOPMENT PROCESS (PDP) ");?></h3>
            <div class="row">
              <div class="col-md-2 shadow bg-white border border-dark mr-2 mb-2 mb-md-0 pt-1 pr-2 pb-1 pl-2 arrow-r arrow-dark">
                <p class="m-0"><?=__("Predesign");?></p>
              </div>
              <div class="col-md-2 shadow bg-white border border-dark mr-2 mb-2 mb-md-0 pt-1 pr-2 pb-1 pl-2 arrow-r arrow-dark">
                <p class="m-0"><?=__("Design and evaluation of new component using existing design tools");?></p>
              </div>
              <div class="col-md-2 shadow bg-white border border-dark mr-2 mb-2 mb-md-0 pt-1 pr-2 pb-1 pl-2 arrow-r arrow-dark">
                <p class="m-0"><?=__("Full-scale prototype and full-scale testbench design and construction");?></p>
              </div>
              <div class="col-md-2 shadow bg-white border border-dark mr-2 mb-2 mb-md-0 pt-1 pr-2 pb-1 pl-2 arrow-r arrow-dark">
                <p class="m-0"><?=__("Full scale testing campaign");?></p>
              </div>
              <div class="col border-dark mr-2 mb-2 mb-md-0 pt-1 pr-2 pb-1 pl-2">
                <p class="m-0"><img src="<?=$URL_ROOT?>assets/img/technological/turbine-bl.svg" class="float-left mr-2"> <?=__("Validation using fixed loads 1 specimen tested");?></p>
              </div>
            </div>
            <div class="row">
              <p class="ml-md-5 mt-0 mt-md-4 mb-4 pt-2 pl-1 arrow-long arrow-long-dark w-100"><?=__("30 months");?></p>
            </div>
            <h3><?=__("INNTERESTING PRODUCT DEVELOPMENT PROCESS (PDP)");?></h3>
            <div class="row">
              <div class="col-md-2 shadow bg-white border border-secondary mr-2 mb-2 mb-md-0 pt-1 pr-2 pb-1 pl-2 arrow-r">
                <p class="m-0"><?=__("Predesign");?></p>
              </div>
              <div class="col-md-3 shadow bg-white border border-secondary mr-2 mb-2 mb-md-0 pt-1 pr-2 pb-1 pl-2 arrow-r">
                <p class="m-0"><?=__("Whole PDP of new component with INNTERESTING design tools and hybrid testing methodology");?></p>
              </div>
              <div class="col-md-3 border-secondary mr-2 pt-1 pr-2 pb-1 pl-2">
                <p class="m-0"><img src="<?=$URL_ROOT?>assets/img/technological/turbine-green.svg" class="float-left mr-2"> <?=__("Validation using fixed loads 1 specimen tested");?></p>
              </div>
            </div>
            <div class="row">
              <p class="ml-md-5 mt-0 mt-md-4 mb-4 pt-2 pl-1 arrow-long w-100"><?=__("15 months");?></p>
            </div>
        </article>
      </div>
    </div>

      <div class="container pt-2 pt-md-4 mb-4" id="casestudies">
        <article class="pl-1 pr-1 pl-md-3 pr-md-3 development">
          <h2 class="mt-4"><?=__("CASE STUDIES overview");?></h2>
          <p class="col-md-6 p-0 mb-2"><?=__("INNTERESTING project also pursues the validation of the developments through 3 different case studies dealing with innovative pitch bearing concept (CS1), new gearbox component design (CS2) and innovative repairing solution for lifetime extension of pitch bearings (CS3)");?></p>
          <div class="csOverview">
            <p class="float-right bg-white"><?=__("source: LAULAGUN");?></p>
            <div class="row m-0 w-100">
              <div class="col-md bg-white shadow p-2 ml-2 mb-2">
                <h4 class="mt-1 mb-1"><?=__("CS1 Novel pitch bearing design concept");?></h4>
                <p class="mb-2"><?=__("CS1 is based on an innovative pitch bearing concept that will be installed in a 20 MW offshore wind turbine from the year 2030 onwards (hub height of 160, rotor diameter of 276 m and the pitch bearing diameter of 7 m). The turbine will be installed in a wind farm with a size of 2 GW with 100 turbines.");?></p>
                <a href="<?=$URL_ROOT_BASE?>/<?=$txt->technological->url?>/<?=$txt->technological->case1->url?>" class=""><?=__("Read more");?></a>
              </div> 
              <div class="col-md bg-white shadow p-2 ml-2 mb-2">
                <h4 class="mt-1 mb-1"><?=__("CS2 Novel gear design concept");?></h4>
                <p class="mb-2"><?=__("CS2 is based on a new gearbox concept that will be installed in a 10 MW onshore wind turbine from the year 2030 onwards (hub height of 119, rotor diameter of 202 m and a torque density up to level of 200Nm/kg). The wind turbine will be installed in a farm size of 100MW with 10 turbines.");?></p>
                <a href="<?=$URL_ROOT_BASE?>/<?=$txt->technological->url?>/<?=$txt->technological->case2->url?>" class=""><?=__("Read more");?></a>
              </div> 
              <div class="col-md bg-white shadow p-2 ml-2 mb-2">
                <h4 class="mt-1 mb-1"><?=__("CS3 Novel stiffening concept for lifetime extension of existing pitch bearings");?></h4>
                <p class="mb-2"><?=__("CS3: In this CS, a 3,4 MW wind turbine will be installed in 2020, and the pitch bearing will fail at an early stage of the lifetime (<10 years): a reparation and stiffening solution will be required in the pitch bearing.");?></p>
                <a href="<?=$URL_ROOT_BASE?>/<?=$txt->technological->url?>/<?=$txt->technological->case3->url?>" class=""><?=__("Read more");?></a>
              </div> 
            </div>
          </div>
        </article>
      </div>

    
    <div class="container pb-2">
      <article class="banner mt-1 mb-3 p-2 p-md-4" style="background-image: url(<?=$URL_ROOT?>assets/img/technological/banner-related-projects.jpg);background-size: cover; background-position: center">
        <div class="text">
          <h3 class="text-gray text-white"><?=__("RELATED PROJECTS");?></h3>
          <p class="text-white"><?=__("Discover the projects related with Innteresting project.");?></p>
        </div>
        <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->relatedprojects->url?>/" class="btn btn-corporate1 shadow"><?=__("ALL RELATED PROJECTS");?></a>
      </article>
    </div>
  </main>
  <?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>