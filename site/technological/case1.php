<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-index.php");
$title=__("Case study 1 - INNTERESTING");
$description=__("A pitch bearing is a component that connects the rotor hub and the rotor blade. The bearing allows the required oscillation to control the loads and power of the wind turbine");
$js[]="ekko-lightbox.min.js";
$js[]="historia.js";
$js[]="about.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
$css[]="historia.css";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="overview" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior">
  <?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
    <div class="full-container">
      <div class="container">
        <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure">
                    <img src="<?=$URL_ROOT?>assets/img/technological/cs1-hero.jpg" class="">
                     <figcaption class="figure-caption"><?=__("source: IKERLAN");?></figcaption>
                 </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=__("CS1 Novel pitch bearing design concept");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
          </article>
      </div>
    </div>
    <div class="container">
      <a href="<?=$URL_ROOT_BASE?>/<?=$txt->technological->url?>/#casestudies"><?=__("< Back to CS overview");?></a>
    </div>
    <div class="full-container bg-gray mt-1 mb-4 pt-4 pb-4">
      <div class="container cs1">
        <article class="pl-1 pr-1 pl-md-3 pr-md-3 development">
          <p class="col-md-6 p-0 mb-3 textDestacado"><?=__("A pitch bearing is a component that connects the rotor hub and the rotor blade. The bearing allows the required oscillation to control the loads and power of the wind turbine. ");?></p>
          <div class="row">
            <ul class="col-md-6 list-unstyled list-arrow">
              <li class="pb-1"><?=__("The <strong>new pitch bearing design</strong> must fulfil the loading, environmental and structural specifications defined for future larger wind turbines: <strong>higher static capacity, longer lifetime</strong> and longer <strong>fatigue loading requirements</strong> are expected. ");?></li>
              <li class="pb-1"><?=__("Final Rolling Contact Fatigue (RCF) failure mode validation will be performed on the 1,5 MW test bench at LAULAGUN´s facilities. Other major failure modes will also be analysed and validated during the development of the component in order to verify the results obtained by hybrid testing implemented during INNTERESTING.");?></li>
            </ul>
            <div class="col-md-5 offset-md-1">
              <figure class="figure">
                <img src="<?=$URL_ROOT?>assets/img/technological/cs1-bearings.png">
                <figcaption class="figure-caption"><?=__("source: SIEMENS");?></figcaption>
              </figure>
            </div>
          </div>
            <div class="row">
              <div class="col-lg-7 shadow bg-white mt-2 mb-3 p-2 d-sm-flex">
                <img src="<?=$URL_ROOT?>assets/img/technological/cs1-bearing1.jpg" class="mr-2 mb-1 mb-sm-0">
                <p class="m-0"><?=__("Currently the most common pitch bearing concept is the Four-point contact (4PC) bearing design, widely used in the wind industry.");?></p>
              </div>
              <div class="col-lg-7 offset-lg-5 shadow bg-white mb-2 mb-md-3 p-2 d-sm-flex">
                <p class="m-0 mb-1 mb-sm-0"><?=__("In the last years 3 Row Roller (3RR) bearings have gained an increasing presence in the sector");?></p>
                <img src="<?=$URL_ROOT?>assets/img/technological/cs1-bearing2.jpg" class="ml-2">
              </div>
              <div class="col-lg-7 offset-lg-1 shadow bg-white mb-3 p-2 d-sm-flex">
                <img src="<?=$URL_ROOT?>assets/img/technological/cs1-bearing3.jpg" class="mr-2 mb-1 mb-sm-0">
                <p class="m-0"><?=__("<strong>Future bearing concept</strong><br>But in any case, a bearing concept for supporting higher loading conditions is needed for future wind turbines.");?></p>
              </div>
            </div>
            <div class="row">
                <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->technological->url?>/#casestudies" class="btn btn-corporate1 back shadow"><?=__("back to cs overview");?></a>
            </div>
        </article>
      </div>
    </div>
  </main>
  <?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>