<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-index.php");
$title=__("Case study 3 - INNTERESTING");
$description=__("Experimentally validated innovative pitch bearing lifetime extension concept developed during the INNTERESTING project for being used in already installed wind turbines for pitch bearing stiffening and reparations.");
$js[]="ekko-lightbox.min.js";
$js[]="historia.js";
$js[]="about.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
$css[]="historia.css";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="overview" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior">
  <?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
    <div class="full-container">
      <div class="container">
        <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure">
                    <img src="<?=$URL_ROOT?>assets/img/technological/cs3-hero.jpg" class="">
                     <figcaption class="figure-caption"><?=__("source: VTT");?></figcaption>
                  </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=__("S3 Novel stiffening concept for lifetime extension of existing pitch bearings");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
          </article>
      </div>
    </div>
    <div class="container">
      <a href="<?=$URL_ROOT_BASE?>/<?=$txt->technological->url?>/#casestudies"><?=__("< Back to CS overview");?></a>
    </div>
    <div class="full-container bg-gray mt-1 mb-4 pt-4 pb-4">
      <div class="container cs1">
        <article class="pl-1 pr-1 pl-md-3 pr-md-3 development">
          <p class="col-md-6 p-0 mb-4 textDestacado"><?=__("The new pitch bearing lifetime extension concept must fulfil the loading, environmental and structural specifications defined for the extension of lifetime in already installed wind turbines. ");?></p>
          <div class="row">
            <div class="col-md-6 mb-3">
              <h2 class="p-0 mb-1 h3"><?=__("problems");?></h2>
              <ul class="list-unstyled list-arrow">
                <li class="pb-1"><?=__("The need of retesting the components for assuring longer lifetimes is a problem for the manufacturers ");?></li>
                <li class="pb-1"><?=__("When there is a failure of the component in use (e.g. Ring structural fatigue (RSF) failure mode) the replacement of the component can be difficult");?></li>
              </ul>
            </div>
          </div>
            <div class="row">
              <div class="col-md-4 shadow bg-corporate1 mb-0 mb-md-4 p-3">
                <h2 class="p-0 mb-1 text-white h3"><?=__("REQUIREMENTS");?></h2>
                <ul class="list-unstyled list-arrow arrow-wh text-white">
                  <li class="pb-1"><?=__("The desired time to implement the idea must be short ");?></li>
                  <li class="pb-1"><?=__("The wind farm must remain in operation working with a high confidence level");?></li>
                </ul>
              </div>
              <div class="col-md shadow bg-white ml-0 ml-md-1 mb-2 mb-md-3 p-3">
                <h2 class="p-0 mb-1 h3"><?=__("OBJECTIVE");?></h2>
                <p><?=__("To verify lifetime extension and reliability evaluation of the new lifetime extension concept for pitch bearings.");?></p>
                <ul class="list-unstyled list-arrow">
                  <li class="pb-1"><?=__("To verify the new lifetime extension concept viability for stiffening and/or reparation in pitch bearings.");?></li>
                  <li class="pb-1"><?=__("To verify the suitability of the hybrid testing methodology during validation process of novel lifetime extension concepts for pitch bearings with respect to the current processes.");?></li>
                </ul>
              </div>
            </div>
            <div class="row">
                <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->technological->url?>/#casestudies" class="btn btn-corporate1 back shadow"><?=__("back to cs overview");?></a>
            </div>
        </article>
      </div>
    </div>
  </main>
  <?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>