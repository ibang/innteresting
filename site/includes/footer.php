<footer>
  <div class="container">
    <div class="row no-gutters">
      <div class="col-lg-2">
        <p class="projectName"><img src="<?=$URL_ROOT?>assets/img/all/logo-eu.png" alt=""></p>
        <p class="small"><?=__("This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 851245.")?></p>
      </div>
      <div class="col-md-9 offset-lg-1">
           <p class="mt-2"><span class="person">MIREIA OLAVE IRIZAR</span> <a data-event="link" data-category="telefono" class="tel" data-label="pie" href="tel:<?=$txt->footer->phonenumber?>"><?=$txt->footer->phonenumberlink?></a> <a data-event="link" data-category="mail" class="mail" data-label="pie" href="mailto:<?=$txt->footer->emailaddress?>"><?=$txt->footer->emailaddress?></a> <a data-event="link" data-category="twitter" data-label="pie" href="https://twitter.com/INNTERESTING_eu" target="_blank" class="twitter"><span><?=__("Twitter")?></span></a> <a data-event="link" data-category="linkedin" data-label="pie" href="https://www.linkedin.com/company/innteresting-eu/" target="_blank" class="linkedin"><span><?=__("Linkedin")?></span></a></p>
           <p class="mt-2 mt-md-3 small"><?=__("The content of this website represents the views of the author only and is his/her sole responsibility; it cannot be considered to reflect the views of the European Commission and/or the Executive Agency for Small and Medium-sized Enterprises (EASME) or any other body of the European Union. The European Commission and the Agency do not accept any responsibility for use that may be made of the information it contains.")?></p>
            <img src="<?=$URL_ROOT?>assets/img/all/logo-innteresting.svg" class="float-md-right m-0  mt-1">
           <ul class="list-unstyled list-inline small mt-3">
            <li class="list-inline-item"><a <? if($s1=="legal"){?> class="active"<? }?> href="<?=$URL_ROOT_BASE?>/<?=$txt->legal->url?>/" rel="nofollow"><?=$txt->nav->footer->legal?></a></li>
            <li class="list-inline-item"><a <? if($s1=="cookies"){?> class="active"<? }?> href="<?=$URL_ROOT_BASE?>/<?=$txt->cookies->url?>/" rel="nofollow"><?=$txt->nav->footer->cookies?></a></li>
            <li class="list-inline-item"><a <? if($s1=="privacy"){?> class="active"<? }?> href="<?=$URL_ROOT_BASE?>/<?=$txt->privacy->url?>/" rel="nofollow"><?=$txt->nav->footer->privacy?></a></li>
            <li class="list-inline-item"><?=sprintf($txt->footer->copy, date('Y'));?></li>
          </ul>
        </div>
    </div>
  </div>
</footer>
<?if (SASSLESS=='LESS'){?>
<script src="<?=$URL_ROOT?>assets/js/jquery-2.1.3.min.js"></script>
<script src="<?=$URL_ROOT?>assets/js/bootstrap.min.js"></script>
<?}elseif(SASSLESS=='SASS'){?>
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<?}?>
<script src="<?=$URL_ROOT?>assets/js/jquery.cookiebar.js?v=<?=CFGCURRENTVERSION;?>"></script>
<script src="<?=$URL_ROOT?>assets/js/nav-main.js?v=<?=CFGCURRENTVERSION;?>"></script>
<script src="<?=$URL_ROOT?>assets/js/scroll.js"></script>
<?if (!empty($js)){?>
	<?foreach($js as $cjs){?>
<script src="<?=$URL_ROOT?>assets/js/<?=$cjs?>?v=<?=CFGCURRENTVERSION;?>"></script>
	<?}?>
<?}?>
<?if (!empty($js_lang)){?>
	<?foreach($js_lang as $cjs_lang){?>
		<?if(!is_array($cjs_lang)){$cjs_lang = array('src' => $cjs_lang, 'params' => '');}?>
<script src="<?=$URL_ROOT_BASE?>/assets/js/<?=$cjs_lang['src']?>?v=<?=CFGCURRENTVERSION;?><?=$cjs_lang['params']?>"></script>
	<?}?>
<?}?>
<script>
$(document).ready(function(){
$.cookieBar({
message: '<?=addslashes($txt->cookies->alert->title)?> <a href="<?=$URL_ROOT_BASE?>/<?=$txt->cookies->url?>/"><?=addslashes($txt->cookies->alert->infolink)?></a>',
acceptText: "<?=addslashes($txt->cookies->alert->link)?>"
});
$(".scroll").on("click", function(e){
e.preventDefault();
var isWebkit = 'WebkitAppearance' in document.documentElement.style
if (isWebkit)
bodyelem = $("body");
else
bodyelem = $("html,body");	
bodyelem.animate({
scrollTop: $($(this).attr('href')).offset().top
}, 400); 
return false;
});
});
</script>

<script type='application/ld+json'>
{
  "@context" : "http://schema.org", 
  "@type" : "LocalBusiness",
  "url" : "<?=MAINHOST;?>",
  "name" : "Innteresting",
  "logo" : "<?=MAINHOST;?>/assets/img/all/logo-innteresting.png",
  "sameAs" : [ "https://www.linkedin.com/company/xxxx",
      "https://www.youtube.com/channel/xxxx"],
  "telephone" : "(+34) XXX XXX XXX",
  "email" : "estudio@innteresting.es",
  "address" :  { 
    "@type" : "PostalAddress",
    "streetAddress" : "Ctra. Madrid-Irún, Km 415",
    "addressLocality" : "Idiazabal",
    "addressRegion" : "Gipuzkoa",
    "postalCode" : "20213",
    "addressCountry" : "Spain"
  } 
}
</script>
<? // addToLog('FIN FOOTER S2, TYPE: '.$_SESSION['s2'].', '.$_SESSION['type']); ?>