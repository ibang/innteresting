<?if (!empty($related_news)){?>
<section class="related-news">
	<?foreach ($related_news as $related){?>
	<div class="col-md-4">
		<article class="item-news little">
			<div class="object">
				<?if ($related["image1"]){?>
				<p class="photo"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->news->url?>/<?=$related["slug"]?>/"><img class="img-responsive" src="<?=$URL_ROOT?>uploads/news/<?=$related["image1"]?>" alt="<?=($related["alt-image1"]?htmlspecialchars(strip_tags($related["alt-image1"])):str_replace("\"","'", $related["headline"]))?>" /></a></p>
				<?}?>
				<div class="content">
					<p class="date"><?=parsedate($related["date"],$language);?></p>
					<h2 class="title"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->news->url?>/<?=$related["slug"]?>/"><?=$related["headline"]?></a></h2>
				</div>
			</div>
			<p class="summary hidden-xs"><?=$related["intro"]?> <a class="link" href="<?=$URL_ROOT_BASE?>/<?=$txt->news->url?>/<?=$related["slug"]?>/"><?=__("Leer más");?> <i class="fa fa-angle-right"></i></a></p>
		</article>
	</div>
	<?}?>
</section>
<?}?>
