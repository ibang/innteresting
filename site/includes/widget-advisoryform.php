<div id="advisoryform">
							<div class="form">
								<? if ($_GET['status']=="success"){?>
								<div class="alert alert-success">
									<p class="title"><?=$txt->form->alert->success->title?></p>
									<p><?=$txt->form->alert->success->text?></p>
								</div>
								<?};?>
								<?if ($validate["developerTest"]==1){?>
								<div class="alert alert-warning">
									<p class="title"><?=$txt->form->alert->error->title?></p>
									<p>DEVELOPER TEST :: NOT SENDED TO THAT EMAIL ADDRESS</p>
								</div>
								<?}elseif ($error>0 ){?>
								<div class="alert alert-danger">
									<p class="title"><?=$txt->form->alert->error->title?></p>
									<p><?=$txt->form->alert->error->text?></p>
									<ol>
										<?if ($validate["firstname"]==1){?>
											<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->firstname->title?></strong> <?=$txt->form->alert->error->required?></li>
										<?}?>
										<?if ($validate["firstname"]==2){?>
											<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->firstname->title?></strong> <?=$txt->form->alert->error->size?></li>
										<?}?>
										
										<?if ($validate["email"]==1){?>
											<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->email->title?></strong> <?=$txt->form->alert->error->required?></li>
										<?}?>
										<?if ($validate["email"]==2){?>
											<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->email->title?></strong> <?=$txt->form->alert->error->emailformat?></li>
										<?}?>
										
										<?if ($validate["message"]==1){?>
											<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->message->title?></strong> <?=$txt->form->alert->error->required?></li>
										<?}?>
										<?if ($validate["message"]==2){?>
											<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->message->title?></strong> <?=$txt->form->alert->error->size?></li>
										<?}?>
										<?if ($validate["alreadysent"]==1){?>
											<li><?=$txt->form->alert->error->alreadysent?></li>
										<?}?>
										<?if ($validate["consent"]==1){?>
											<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->consent->title?></strong> <?=$txt->form->alert->error->required?></li>
										<?}?>
										<?if ($validate["recaptcha"]==1){?>
											<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->recaptcha->title?></strong> <?=$txt->form->alert->error->required?></li>
										<?}?>

                                        <?if ($validate["position"]==1){?>
											<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->position->title?></strong> <?=$txt->form->alert->error->required?></li>
										<?}?>

                                        <?if ($validate["advisory"]==1){?>
											<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->advisory->title?></strong> <?=$txt->form->alert->error->required?></li>
										<?}?>

									</ol>
								</div>
								<?}?>
								<form action="<?=rtrim(preg_replace(array('/\?.*/', '/'.preg_quote($txt->form->url, '/').'\/$/'), '',  $_SERVER["REQUEST_URI"]), '/').'/'?>" method="post" id="form-contact" name="form-contact"  data-gc="form" class="recapcha3form" >
									<?if (CAPTCHA_ACTIVE == '2' OR (CAPTCHA_ACTIVE == '1' AND $validate["recaptcha"]==1)){?>
									<input type="hidden" name="requesttype" id="requesttype" value="1"  />
									<?}?>
									<?if(!empty($contact["about"])){?>
									<input class="hidden" type="hidden" name="about" id="about" value="<?=htmlspecialchars($contact["about"])?>"  />
									<?}?>
									<?if(!empty($contact["type"])){?>
									<input class="hidden" type="hidden" name="type" id="type" value="<?=htmlspecialchars($contact["type"])?>"  />
									<?}?>
									<?if ($_GET['status']=="success"){?>
									<input type="hidden" name="sended" id="sended" value="<?=$s1;?>"  />
									<?}?>
									<input class="hidden" type="hidden" name="send" id="send" value="send"  />
									<input class="hidden" type="hidden" name="dataid" id="dataid" value="<?=$contact["dataid"];?>"  />
									<input type="hidden" name="googlecookie" id="googlecookie" value="<?php echo !empty($contact['googlecookie'])?$contact['googlecookie']:'';?>" />
									<input type="hidden" name="gclid" id="gclid" value="<?php echo !empty($contact['gclid'])?$contact['gclid']:'';?>" />
									<input type="hidden" name="gmode" id="gmode" value="<?php echo !empty($contact['gmode'])?$contact['gmode']:'';?>" />
									<input type="hidden" name="request" id="request" value="<?php echo !empty($contact['request'])?$contact['request']:$_SERVER["REQUEST_URI"];?>" />
									<div class="row">

										<div class="col-md-6">
											  <? /* nuevo campo de formulario */?>
										   
											<select name="advisory" id="advisory" class="mb-3 border-0 shadow">
												<option selected value="sustainabilityGroup"><?=$txt->form->advisoryGroup->holder1?> *</option>
												<option value="technicalGroup"><?=$txt->form->advisoryGroup->holder2?> *</option>
											</select>
                                            
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group<?if ($error AND !empty($validate["firstname"]) ){?> has-error<?}?>">
											  <label for="firstname" class=""><?=$txt->form->firstname->title?> <span>*</span></label>
											  <input type="text" class="form-control border-0 mb-3 shadow input-lg<?if ($error AND !empty($validate["firstname"]) ){?> has-error<?}?>" name="firstname" id="firstname" placeholder="<?=$txt->form->firstname->holder?> *" title="<?=$txt->form->firstname->title?>" value="<?=htmlspecialchars($contact["firstname"])?>" required />				    
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group<?if ($error AND !empty($validate["company"]) ){?> has-error<?}?>">
											  <label for="company" class=""><?=$txt->form->company->title?> <span>*</span></label>
											  <input type="text" class="form-control border-0 mb-3 shadow input-lg<?if ($error AND !empty($validate["company"]) ){?> has-error<?}?>" name="company" id="company" placeholder="<?=$txt->form->company->holder?> *" title="<?=$txt->form->company->title?>" value="<?=htmlspecialchars($contact["company"])?>" required />				    
											</div>
										</div>
										
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group<?if ($error AND !empty($validate["company"]) ){?> has-error<?}?>">
											  <label for="position" class=""><?=$txt->form->position->title?> <span>*</span></label>
											  <? /* nuevo campo de formulario */?>
											  <input type="text" class="form-control border-0 mb-3 shadow input-lg<?if ($error AND !empty($validate["position"]) ){?> has-error<?}?>" name="position" id="position" placeholder="<?=$txt->form->position->holder?> *" title="<?=$txt->form->position->title?>" value="<?=htmlspecialchars($contact["position"])?>" required />				    
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group<?if ($error AND !empty($validate["email"]) ){?> has-error<?}?>">
											  <label for="email" class=""><?=$txt->form->email->title?> <span>*</span></label>
											  <input type="email" class="form-control border-0 mb-3 shadow input-lg<?if ($error AND !empty($validate["email"]) ){?> has-error<?}?>" name="email" id="email" placeholder="<?=$txt->form->email->holder?> *" title="<?=$txt->form->email->title?>" value="<?=htmlspecialchars($contact["email"])?>" required />				    
											</div>
										</div>
										
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group<?if ($error AND !empty($validate["message"])){?> has-error<?}?>">
											  <label for="message" class=""><?=$txt->form->reasons->title?> <span>*</span></label>
											  <textarea class="form-control border-0 mb-3 shadow input-lg<?if ($error AND !empty($validate["message"])){?> has-error<?}?>" rows="7" name="message" id="message" placeholder="<?=$txt->form->reasons->holder?> *" title="<?=$txt->form->reasons->title?>" required><?=htmlspecialchars($contact["message"])?></textarea>	    
											</div>
										</div>
									</div>
									<div class="row" data-show="#consentinfo">
										<div class="col-md-12">
											<div class="form-group<?if ($error AND !empty($validate["consent"])){?> has-error<?}?>">
												<div class="checkbox p-0">
													<label class="checkbox small"><input type="checkbox" name="consent" id="checkbox" class="mr-1" title="" value="Yes" required /><?=sprintf($txt->form->consent->text, $URL_ROOT_BASE.'/'.$txt->privacy->url.'/');?></label>
												</div>
												<div class="consentinfo starthide small" id="consentinfo">
													<?=sprintf($txt->form->consent->contact, $URL_ROOT_BASE.'/'.$txt->privacy->url.'/');?>
												</div>
											</div>
											<p class="required small"><span><?=$txt->form->required?></span></p>
										</div>
									</div>
									<?if (CAPTCHA_ACTIVE == '2' OR (CAPTCHA_ACTIVE == '1' AND $validate["recaptcha"]==1)){?>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group<?if ($error AND !empty($validate["recaptcha"])){?> has-error<?}?>">
												<label for="recaptcha" class="sr-only"><?=$txt->form->recaptcha->title?> <span>*</span></label>
												<div class="recaptcha form-control<?if ($error AND !empty($validate["recaptcha"])){?> has-error<?}?>">
													<div class="g-recaptcha" data-sitekey="<?=CAPTCHA_PUBLIC_SITE_KEY;?>"></div>
												</div>
											</div>
										</div>
									</div>
									<?}?>
									<div class="row">
										<div class="col-md-12">
											  <button type="submit" class="btn btn-corporate1"><span><?=htmlspecialchars($txt->form->submit2)?></span></button>		    
										</div>
									</div>
								</form>
							</div>
						</div>