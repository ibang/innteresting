<?require("{$DOC_ROOT}site/includes/widget-body-tag-manager.php")?>	
<!--[if lt IE 8]>
<div id="browser-bar">
	<p><?=$txt->browser->alert->title?> <a class="cb-enable" href="http://outdatedbrowser.com/<?=$language?>" rel="nofollow" target="_blank"><?=$txt->browser->alert->link?></a></p>
</div>
<![endif]-->
<header>
  <nav class="fixed-top navbar-expand-lg navbar-dark bg-white">

  <div class="full-container bg-corporate1 d-none d-lg-block">
    <div class="container">
      <ul class="navbar-aux text-right list-unstyled">
          <li class="<? if($s1=="downloads"){?>active<? }?> nav-item"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->downloads->url?>/"><?=__("Downloads")?></a></li>
          <li class="<? if($s1=="news"){?>active<? }?> nav-item"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->media->url?>/"><?=__("Media")?></a></li>
          <li class="<? if($s1=="contacto"){?>active<? }?> nav-item"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->contact->url?>/"><?=__("Contact")?></a></li>
      </ul>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <? if ($s1=="home") {?><h1 id="logo" class=""><a href="<?=$URL_ROOT_BASE?>/" class=""><span><?=__("Durable")?></span></a></h1><? }
        else {?><p id="logo" class=""><a href="<?=$URL_ROOT_BASE?>/" class=""><span><?=__("Durable")?></span></a></p><? }?>
          <button class="navbar-toggler d-inline-block d-lg-none" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
      </div>
      <div class="col-lg-9">
        <div class="collapse navbar-collapse flex-row ml-1 ml-lg-3" id="navbarCollapse">
          <ul class="navbar-nav mb-1">
            <li class="<? if($s1=="project"){?> active<? }?> nav-item sub"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->project->url?>/"><?=__("PROJECT")?></a>
              <div class="sub-container clearfix">
                <div class="sub-content shadow bg-white d-inline-block pt-2 pb-2">
                  <ul>
                    <li class="d-block"><a href="<? if($s1!=="project"){?><?=$URL_ROOT_BASE?>/<?=$txt->project->url?>/<? }?>#overview" class="scroll"><?=__("OVERVIEW")?></a></li>
                    <li class="d-block"><a href="<? if($s1!=="project"){?><?=$URL_ROOT_BASE?>/<?=$txt->project->url?>/<? }?>#challenge" class="scroll"><?=__("CHALLENGE")?></a></li>
                    <li class="d-block"><a href="<? if($s1!=="project"){?><?=$URL_ROOT_BASE?>/<?=$txt->project->url?>/<? }?>#objectives" class="scroll"><?=__("OBJECTIVES")?></a></li>
                    <li class="d-block"><a href="<? if($s1!=="project"){?><?=$URL_ROOT_BASE?>/<?=$txt->project->url?>/<? }?>#impacts" class="scroll"><?=__("MAIN EXPECTED IMPACTS")?></a></li>
                    <li class="d-block"><a href="<? if($s1!=="project"){?><?=$URL_ROOT_BASE?>/<?=$txt->project->url?>/<? }?>#partners" class="scroll"><?=__("PARTNERS")?></a></li>
                    <li class="d-block"><a href="<? if($s1!=="project"){?><?=$URL_ROOT_BASE?>/<?=$txt->project->url?>/<? }?>#wpstructure" class="scroll"><?=__("WP STRUCTURE")?></a></li>
                    <li class="d-block"><a href="<? if($s1!=="project"){?><?=$URL_ROOT_BASE?>/<?=$txt->project->url?>/<? }?>#collaboration" class="scroll"><?=__("Collaboration")?></a></li>
                  </ul>
                </div>
              </div>
            </li>
            <li class="<? if($s1=="advisory"){?> active<? }?> nav-item sub"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->advisory->url?>/"><?=__("Advisory Group")?></a>
              <div class="sub-container clearfix">
                <div class="sub-content shadow bg-white d-inline-block pt-2 pb-2">
                  <ul>
                    <li class="d-block"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->advisory->url?>/#overview"class="scroll"><?=__("OVERVIEW")?></a></li>
                    <li class="d-block"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->advisory->url?>/#form" class="scroll <? if($s2=="become"){?>active<? }?>"><?=__("BECOME PART OF IT!")?></a></li>
                    <li class="d-block"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->socialacceptance->url?>/" class="scroll <? if($s2=="socialacceptance"){?>active<? }?>"><?=__("SOCIAL ACCEPTANCE")?></a></li>
                  </ul>
                </div>
              </div>
            </li>
            <li class="<? if($s1=="technological"){?> active<? }?> nav-item sub"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->technological->url?>/" class=""><?=__("Technological approach ")?></a>
              <div class="sub-container clearfix">
                <div class="sub-content shadow bg-white d-inline-block pt-2 pb-2">
                  <ul>
                    <li class="d-block"><a href="<? if($s1!=="technological" or $s2=="cs"){?><?=$URL_ROOT_BASE?>/<?=$txt->technological->url?>/<? }?>#overview" class="scroll"><?=__("OVERVIEW")?></a></li>
                    <li class="d-block"><a href="<? if($s1!=="technological" or $s2=="cs"){?><?=$URL_ROOT_BASE?>/<?=$txt->technological->url?>/<? }?>#testingmethod" class="scroll"><?=__("Testing methodology")?></a></li>
                    <li class="d-block"><a href="<? if($s1!=="technological" or $s2=="cs"){?><?=$URL_ROOT_BASE?>/<?=$txt->technological->url?>/<? }?>#casestudies" class="scroll<? if($s2=="cs"){?> active<? }?>"><?=__("CASE STUDIES")?></a></li>
                  </ul>
                </div>
              </div>
            </li>
            <li class="<? if($s1=="relatedprojects"){?> active<? }?> nav-item">
              <a href="<?=$URL_ROOT_BASE?>/<?=$txt->relatedprojects->url?>/" class=""><?=__("RELATED PROJECTS")?></a>
            </li>
            <li class="<? if($s1=="sectorialnews"){?> active<? }?> nav-item"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->sectorialnews->url?>/" class=""><?=__("News")?></a></li>
        		<li class="<? if($s1=="events"){?> active<? }?> nav-item">
        				<a href="<?=$URL_ROOT_BASE?>/<?=$txt->events->url?>/" class=""><?=__("Events")?></a>
			       </li>
          </ul>
          <ul class="navbar-aux list-unstyled mb-1 d-block d-lg-none">
            <li class="<? if($s1=="downloads"){?>active<? }?> nav-item"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->downloads->url?>/"><?=__("Downloads")?></a></li>
            <li class="<? if($s1=="news"){?>active<? }?> nav-item"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->media->url?>/"><?=__("Media")?></a></li>
            <li class="<? if($s1=="contacto"){?>active<? }?> nav-item"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->contact->url?>/"><?=__("Contact")?></a></li>
        </ul>
        </div>
      </div>
    </div>
</div>
  </nav>
</header>