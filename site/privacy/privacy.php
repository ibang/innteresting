<?
require_once("../../php/init.php");
$title=$txt->privacy->title;
$description=$txt->privacy->description;
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="legal" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?" ".$langURL:"")?> interior soloTexto">
<?require("{$DOC_ROOT}site/includes/header.php")?>
<div class="container mt-2 mt-lg-5">
	<section class="info">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<article class="infoProducto">
					<h1><?=$txt->privacy->h1?></h1>
					<?=$txt->privacy->text?>
				</article>
			</div>
		</div>
	</section>
</div>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>