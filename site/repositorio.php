<?
require_once("../php/init.php");
require_once("../site/php/inc-index.php");
require_once("../site/php/inc-functions.php");
$title=__("Innteresting");
$description=__("Innteresting");
$js[]="ekko-lightbox.min.js";
$js[]="lightbox.js";
$js[]="about.js";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="home" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?>">
<?require("{$DOC_ROOT}site/includes/header.php")?>


    <main role="main">

      <div class="container marketing">

            <h1><?=__("Titulo  1");?></h1>
            <h2><?=__("Titulo  2");?></h2>
            <h3><?=__("Titulo  3");?></h3>
            <h4><?=__("Titulo  4");?></h4>
            <h5><?=__("Titulo  5");?></h5>
            <p class="intro"><?=__("This is some lead body copy. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.");?></p>
             <p>This is some body copy. Donec id elit non mi porta gravida at eget metus. <a href="/style-guide#" title="">This is a hyperlink</a>. Donec ullamcorper nulla non metus auctor fringilla. <strong>This is some bold text</strong>. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. <em>This is some italicised text</em>. Sed posuere consectetur est at lobortis.</p>
             <p><small>This is some fine print. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</small></p>
            <ul>
              <li><?=__("ul li");?></li>
              <li><?=__("ul li");?></li>
              <li><?=__("ul li");?>
                  <ul>
                    <li><?=__("ul li");?></li>
                    <li><?=__("ul li");?></li>
                    <li><?=__("ul li");?></li>
                    <li><?=__("ul li");?></li>
                    <li><?=__("ul li");?></li>
                  </ul>
              </li>
              <li><?=__("ul li");?></li>
              <li><?=__("ul li");?></li>
            </ul>
            <ul class="list-unstyled">
              <li><?=__("ul li");?></li>
              <li><?=__("ul li");?></li>
              <li><?=__("ul li");?>
                  <ul>
                    <li><?=__("ul li");?></li>
                    <li><?=__("ul li");?></li>
                    <li><?=__("ul li");?></li>
                    <li><?=__("ul li");?></li>
                    <li><?=__("ul li");?></li>
                  </ul>
              </li>
              <li><?=__("ul li");?></li>
              <li><?=__("ul li");?></li>
            </ul>
            <ol>
              <li><?=__("ol li");?></li>
              <li><?=__("ol li");?></li>
              <li><?=__("ol li");?></li>
              <li><?=__("ol li");?></li>
              <li><?=__("ol li");?></li>
            </ol>
            <dl>
              <dt><?=__("dl dt");?></dt>
              <dd><?=__("dl dd");?></dd>
              <dt><?=__("dl dt");?></dt>
              <dd><?=__("dl dd");?></dd>
              <dt><?=__("dl dt");?></dt>
              <dd><?=__("dl dd");?></dd>
            </dl>
            <table class="table table-condensed table-hover">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Column One</th>
                  <th>Column Two</th>
                  <th>Column Three</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Dapibus</td>
                  <td>Nullam</td>
                  <td>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Euismod</td>
                  <td>Vestibulum</td>
                  <td>Maecenas sed diam eget risus varius blandit sit amet non magna.</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Elit</td>
                  <td>Purus</td>
                  <td>Nullam id dolor id nibh ultricies vehicula ut id elit.</td>
                </tr>
              </tbody>
            </table>
            <div class="tags">
              <a href="#" class="badge badge-pill badge-default">Default</a>
            </div>
            <!-- /tags -->
            <div class="icos">
              <object data="<?=$URL_ROOT?>assets/ico/camion.svg" type="image/svg+xml">camión</object>
              <object data="<?=$URL_ROOT?>assets/ico/carro.svg" type="image/svg+xml">Añade al carrito</object>
              <object data="<?=$URL_ROOT?>assets/ico/descuento.svg" type="image/svg+xml">Descuentos</object>
              <object data="<?=$URL_ROOT?>assets/ico/doc-reloj.svg" type="image/svg+xml">Entrega rápida</object>
              <object data="<?=$URL_ROOT?>assets/ico/doc-ok.svg" type="image/svg+xml">Documentación</object>
              <object data="<?=$URL_ROOT?>assets/ico/grua.svg" type="image/svg+xml">Instalación</object>
              <object data="<?=$URL_ROOT?>assets/ico/herramienta.svg" type="image/svg+xml">Construcción</object>
              <object data="<?=$URL_ROOT?>assets/ico/lock.svg" type="image/svg+xml">Área privada</object>
              <object data="<?=$URL_ROOT?>assets/ico/maleta.svg" type="image/svg+xml">Maleta</object>
              <object data="<?=$URL_ROOT?>assets/ico/ok.svg" type="image/svg+xml">OK</object>
              <object data="<?=$URL_ROOT?>assets/ico/persona.svg" type="image/svg+xml">Equipo</object>
              <object data="<?=$URL_ROOT?>assets/ico/phone.svg" type="image/svg+xml">Contacto</object>
              <object data="<?=$URL_ROOT?>assets/ico/prod-nuevo.svg" type="image/svg+xml">Producto nuevo</object>
              <object data="<?=$URL_ROOT?>assets/ico/sello.svg" type="image/svg+xml">Sello de calidad</object>
            </div>
            <div class="icos bg-corporate1">
              <div class="ico"><object data="<?=$URL_ROOT?>assets/ico/camion-bl.svg" type="image/svg+xml">camión</object>Innovación</div>
              
              <div class="ico"><object data="<?=$URL_ROOT?>assets/ico/doc-ok-bl.svg" type="image/svg+xml">Documentación</object> Disponibilidada inmediata</div>
              <div class="ico"><object data="<?=$URL_ROOT?>assets/ico/descuento.svg" type="image/svg+xml">Descuentos</object> Capacidad de Suministro</div>
              <object data="<?=$URL_ROOT?>assets/ico/doc-reloj-bl.svg" type="image/svg+xml">Entrega rápida</object>
              <object data="<?=$URL_ROOT?>assets/ico/grua-bl.svg" type="image/svg+xml">Instalación</object>
              <object data="<?=$URL_ROOT?>assets/ico/lock-bl.svg" type="image/svg+xml">Área privada</object>
              <object data="<?=$URL_ROOT?>assets/ico/ok-bl.svg" type="image/svg+xml">OK</object>
              <object data="<?=$URL_ROOT?>assets/ico/persona-bl.svg" type="image/svg+xml">Equipo</object>
              <object data="<?=$URL_ROOT?>assets/ico/prod-nuevo-bl.svg" type="image/svg+xml">Producto nuevo</object>
              <object data="<?=$URL_ROOT?>assets/ico/sello-bl.svg" type="image/svg+xml">Sello de calidad</object>
            </div>
            <!-- /icos -->
            <form>
              <fieldset>
                <legend>Form Legend</legend>
                <div class="form-group"> <label for="formGroupExampleInput">Example label</label> <input class="form-control" id="formGroupExampleInput" placeholder="Example input"> </div> <div class="form-group"> <label for="formGroupExampleInput2">Another label</label> <textarea class="form-control" id="formGroupExampleInput2"></textarea> </div>

                <div class="form-inline"> <label class="sr-only" for="inlineFormInput">Name</label> <input class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput" placeholder="Jane Doe"> <label class="sr-only" for="inlineFormInputGroup">Username</label> <div class="input-group mb-2 mr-sm-2 mb-sm-0"> <div class="input-group-addon">@</div> <input class="form-control" id="inlineFormInputGroup" placeholder="Username"> </div> <div class="form-check mb-2 mr-sm-2 mb-sm-0"> <label class="form-check-label"> <input class="form-check-input" type="checkbox"> Remember me </label> </div>  
              </div>
              <div class="input-group">
                <span class="input-group-btn">
                    <button type="button" class="btn opt-corporate1 btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                        -
                    </button>
                </span>
                <input type="text" name="quant[1]" class="form-control input-number" value="1" min="1" max="10">
                <span class="input-group-btn">
                    <button type="button" class="btn opt-corporate1 btn-number" data-type="plus" data-field="quant[1]">
                        +
                    </button>
                </span>
            </div>
              <div class="custom-control custom-radio">
                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                <label class="custom-control-label" for="customRadio1">Toggle this custom radio</label>
              </div>
              <div class="custom-control custom-radio">
                <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                <label class="custom-control-label" for="customRadio2">Or toggle this other custom radio</label>
              </div>
              <div class="checks">
                <div class="btn-group" data-toggle="buttons"> <label class="btn opt-corporate1 active"> <input type="checkbox" checked=""> Checkbox 1 (pre-checked) </label> <label class="btn btn-primary opt-corporate1"> <input type="checkbox"> Checkbox 2 </label> </div>
              <div class="btn-group" data-toggle="buttons"> <label class="btn opt-corporate1 active"> <input type="radio" name="options" id="option1" checked=""> Radio 1 (preselected) </label> <label class="btn opt-corporate1"> <input type="radio" name="options" id="option2"> Radio 2 </label> 
              </div>
              <div class="form-group">
                <label for="exampleFormControlSelect1">Example select</label>
                <select class="form-control" id="exampleFormControlSelect1">
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                </select>
              </div>
              <div class="precio">
                500€ <span>PVPR</span>
              </div>
              <div class="form-actions ">
                <button type="button" class="btn btn-corporate1">Primary</button>
                <button type="button" class="btn btn-corporate2">Secondary</button>
                <button type="button" class="btn btn-outline-primary">Primary</button>
                <button type="button" class="btn btn-secondary">Secondary</button>
                <button type="button" class="btn btn-outline-secondary">Secondary</button>
                <button type="button" class="btn btn-success">Success</button>
                <button type="button" class="btn btn-primary btn-lg">Large button</button>
                <button type="button" class="btn btn-lg btn-primary" disabled="">Primary button</button> <a href="#" class="btn btn-primary btn-lg disabled" role="button" aria-disabled="true">Primary link</a>
                </div>
              </div>
            </fieldset>
          </form>
            <!-- /form -->

            <div class="captionImage left">
              <img class="left" src="<?=$URL_ROOT?>assets/img/no-image.png" alt="Sample Pic 1">
              <p class="caption">This is an image caption.</p>
            </div>


            <!-- listados -->
            <div class="product-list">

              <div class="row">
                <div class="col-lg-3">
                  <div class="producto">
                    <div class="img-box">
                      <object data="<?=$URL_ROOT?>assets/img/producto/arqueta-A1.svg" type="image/svg+xml">Arqueta F</object>
                    </div>
                    <h2><?=__("CANALETA RENFE ");?></h2>
                    <p><?=__("En nuestras instalaciones fabricamos cunetas de hormigón en masa, así como de hormigón armado para mayor resistencia.");?></p>
                    <div class="product-list-actions">
                      <button type="button" class="btn btn-corporate1">+info</button>
                      <button type="button" class="btn btn-corporate2">Añadir</button>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="producto">
                    <div class="img-box">
                      <object data="<?=$URL_ROOT?>assets/img/producto/arqueta-A1.svg" type="image/svg+xml">Arqueta F</object>
                    </div>
                    <h2><?=__("CANALETA RENFE ");?></h2>
                    <p><?=__("En nuestras instalaciones fabricamos cunetas de hormigón en masa, así como de hormigón armado para mayor resistencia.");?></p>
                    <div class="product-list-actions">
                      <button type="button" class="btn btn-corporate1">+info</button>
                      <button type="button" class="btn btn-corporate2">Añadir</button>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="producto">
                    <div class="img-box">
                      <object data="<?=$URL_ROOT?>assets/img/producto/arqueta-A1.svg" type="image/svg+xml">Arqueta F</object>
                    </div>
                    <h2><?=__("CANALETA RENFE ");?></h2>
                    <p><?=__("En nuestras instalaciones fabricamos cunetas de hormigón en masa, así como de hormigón armado para mayor resistencia.");?></p>
                    <div class="product-list-actions ">
                      <button type="button" class="btn btn-corporate1">+info</button>
                      <button type="button" class="btn btn-corporate2">Añadir</button>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="producto">
                    <div class="img-box">
                      <object data="<?=$URL_ROOT?>assets/img/producto/arqueta-A1.svg" type="image/svg+xml">Arqueta F</object>
                    </div>
                    <h2><?=__("CANALETA RENFE ");?></h2>
                    <p><?=__("En nuestras instalaciones fabricamos cunetas de hormigón en masa, así como de hormigón armado para mayor resistencia.");?></p>
                    <div class="product-list-actions">
                      <button type="button" class="btn btn-corporate1">+info</button>
                      <button type="button" class="btn btn-corporate2">Añadir</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-lg-3">
                  <div class="producto">
                    <div class="img-box">
                      <object data="<?=$URL_ROOT?>assets/img/producto/arqueta-A1.svg" type="image/svg+xml">Arqueta F</object>
                    </div>
                    <h2><?=__("CANALETA RENFE ");?></h2>
                    <p><?=__("En nuestras instalaciones fabricamos cunetas de hormigón en masa, así como de hormigón armado para mayor resistencia.");?></p>
                    <div class="product-list-actions">
                      <button type="button" class="btn btn-corporate1">+info</button>
                      <button type="button" class="btn btn-corporate2">Añadir</button>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="producto">
                    <div class="img-box">
                      <object data="<?=$URL_ROOT?>assets/img/producto/arqueta-A1.svg" type="image/svg+xml">Arqueta F</object>
                    </div>
                    <h2><?=__("CANALETA RENFE ");?></h2>
                    <p><?=__("En nuestras instalaciones fabricamos cunetas de hormigón en masa, así como de hormigón armado para mayor resistencia.");?></p>
                    <div class="product-list-actions">
                      <button type="button" class="btn btn-corporate1">+info</button>
                      <button type="button" class="btn btn-corporate2">Añadir</button>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="producto">
                    <div class="img-box">
                      <object data="<?=$URL_ROOT?>assets/img/producto/arqueta-A1.svg" type="image/svg+xml">Arqueta F</object>
                    </div>
                    <h2><?=__("CANALETA RENFE ");?></h2>
                    <p><?=__("En nuestras instalaciones fabricamos cunetas de hormigón en masa, así como de hormigón armado para mayor resistencia.");?></p>
                    <div class="product-list-actions ">
                      <button type="button" class="btn btn-corporate1">+info</button>
                      <button type="button" class="btn btn-corporate2">Añadir</button>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="producto">
                    <div class="img-box">
                      <object data="<?=$URL_ROOT?>assets/img/producto/arqueta-A1.svg" type="image/svg+xml">Arqueta F</object>
                    </div>
                    <h2><?=__("CANALETA RENFE ");?></h2>
                    <p><?=__("En nuestras instalaciones fabricamos cunetas de hormigón en masa, así como de hormigón armado para mayor resistencia.");?></p>
                    <div class="product-list-actions">
                      <button type="button" class="btn btn-corporate1">+info</button>
                      <button type="button" class="btn btn-corporate2">Añadir</button>
                    </div>
                  </div>
                </div>
                </div>
                <!-- /.row -->
              </div>
                <!-- /.product-list -->
                <div class="proyect-list">
                 <div class="row">
                  <div class="col-lg-3">
                    <div class="proyecto">
                      <a href="#">
                        <h3><?=__("1992 - Expo de Sevilla");?></h3>
                        <p><?=__("Autovía A92, Nacional N-IV, Ave Madrid-Sevilla, Aeropuerto de Sevilla y por supuesto el propio recinto de la expo92");?></p>
                      </a>
                      <img src="<?=$URL_ROOT?>assets/img/proyectos/proyecto1.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="proyecto">
                      <a href="#">
                        <h3><?=__("1992 - Expo de Sevilla");?></h3>
                        <p><?=__("Autovía A92, Nacional N-IV, Ave Madrid-Sevilla, Aeropuerto de Sevilla y por supuesto el propio recinto de la expo92");?></p>
                      </a>
                      <img src="<?=$URL_ROOT?>assets/img/proyectos/proyecto1.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="proyecto">
                      <a href="#">
                        <h3><?=__("1992 - Expo de Sevilla");?></h3>
                        <p><?=__("Autovía A92, Nacional N-IV, Ave Madrid-Sevilla, Aeropuerto de Sevilla y por supuesto el propio recinto de la expo92");?></p>
                      </a>
                      <img src="<?=$URL_ROOT?>assets/img/proyectos/proyecto1.jpg" alt="">
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="proyecto">
                      <a href="#">
                        <h3><?=__("1992 - Expo de Sevilla");?></h3>
                        <p><?=__("Autovía A92, Nacional N-IV, Ave Madrid-Sevilla, Aeropuerto de Sevilla y por supuesto el propio recinto de la expo92");?></p>
                      </a>
                      <img src="<?=$URL_ROOT?>assets/img/proyectos/proyecto1.jpg" alt="">
                    </div>
                  </div>
                </div> <!-- /.row -->
                </div>
                 <!-- /.proyect-list -->
       
      </div><!-- /.container -->



    </main>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>