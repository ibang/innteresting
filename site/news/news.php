<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-news.php");
$title = __("Media - INNTERESTING");
$description = __("Keep up to date with our latest news");
$keywords = __("news, events,");
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="news" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?>">
	<?require("{$DOC_ROOT}site/includes/header.php")?>
	<main>
	<div class="full-container">
		<div class="container">
			<article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure">
                    <img src="<?=$URL_ROOT?>assets/img/media/hero.jpg" class="">
                  </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=__("Media");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
          </article>
		</div>
	</div>

<div class="full-container bg-gray pt-3 pt-md-3 pb-3">
    <div class="container containerMedia">
    
        <?php
        $categories = $db->get_news_categories($language, $s1);
        if( !empty($categories) ) {
            foreach ( $categories as $cat ) {
                /*  MOSTRAMOS CADA CATEGORIA POR SEPARADO */
            ?>
	           	<h2 class="newsMenu h3 ml-4 mt-4"><?= $cat['category' ]; ?></h2>
	            <div class="noticia shadow bg-white ml-4 mr-4 mb-2">
	                    

	                    <?php
	                    $newsCategory = $db->get_news_from_category($language, $cat['category'] );
	                    $newsCategory = $db->get_news_images_list($newsCategory);
	                    $newsCategory = $db->get_news_files_list($newsCategory);
	                    
	                    foreach ( $newsCategory as $new ) {
	                        /*  POR CADA CATEGORIA MOSTRAMOS TODAS SUS NOTICIAS */
	                    ?>

	                    <article class="item-news">
	                    	<div class="row">
	                    		<?if ($new["image1"]){?>
	                    		<div class="col-md-5">

	                    				<p class="photo"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->{$new['type']}->url?>/<?=$new["slug"]?>/"><img class="img-responsive" src="<?=$URL_ROOT?>uploads/news/<?=$new["image1"]?>" alt="<?=($new["alt-image1"]?htmlspecialchars(strip_tags($new["alt-image1"])):str_replace("\"","'", $new["headline"]))?>" /></a></p>

	                    		</div>
	                    		<?}?>

	                    		<div class="col-md-7">
	                    			<div class="content pt-3 pr-3 pl-2">
	                    				<p class="date"><span <?if($new['type']=='eventos' AND !empty($new["date"])){?>class="date2"<?}?>><?=parsedate($new["date"],$language);?></span><?if($new['type']=='eventos' AND !empty($new["location"])){?> <span class="place"><?=$new["location"];?></span><?}?></p>
	                    				<h3 class="title mb-1"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->{$new['type']}->url?>/<?=$new["slug"]?>/"><?=$new["headline"]?></a></h3>
		                    			<p><?=$new["intro"]?> <a class="link text-secondary float-right" href="<?=$URL_ROOT_BASE?>/<?=$txt->{$new['type']}->url?>/<?=$new["slug"]?>/"><?=__("Read more");?> <i class="fa fa-angle-right"></i></a></p>
	                    			</div>
	                    		</div>
	                    	</div>
	                    </article>

	                    <?php
	                    }
	                    ?>

	            </div>

            <?php
            }
        }
        ?>

    </div>
</div> <!-- /.full-container -->

</main>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>