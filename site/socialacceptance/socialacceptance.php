<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-index.php");
require_once("../../site/php/inc-privacy.php");
require_once("../../site/php/inc-socialacceptance.php");
$title=__("Social Acceptance - INNTERESTING");
$description=__("INNTERESTING research and innovation activities will be driven by the opinion of stakeholders involved providing expert advice to ensure consistency in the project outcomes");
$js[]="ekko-lightbox.min.js";
$js[]="historia.js";
$js[]="about.js";
$js[]="scroll.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
$css[]="historia.css";
if(CAPTCHA_ACTIVE == '3' OR (CAPTCHA_ACTIVE == '1' AND (!isset($validate["recaptcha"]) OR $validate["recaptcha"]!=1) )){
	$js_lang[]="recaptcha3.js";
}
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="nosotros" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior">
  <?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
    <div class="full-container">
      <div class="container">
        <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure">
                    <img src="<?=$URL_ROOT?>assets/img/stakeholders/social-hero.jpg" class="">
                  </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=__("SOCIAL ACCEPTANCE");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
          </article>
      </div>
    </div>   
    <div class="container pt-3 pt-md-3">
      <article class="pl-1 pr-1 pl-md-3 pr-md-3">
        <p class="textDestacado col-md-6 p-0 mb-2"><?=__("A part of the fifth objective of the INNTERESTING project is to <strong>improve social acceptance of the newly developed designs, concepts and testing methods.</strong> For a better understanding of the concept of social acceptance a literature review has been performed.");?></p>
        <p><?=__("The complete overview with key messages from the literature review on social acceptance towards wind energy, including the definition and aspects of, and influences on social acceptance, and some examples of good practices, are included in");?> <a href="https://www.innterestingproject.eu/downloads/d1-1-technical-environmental-and-social-requirements-of-the-future-wind-turbines-and-lifetime-extension.pdf" target="_blank"><?=__("deliverable 1.1.");?></a></p>
        <p><?=__("The two main key messages found in literature are: ");?></p>
        <div class="row">
          <div class="col-md-6 mt-3 mb-2">
            <ul class="list-arrow p-0 mr-md-4">
              <li class="textDestacado"><?=__("The overall acceptance at society level needs to increase, rather than at the level of individual projects");?></li>
            </ul>
          </div>
          <div class="col-md-6 mt-3 mb-2">
            <ul class="list-arrow p-0 mr-md-4">
              <li class="textDestacado"><?=__("There is a lack of comprehensive and systematic review to identify common findings and outstanding research questions. Important insights are produced, yet knowledge gaps remain");?></li>
            </ul>
          </div>
        </div>
        <p><?=__("Based on the literature review, <strong>no major difference is to be expected for social acceptance</strong> of the host community of a wind energy project <strong>with or without the solutions developed within INNTERESTING.</strong>");?></p>
        <div class="bg-greenLight row p-3">
          <ul class="list-arrow p-0 col-md-6">
              <li><?=__("Research on the <strong>influence of wind farm life time on social acceptance is lacking.</strong>");?></li>
              <li><?=__("<strong>Less maintenance</strong> and thus less idle time may lead to a stronger acceptance.");?></li>
              <li><?=__("Further research to learn more about the difference in social acceptance of wind farms with an extended lifetime versus repowering is needed, as reports have shown that <strong>repowering can be highly effective in improving the social acceptance</strong> of a wind farm.");?></li>
            </ul>
          <img src="<?=$URL_ROOT?>assets/img/stakeholders/social-acceptance.svg" alt="" class="col-md-5 offset-md-1" style="max-width: 380px;">
        </div>
        <p class="mt-3"><?=__("On the matter of social acceptance of wind energy projects more comprehensive studies of several years have been dedicated than the literature review done for INNTERESTING, therefore we are eager to engage interest groups, stakeholders, researchers, and others on this matter. ");?></p>
      </article>
    </div> 
    <div class="full-container bg-gray mt-3 mt-md-4" id="form">
      <div class="container pt-3 pt-md-5 pb-4">
          <div class="col-md-7 mb-3">
            <h2 class=""><?=__("Share your vision");?></h2>
            <p class="mt-2 mb-3"><?=__("Please feel free to share your knowledge and/or feedback on social acceptance of wind energy with the INNTERESTING project partners via the fields below. ");?></p>
            <? /*<ul class="list-arrow p-0 mb-3">
              <li><a href="https://www.innterestingproject.eu/events/1st-innteresting-stakeholder-consultation-meeting/"><?=__("Or join us at the first INNTERESTING stakeholder consultation meeting on 10 September 2020!");?></a></li>
            </ul>*/ ?>
            <?require("{$DOC_ROOT}site/includes/widget-socialacceptanceform.php")?>
          </div>
      </div>
    </div>
    <div class="container pb-2">
      <article class="banner bannerStakeholders mt-1 mb-1 p-2 p-md-4" style="background-image: url(<?=$URL_ROOT?>assets/img/stakeholders/banner-technological.jpg);background-size: cover; background-position: center">
        <div class="text">
          <h3 class="text-gray text-white"><?=__("KNOW MORE ABOUT THE TECHNOLOGICAL APPROACH");?></h3>
          <p class="text-white"><?=__("Discover the key phase of the project, where we will lead the way.");?></p>
        </div>
        <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->technological->url?>/" class="btn btn-corporate1 shadow"><?=__("TECHNOLOGICAL <br>
APPROACH");?></a>
      </article>
    </div>
  </main>
  <?require("{$DOC_ROOT}site/includes/footer.php")?>
  <script></script>
</body>
</html>