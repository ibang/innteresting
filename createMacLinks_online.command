#!/bin/sh
# shell script to create simbolic links to files
ABSPATH=$(cd "$(dirname "$0")"; pwd)
echo "Creando enlaces simbólicos de archivos .htacces_online y de config.php"
# El directorio actual
echo $ABSPATH
# read -n1 -r -p "Press any key to continue..."
echo "Creando enlace a .htaccess..."
cd $ABSPATH
file=".htaccess"
if [ -f "$file" ]
then
	echo "El fichero $file existe, bórralo antes de crear el enlace simbólico"
	read -n1 -r -p "Press any key to close..."
	exit
else
	#echo "$file not found."
	ln -s htaccess_online.txt .htaccess
fi
# read -n1 -r -p "Press any key to continue..."
echo "Creando enlace a .htaccess en cms..."
cd cms/
if [ -f "$file" ]
then
	echo "El fichero $file del cms existe, bórralo antes de crear el enlace simbólico"
	read -n1 -r -p "Press any key to close..."
	exit
else
	#echo "$file not found."
	ln -s htaccess_online.txt .htaccess
fi
# read -n1 -r -p "Press any key to continue..."
echo "Creando enlace a config.php..."
cd ..
cd php/
file="config.php"
if [ -f "$file" ]
then
	echo "El fichero $file existe, bórralo antes de crear el enlace simbólico"
	read -n1 -r -p "Press any key to close..."
	exit
else
	#echo "$file not found."
	ln -s config_online.php config.php
fi
echo "Se han creado los enlaces simbólicos correctamente."
# read -n1 -r -p "Press any key to close..."
exit
