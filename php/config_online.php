<?
define('ENVIROMENT', 'online'); // local pruebas online

// connect data base
define( "DBNAME", "innteresting");
define( "DBHOST", "localhost");
define( "DBUSERNAME", "innteresting");
define( "DBUSERPASS", "gaE4bsvW");

define( "DB_DEBUG",false);  // true for queries results

if (!defined("CMSDIRNAME")){
	define("CMSDIRNAME", "cms");
}

// ------ email address to send contact form (customer email)
define("ADMIN_EMAIL_ADDRESS", "p.berard@estia.fr");
define("ADMIN_EMAIL_ADDRESS2", "bernardo@promueve3.com");
define("ADMIN_EMAIL_ADDRESS3", "");
define("ADMIN_EMAIL_ADDRESS4", "");
define("ADMIN_EMAIL_ADDRESS5", "");
define("ADMIN_EMAIL_ADDRESS6", "");
define("ADMIN_EMAIL_ADDRESS7", "");
define("ADMIN_EMAIL_ADDRESS8", "");
define("ADMIN_EMAIL_ADDRESS_ADVISORY", "jirigoyen@clusterenergia.com");
define("ADMIN_EMAIL_ADDRESS_NEWSLETTER", "jirigoyen@clusterenergia.com");
define("ADMIN_EMAIL_ADDRESS_SOCIAL_ACCEPTANCE1", "innteresting@vito.be");
define("ADMIN_EMAIL_ADDRESS_SOCIAL_ACCEPTANCE2", "MOlave@ikerlan.es");


// ------- email address to send budget form ----/
define("BUDGET_EMAIL_ADDRESS", "p.berard@estia.fr");

//------- SMTP data to send FROM contact form to ADMIN EMAIL ADDRESS----- //

define('EMAILING_ACTIVE', '1');

define("SMTP_ACTIVE", '0');
define("SMTP_SECURE",""); // "" => ssl , "tls" => tls
define("SMTP_HOST","smtp.xxx.com");
define("SMTP_PORT","587"); // "587" => ssl, "465" => tls
define("SMTP_USERNAME","xx");
define("SMTP_PASSWORD","xx");
define("SMTP_ADDRESS","xx@innterestingproject.com");

//----- To USE SENDMAIL OR MAIL FUNCTION ---/
define("SENDMAIL_ACTIVE", '0');

//------- email address to use in FROM to send width SENDMAIL to USER a contact form email ------//
define("CONTACT_ADMIN_EMAIL_ADDRESS", "innterestingproject@innterestingproject.eu");
define("CONTACT_REPLY_TO_EMAIL_ADDRESS", "p.berard@estia.fr");

define("DEVELOPER_EMAIL_ADDRESS", "");
define("SASSLESS", "SASS"); // SASS for bootstrap 4 or LESS for bootstrap 3
define("SRCORCSS", "CSS"); // SRC in development for less or sass, CSS for production
define('CFGCURRENTVERSION', '201910011200'); //for avoid CSS and JS caching

//------- log options -------------//
define("LOGFILE", rtrim($_SERVER['DOCUMENT_ROOT'],"/").'/'.CMSDIRNAME.'/logs/debug.log'); // absolute PATH with LOG FILE NAME
define("CANLOG", '1'); // 1 => Can Log, Another string => Can't log

//------- GEO IP options ---------//
define("GEOIP_ACTIVE", '1');

//------- GOOGLE MAPS API KEY & ANALYTICS KEY
define("GOOGLEMAPS_KEY", ''); //producción???
define("GOOGLESTATICMAPS_KEY", ''); //producción???
define("GOOGLEANALYTICS_KEY", 'UA-167196933-1');  // producción
define("GOOGLETAGMANAGER_KEY", ''); //producción


//------- listing options ---------//
define("CMSLISTINGPAGESIZE", 20);
define("CMSSMALLLISTINGPAGESIZE", 50);
define("CMSMEDIUMLISTINGPAGESIZE", 100);
define("CMSBIGLISTINGPAGESIZE", 200);
define("CMSLISTEXTRAORDER", 10);

//------- Image options ----------//
define("PDFPREVIEWIMAGECROPHORIZONTAL", 300); //210 A4
define("PDFPREVIEWIMAGECROPVERTICAL", 424); //297 A4

// --- rest of images
define("IMAGECROPHORIZONTAL", 900); //900
define("IMAGECROPVERTICAL", 600); //600

//define("FRONTIMAGECROPHORIZONTAL", 1310); 
//define("FRONTIMAGECROPVERTICAL", 630); 

// --- news main image
define("NEWSIMAGECROPHORIZONTAL", 1200); 
define("NEWSIMAGECROPVERTICAL", 800); 

//define("PRODUCTSCATEGORIESSLIDERIMAGECROPHORIZONTAL", 1200); 
//define("PRODUCTSCATEGORIESSLIDERIMAGECROPVERTICAL", 500); 

//define("PRODUCTSCATEGORIESIMAGECROPHORIZONTAL", 1200); 
//define("PRODUCTSCATEGORIESIMAGECROPVERTICAL", 800);
 
//define("PRODUCTSRENDERIMAGECROPHORIZONTAL", 1200); 
//define("PRODUCTSRENDERIMAGECROPVERTICAL", 800); 

//define("PRODUCTSCATEGORIESRENDERIMAGECROPHORIZONTAL", 704); 
//define("PRODUCTSCATEGORIESRENDERIMAGECROPVERTICAL", 770); 

//define("PROJECTSHEADIMAGECROPHORIZONTAL", 1310); 
//define("PROJECTSHEADIMAGECROPVERTICAL", 630); 

define("DOWNLOADSCATEGORIESSLIDERIMAGECROPHORIZONTAL", 1200); 
define("DOWNLOADSCATEGORIESSLIDERIMAGECROPVERTICAL", 500); 

define("DOWNLOADSCATEGORIESIMAGECROPHORIZONTAL", 1200); 
define("DOWNLOADSCATEGORIESIMAGECROPVERTICAL", 800); 

//------- SESSION OPTIONS ---------//
define('COOKIENAME', '_CFGINNTERESTINGSESSID');
define('ADMINCOOKIENAME', '_ADMCFGINNTERESTINGSESSID');

//------ HTTP or HTTPS ---------//
define('BASE_PROTOCOL', 'https');


//---------------------------------//
define("HOSTDOMAIN", $_SERVER['HTTP_HOST']);
define("HOSTDOMAINPORT", ((strrpos($_SERVER['HTTP_HOST'], ':' )===false)?'':':'.substr($_SERVER['HTTP_HOST'], strrpos($_SERVER['HTTP_HOST'], ':' )+1)));

define('LOCALHOSTPORT', ':8887');

define("CFGNEWSBYPAGE", 4);
define("CFGPROJECTSBYPAGE", 12);
define("CFGDOWNLOADSBYPAGE", 16);


//---------- LANGUAGES MODE XML  / PO  / ALL   --------//
define("LANGUAGEMODE", 'ALL');
define("DIR_LOCALE","lang/locale");
define("POMODE","ARRAY"); // ARRAY -> load php array of translations, GETTEXT -> uses PHP gettext library
define("CANGENERATETRANSLATIONSARRAY", '1'); // 1 => can create php translations array if po file changes

//------ DOMINIOS QUE NO LLEVAN EN LA URL EL IDIOMA - RELACION DOMINIO - IDIOMA - CON PHP 7 SE PODRIA GUARDAR EN UNA CONSTANTE DEFINE UN ARRAY, EN VERSIONES ANTERIORES NO.--------//
class Constants {
	//private static $languageDomains = array('[DOMINIO].promueve3.com'=>'es-mx');
    private static $languageDomains = array('www.innterestingproject.eu'=>'en');
	private static $protocolDomains = array('www.innterestingproject.eu'=>'https'); // domain -> http or https, default http
    public static function getLanguageDomains() {
        return self::$languageDomains;
    }
	public static function getProtocolDomains() {
        return self::$protocolDomains;
    }
	//Get domain of a given language
	public static function getDomainLanguage($lang=''){
		$domainLanguages = array_flip(self::$languageDomains);
		if (!empty($domainLanguages[$lang])){
			return $domainLanguages[$lang];
		}
		return false;
		
	}
	//Get language of a given domain
	public static function getLanguage($domain=''){
		$langdomains = self::$languageDomains;
		if (!empty($domain) AND !empty($langdomains[$domain])){
			return $langdomains[$domain];
		}
		return false;
		
	}
	//Get Protocol of given domain
	public static function getProtocol($domain=''){
		$protocoldomains = self::$protocolDomains;
		if (empty($domain)){
			return false;
		}
		if (!empty($protocoldomains[$domain])){
			return $protocoldomains[$domain];
		}
		else{
			return 'http'; // default http
		}
		return false;
		
	}
}

//------- HTTP HOST URL ------------//
define("HTTPHOST", Constants::getProtocol($_SERVER['HTTP_HOST']).'://'.$_SERVER['HTTP_HOST']);
//-------- MAIN HOST URL ----------//
define("MAINHOST", BASE_PROTOCOL.'://www.innterestingproject.eu');

//------ RECAPTCHA ---- //
define("CAPTCHA_ACTIVE", '1'); //0 => inactiva, 1=> captacha 3 + 2, 2 => captcha 2, 3 => captcha 3
define("CAPTCHA_MODE", 'POST'); // CURL or POST if allow_url_fopen is false, use curl

//------ RECAPTCHA v2 ---- //
define("CAPTCHA_SECRET_KEY", "6LflnfAUAAAAAIpXL-_DgDakZMNn6azypdku8ske");
define("CAPTCHA_PUBLIC_SITE_KEY", "6LflnfAUAAAAAJz90rTtxvz5c3F4qNciCaJSL7Tc");
//

//------ RECAPTCHA v3 ---- //
define("CAPTCHA3_SECRET_KEY", "6LcgnfAUAAAAAJRiKywp0_0qDg-VNmFqyBro2ia8");
define("CAPTCHA3_PUBLIC_SITE_KEY", "6LcgnfAUAAAAANNq3JL4BXHXV_9FWKjK6_Rj7mzN");
define("CAPTCHA3_SCORE", 0.7);
//

//------ MAILCHIMP ---- //
define("MAILCHIMP_ACTIVE", "0");
define("MAILCHIMP_API_KEY", ""); // Mailchimp API key for innteresting SL ACCOUNT
define("MAILCHIMP_LIST_ID", ""); // innteresting SL List
// ---MAILCHIMP GROUP FIELDS DATA INTEREST/CATEGORIES FROM PLAYGROUND URL https://us1.api.mailchimp.com/playground/  -- //
define("MAILCHIMP_CONSENT_CATEGORY_ID", ""); // Category id for "consent" interest group
define("MAILCHIMP_CONSENT_STORE_DATA_ID", ""); // Interest id for "store data consent" checkbox field
define("MAILCHIMP_CONSENT_NEWSLETTER_ID", ""); // Interest id for "receive newsletter" checkbox field

// check php version 
define("PHPVERSION", (version_compare(phpversion(), '7', '<')?"5x":"")); //default 7
?>