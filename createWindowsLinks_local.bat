@echo off
title Creando enlaces simbólicos de archivos .htacces_local y de config.php
rem El directorio actual
echo %~dp0
rem pause
echo Creando enlace a .htaccess...
cd %~dp0
if exist .htaccess (
rem El fichero .htaccess exist
echo El fichero .htaccess existe, bórralo antes de crear el enlace simbólico
pause
exit
) else (
	mklink .htaccess htaccess_local.txt
)
rem pause
echo Creando enlace a .htaccess en cms...
cd cms/
if exist .htaccess (
rem El fichero .htaccess exist
echo El fichero .htaccess del cms existe, bórralo antes de crear el enlace simbólico
pause
exit
) else (
	mklink .htaccess htaccess_local.txt
)
rem pause
echo Creando enlace a config.php...
cd ..
cd php/
if exist config.php (
rem El fichero config.php exist
echo El fichero config.php existe, bórralo antes de crear el enlace simbólico
pause
exit
) else (
	mklink config.php config_local.php
)
echo Se han creado los enlaces simbólicos correctamente.
rem pause
exit