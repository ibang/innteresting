<?php
$error = 'js';
require_once("../../php/init.php");
header("Content-type: application/javascript");
?>
// --- get provinces with ajax and cache
var cache = {};
function getProvinces(countryId, callback) {
	// load provinces with ajax or from cache
	if(!cache[countryId]) {
        cache[countryId] = $.getJSON("?fn=getProvinces", 'selected='+countryId, "json").promise();
	}
	cache[countryId].done(callback);
}
// --- load provinces in select object, callback function
function loadProvinces(datos){
	if (datos['error']){
		//show error message
		alert(datos['error']);
	}
	else{
		//load provinces in select
		var provinces = datos['provinces'];
		$('#province').empty();
		for (var i=0; i<provinces.length; i++) {
		  $('#province').append('<option value="' + provinces[i].value + '">' + provinces[i].title + '</option>');
		}
		
		$('#province').prop('disabled', false);
		$('#province').prop('selectedIndex',0);
		$('#province').focus();
	}
		
}
	
$(function() {
	$('#country').on('change', function(){
    var selected = $(this).find("option:selected").val();
    if (selected=='ESP'){
		$('#province-content').removeClass('d-none');
		
		// load provinces with ajax
		getProvinces(selected, loadProvinces);
		
	}
	else{
		$('#province').prop('disabled', 'disabled');
		$('#province').prop('selectedIndex',0);
		$('#province').empty();
		$('#province-content').addClass('d-none');
	}
  });
});
