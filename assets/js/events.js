$(function() {
	$("a[data-event='link']").click(function(e){
		e.preventDefault();
		var category = $(this).data('category');
		var label = $(this).data('label');
		
		var href = $(this).attr('href');
		console.log ('link-clic,'+category+':'+label);
		gtag("event","link-clic", {
			'event_category': category,
			'event_label': label,
			'transport_type': 'beacon',
			'event_callback': function(){document.location = href;}
		});
	});
		
});		