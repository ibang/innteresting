<?php
$error = 'js';
require_once("../../php/init.php");
$_GET['p'] = isset($_GET['p'])?$_GET['p']:'';
$_GET['page'] = isset($_GET['page'])?$_GET['page']:0;
$_GET['i'] = isset($_GET['i'])?$_GET['i']:0;
//addToLog('PROJECTS.JS: '.print_r($_SERVER, true));
if (!empty($_GET['p'])){
	$article = $db->get_projects_sheet($_GET['p'],$language);
	$projects = $db->get_projects_images_list(array($article));
}
else{
	$projectspage=CFGPROJECTSBYPAGE;
	$projectscount=count($db->count_front_projects_list($language, $_GET['i']));
	$total_pages=ceil($projectscount/$projectspage);
	if ($_GET["page"]){$actual_page=$_GET["page"];}else{$actual_page=1;}
	$limit="LIMIT ".$projectspage*($actual_page-1)." , ".$projectspage;
	$projects=$db->get_front_projects_list($language, $limit, $_GET['i']);
	$projects = $db->get_projects_images_list($projects);
}
//addToLog('PROJECTOS JS: '.print_r($projects, true));
header("Content-type: application/javascript");
?>
$(function() {
	$('#filter').on('submit', function(e){
		e.preventDefault();  //prevent form from submitting
		var selindustry = $('#industry', $(this)).val();
		if (selindustry){
			selindustry = "<?=__('industria');?>"+"-"+selindustry+'/';
		}
		location.href = $(this).attr('action')+selindustry;
	});
});

function initialize() {
  var myLatlng = new google.maps.LatLng(38, -2);
  
	var myStyles =[
		{
			featureType: "administrative",
			elementType: "all",
			stylers: [
				{visibility: "off"}
			]
		},
		{
			featureType: "landscape",
			elementType: "all",
			stylers: [
				{visibility: "on"}
			]
		},
		{
			featureType: "poi",
			elementType: "all",
			stylers: [
				{visibility: "off"}
			]
		},
		{
			featureType: "road",
			elementType: "all",
			stylers: [
				{visibility:"off"}
			]
		},
		{
			featureType: "transit",
			elementType: "all",
			stylers: [
				{visibility:"off"}
			]
		},
		{
			featureType: "water",
			elementType: "all",
			stylers: [
				{"color": "#ffffff"}
			]
		},
		{
			elementType: "all",
			stylers: [
				{"invert_lightness": false},
				{"saturation": -100},
				{"lightness":  0}
			]
			
		}
	];
  
  
  var mapOptions = {
    zoom: 3,
    center: myLatlng,
    mapTypeControl: false,
    panControl: false,
    zoomControl: true,
    scaleControl: false,
    streetViewControl: false,
    scrollwheel: false,
    styles: myStyles 
  }
  
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
 	
	var image = {
	  url: '<?=HTTPHOST?><?=$URL_ROOT;?>assets/img/all/marker.png',
	  size: new google.maps.Size(14, 14),
	  origin: new google.maps.Point(0,0),
	  anchor: new google.maps.Point(7, 7)
	};
	
	google.maps.event.addListener(map, 'bounds_changed', checkLatitude);
	function checkLatitude(){
		var proj = map.getProjection();
		var bounds = map.getBounds();
		var sLat = map.getBounds().getSouthWest().lat();
		var nLat = map.getBounds().getNorthEast().lat();
		if (sLat < -85 || nLat > 85) {
			//gray areas are visible
			//alert('Gray area visible');
			//map.setOptions({draggable:false});
			//return to a valid position
			map.setCenter(myLatlng);
		}
	}
	
	<?if(!empty($projects)){?>
	var infowindow = new google.maps.InfoWindow({
		content: ''
	});
		<?foreach($projects as $current){?> 
			<?if(!empty($current['location'])){?>
	var marker<?=$current['code']?> = new google.maps.Marker({
		position: new google.maps.LatLng(<?=$current['location']?>),
		icon: image,
		title:"<?=addslashes($current['headline'])?>",
		url: "<?=HTTPHOST?><?=$URL_ROOT_BASE?>/<?=$txt->projectos->url?>/<?=$current["slug"]?>/",
		map: map
	});
			
	google.maps.event.addListener(marker<?=$current['code']?>, 'click', function() {
		var contentString<?=$current['code']?> = '<div class="mapwindow">'+
		'<?if (!empty($current["image1"])){?><img src="<?=HTTPHOST?><?=$URL_ROOT;?>uploads/projects/<?=$current["image1"]?>" class="image"><?}?>'+
		'<p class="title"><?=addslashes($current['headline'])?></p>'+
		'<p class="place"><?php echo addslashes($current['area']);?></p>'+
		'<p class="link"><a class="btn btn-sm" href="<?=$URL_ROOT_BASE?>/<?=$txt->proyectos->url?>/<?=$current["slug"]?>/"><span><?=__('Leer más')?></span></a></p>'+
		'</div>'; 
		infowindow.setContent(contentString<?=$current['code']?>);
		infowindow.open(map,marker<?=$current['code']?>);
		
		$('.gm-style-iw').not('.removed').prev('div').remove();
		$('.gm-style-iw').not('.removed').addClass('removed');
	});
	/*
	google.maps.event.addListener(marker<?=$current['code']?>, 'click', function() {
		window.location.href = this.url;
	});
	*/
			<?}?>
		<?}?>
	<?}?>
}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?=GOOGLEMAPS_KEY;?>' +
      '&callback=initialize';
  document.body.appendChild(script);
}

window.onload = loadScript;