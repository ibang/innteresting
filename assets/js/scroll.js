$(document).ready(function(){
	$('.scroll').click(function(){
	    $('html, body').animate({
	        scrollTop: $( $(this).attr('href') ).offset().top
	    }, 400);
		$(this).trigger("scroll:start");
	    return false;
	});
});