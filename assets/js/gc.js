// get Google Cookies and returns specific value
function _uGC(l, n, s){
	if (!l || l=="" || !n || n=="" || !s || s=="") return "-";
	var i, i2, i3, c="-";
	i = l.indexOf(n);
	i3 = n.indexOf("=")+1;
	if (i > -1){
		i2 = l.indexOf(s, i);
		if (i2 < 0){
			i2 = l.length;
		}
		c = l.substring((i+i3), i2);
	}
	return c;
}

function setCookie(name, value, days){
    var date = new Date();
    date.setTime(date.getTime() + (days*24*60*60*1000)); 
    var expires = "; expires=" + date.toGMTString();
    document.cookie = name + "=" + value + expires;
}
function getParam(p){
    var match = RegExp('[?&]' + p + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function getGclid(){
	var gclidcookievalue = (name = new RegExp('(?:^|;\\s*)gclid=([^;]*)').exec(document.cookie)) ? name.split(",")[1] : ""; 
	return gclidcookievalue;
}

var GCsource;
var GCmedium;
var GCterm;
var GCcontent;
var GCcampaign;
var GCgclid;

function loadGoogleCookies(){
	//load google cookies
	
	//first we explore query string for a gclid variable
	var gclid = getParam('gclid'); // is there a gclid get param??
	if(gclid){
		var gclsrc = getParam('gclsrc');
		if(!gclsrc || gclsrc.indexOf('aw') !== -1){
			setCookie('gclid', gclid, 90);
		}
	}

	//console.log("Guardando las variables con los datos de las cookies de Google 1.");
	var GoogleCookie = _uGC(document.cookie, '__utmz=', ';');
	GCsource	= 	_uGC(GoogleCookie, 'utmcsr=', '|');
	GCmedium	= 	_uGC(GoogleCookie, 'utmcmd=', '|');
	GCterm		= 	_uGC(GoogleCookie, 'utmctr=', '|');
	GCcontent	= 	_uGC(GoogleCookie, 'utmcct=', '|');
	GCcampaign	= 	_uGC(GoogleCookie, 'utmccn=', '|');
	GCgclid		= 	_uGC(GoogleCookie, 'utmgclid=', '|');
	
	//console.log('GCsource1 = '+ GCsource);
	//console.log('GCgclid1 = '+ GCgclid);
	
	if (GCgclid != "-"){
		GCsource = "google";
		GCmedium = 'cpc';
	}
	else{
		GCgclid = '';
	}
	
	//miramos si tenemos la cookie de gclid guardada de antes 
	if (!GCgclid){
		GCgclid = getGclid();
		if (GCgclid){
			GCsource = "google";
			GCmedium = 'cpc';
		}
	}
	
	
	console.log('GoogleCookie = '+ GoogleCookie);
	//console.log('GCsource = '+ GCsource);
	//console.log('GCmedium = '+ GCmedium);
	//console.log('GCterm = '+ GCterm);
	//console.log('GCcontent = '+ GCcontent);
	//console.log('GCcampaign = '+ GCcampaign);
	console.log('GCgclid = '+ GCgclid);
	
	//console.log("Cargando datos de cookies en campos del formulario 2.");
	if ( $( "form#form\\-contact input#googlecookie" ).length ) {
		$('form#form\\-contact input#googlecookie').val(""+GoogleCookie+"");
	}
	if ( $( "form#form\\-contact input#gclid" ).length ) {
		$('form#form\\-contact input#gclid').val(""+GCgclid+"");
	}
	if ( $( "form#form\\-contact input#gmode" ).length ) {
		$('form#form\\-contact input#gmode').val(""+GCcampaign+""); // (not provided) -> adwords, (direct) or (organic)
	}
}
	
//google cookies
$(window).on('load', function() {
	loadGoogleCookies();
});

$(document).ready(function(){
	$( "form#form\\-contact" ).submit(function( event ) {
		//if (all ok){
		loadGoogleCookies();
		return;
		//}
		// else{
		//$( "span" ).text( "Not valid!" ).show().fadeOut( 1000 );
		//event.preventDefault();
		//}
	});
});