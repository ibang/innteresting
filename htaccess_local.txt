Options -Indexes
Options +FollowSymlinks

RewriteEngine on

# Protejo los directorios .git y los archivos .git* para que no sean accesibles, tienen información del source code
RedirectMatch 404 /\.git

### Manu
# Attempt to load files from production if
# they're not in our local version
# RewriteCond %{REQUEST_FILENAME} !-d
# RewriteRule ^uploads/(.*) \
# https://www.innteresting.eu/uploads/$1 [NC,L]
### Fin Manu

# CONSTANTE PARA EL PROTOCOLO, HTTP OR HTTPS -----
RewriteCond %{HTTPS}s ^(on(s)|offs)$
RewriteRule ^ - [env=s:%2]
# FIN CONSTANTE ----------------------------------

# main domain (si no es www.innteresting.eu, www.xxx.fr, www.xxx.de, como ejemplo estos últimos de un addon domain sin idioma)
RewriteCond %{HTTP_HOST} !^www\.innteresting\.localhost\.com$ [NC]
RewriteCond %{HTTP_HOST} !^(|innteresting\.)localhost(|:8887|:8056)$ [NC]
# la siguiente la descomento en local
RewriteRule ^(.*)$ http://%1localhost%2/innteresting/$1 [R=301,L]


# index.php to / Cuando metan index.php lo quito para que se quede en / también para subcarpetas
RewriteCond %{THE_REQUEST} ^[A-Z]{3,9}\ /.*index\.php\ HTTP/
RewriteRule ^(.*)index\.php$ /innteresting/$1 [R=301,L]

#
# Trailing slash check Cuando no se ponga la barra al final, lo redirecciono 301 a la url con la barra al final.

# Don't fix direct file links
RewriteCond %{REQUEST_FILENAME} !-f

# Ni las requesturis de .../gracias.html, o cualquier otra que sea un archivo(punto algo) .html, .js, .css, .jpg, .png, etc.. 
RewriteCond %{REQUEST_URI} !\.(.+)$

RewriteCond %{REQUEST_URI} !(.*)/$
RewriteRule ^(.*)$ http%{ENV:s}://%{HTTP_HOST}/innteresting/$1/ [L,R=301]

#<IfModule mod_expires.c>
#ExpiresActive On
#ExpiresDefault "access plus 1 month"
#ExpiresByType image/x-icon "access plus 1 year"
#ExpiresByType image/gif "access plus 1 month"
#ExpiresByType image/png "access plus 1 month"
#ExpiresByType image/jpg "access plus 1 month"
#ExpiresByType image/jpeg "access plus 1 month"
#ExpiresByType text/css "access 1 month"
#ExpiresByType application/javascript "access plus 1 year"
#</IfModule>

# Redireccionar correo
# RewriteRule  ^correo/?$ https://login.microsoftonline.com/es [L] 
# RewriteRule  ^correo/(.*)$ https://login.microsoftonline.com/es [L] 

# Redirect 301 ----------------------------------------------------------

# REDIRECTS ESPAÑOL ------------------------------------------------

# REDIRECTS INGLES ------------------------------------------------

# REDIRECTS FRANCES --------------------------------------------------------

# Fin redirects 301

# ERROR HANDLING ----------------------------------------------------------

# Error 404 para imágenes/ archivos que no sean páginas o documentos
<filesmatch "\.(jpg|JPG|gif|GIF|png|PNG|css|ico)$">
ErrorDocument 404 "/assets/img/no-image.png"
</filesmatch>

# Resto de páginas

ErrorDocument 400 /innteresting/site/error.php?s1=error&error=400
ErrorDocument 401 /innteresting/site/error.php?s1=error&error=401
ErrorDocument 403 /innteresting/site/error.php?s1=error&error=403
ErrorDocument 404 /innteresting/site/error.php?s1=error&error=404
ErrorDocument 500 /innteresting/site/error.php?s1=error&error=500

# QUERY STRING - Creo una variable de entorno con el query string para poder añadirlo cuando quiera %{ENV:Qs}&
RewriteCond %{QUERY_STRING} ^(.+)$ [NC]
RewriteRule .? - [E=Qs:%1]   # Copy QueryString in Qs Enviroment variable

# PRIMERO LAS URLS DE DOMINIOS SIN IDIOMA

#--------------------------------------------------------------------------------------------

# Europa innterestingproject.eu

# --- URLS CON(AJAX) O SIN QUERY STRING

# -- OTHER PAGES --- #

# Para los dominios que no sean localhost me salto todas estas reglas que son aplicables únicamente a los dominios localhost
RewriteCond %{HTTP_HOST} !^www\.innteresting\.localhost\.com$ [NC]
RewriteCond %{HTTP_HOST} !^(|innteresting\.)localhost(|:8887|:8056)$ [NC]
RewriteRule .? - [S=34]   # me salto las siguientes 34 Reglas
RewriteRule ^(400|401|403|404|500)/$ /innteresting/site/error.php?s1=error&error=$1 [L]
RewriteRule ^project/$ /innteresting/site/project/project.php?s1=project [L]
RewriteRule ^advisory/$ /innteresting/site/advisory/advisory.php?%{ENV:Qs}&s1=advisory&s2=become [L]
RewriteRule ^advisory/sent/$ /innteresting/site/advisory/advisory.php?%{ENV:Qs}&s1=advisory&s2=become&status=success [L]
RewriteRule ^socialacceptance/$ /innteresting/site/socialacceptance/socialacceptance.php?%{ENV:Qs}&s1=advisory&s2=socialacceptance [L]
RewriteRule ^socialacceptance/sent/$ /innteresting/site/socialacceptance/socialacceptance.php?%{ENV:Qs}&s1=advisory&s2=socialacceptance&status=success [L]
RewriteRule ^sectorialnews/$ /innteresting/site/sectorialnews/sectorialnews.php?s1=sectorialnews [L]
RewriteRule ^technological/$ /innteresting/site/technological/technological.php?s1=technological [L]
RewriteRule ^technological/cs1-novel-pitch-bearing/$ /innteresting/site/technological/case1.php?s1=technological&s2=cs [L]
RewriteRule ^technological/cs2-next-generation-gearbox/$ /innteresting/site/technological/case2.php?s1=technological&s2=cs [L]
RewriteRule ^technological/cs3-pitch-bearing-lifetime-extension/$ /innteresting/site/technological/case3.php?s1=technological&s2=cs [L]

RewriteRule ^relatedprojects/$ /innteresting/site/relatedprojects/relatedprojects.php?s1=relatedprojects [L]

RewriteRule ^contact/$ /innteresting/site/contact/contact.php?%{ENV:Qs}&s1=contact [L]
RewriteRule ^contact/sent/$ /innteresting/site/contact/contact.php?%{ENV:Qs}&s1=contact&status=success [L]
RewriteRule ^downloads/$  /innteresting/site/downloads/downloads.php?%{ENV:Qs}&s1=downloads [L]
RewriteRule ^downloads/([0-9]+)/$  /innteresting/site/downloads/downloads.php?%{ENV:Qs}&s1=downloads&page=$2 [L]
RewriteRule ^downloads/([^\.]+)\.([^/]+)$  /innteresting/site/downloads/downloads.php?s1=downloads&s2=download&slug=$1 [L]
RewriteRule ^media/$ /innteresting/site/news/news.php?s1=news [L]
RewriteRule ^news/category/([^/]+)/([0-9]+)/$ /innteresting/site/news/category.php?s1=news&s2=category&category=$1&page=$2 [L]
RewriteRule ^news/category/([^/]+)/$ /innteresting/site/news/category.php?s1=news&s2=category&category=$1 [L]
RewriteRule ^news/([0-9]+)/$ /innteresting/site/news/news.php?s1=news&page=$1 [L]
RewriteRule ^news/([^/]+)/$ /innteresting/site/news/add.php?s1=news&s2=new&slug=$1 [L]
RewriteRule ^events/$ /innteresting/site/events/events.php?s1=events [L]
RewriteRule ^events/category/([^/]+)/([0-9]+)/$ /innteresting/site/events/category.php?s1=events&s2=category&category=$1&page=$2 [L]
RewriteRule ^events/category/([^/]+)/$ /innteresting/site/events/category.php?s1=events&s2=category&category=$1 [L]
RewriteRule ^events/([0-9]+)/$ /innteresting/site/events/events.php?s1=events&page=$1 [L]
RewriteRule ^events/past/$ /innteresting/site/events/events.php?s1=events&s2=past [L]
RewriteRule ^events/past/([0-9]+)/$ /innteresting/site/events/events.php?s1=events&page=$1&s2=past [L]
RewriteRule ^events/([^/]+)/$ /innteresting/site/events/add.php?s1=events&s2=event&slug=$1 [L]
RewriteRule ^legal-notice/$ /innteresting/site/legal/legal.php?s1=legal [L]
RewriteRule ^privacy-policy/$ /innteresting/site/privacy/privacy.php?s1=privacy [L]
RewriteRule ^cookies-policy/$ /innteresting/site/cookies/cookies.php?s1=cookies [L]
RewriteRule ^assets/js/projects\.js$ /innteresting/assets/js/projects\.js\.php [L]
RewriteRule ^assets/js/contact\.js$ /innteresting/assets/js/contact\.js\.php [L]
RewriteRule ^assets/js/recaptcha3\.js$ /innteresting/assets/js/recaptcha3\.js\.php [L]
RewriteRule ^assets/js/newsletter\.js$ /innteresting/assets/js/newsletter\.js\.php [L]
RewriteRule ^$ /innteresting/site/?s1=home [L]
RewriteRule ^sent/$ /innteresting/site/?%{ENV:Qs}&s1=home&status=success [L]

# -------------------------------------------------------------------------------------------

# AHORA LAS URLS DE DOMINIOS CON IDIOMA


RewriteRule ^(en)(|-[a-z]{2})/(400|401|403|404|500)/$ /innteresting/site/error.php?s1=error&error=$2 [L]

RewriteRule ^en(|-[a-z]{2})/mail_test/$ /innteresting/site/mail_test.php [L]

# páginas en Ingles ----------------------------------------------------------


# --- URLS CON(AJAX) O SIN QUERY STRING
# -- OTHER PAGES --- #

    # home
RewriteRule ^en(|-[a-z]{2})/home/$ /innteresting/site/home/home.php?s1=home [L]

	# project
RewriteRule ^en(|-[a-z]{2})/project/$ /innteresting/site/project/project.php?s1=project [L]

	# advisory
RewriteRule ^en(|-[a-z]{2})/advisory/$ /innteresting/site/advisory/advisory.php?%{ENV:Qs}&s1=advisory [L]
RewriteRule ^en(|-[a-z]{2})/advisory/sent/$ /innteresting/site/advisory/advisory.php?%{ENV:Qs}&s1=advisory&status=success [L]

    # social acceptance
RewriteRule ^en(|-[a-z]{2})/socialacceptance/$ /innteresting/site/socialacceptance/socialacceptance.php?%{ENV:Qs}&s1=socialacceptance [L]
RewriteRule ^en(|-[a-z]{2})/socialacceptance/sent/$ /innteresting/site/socialacceptance/socialacceptance.php?%{ENV:Qs}&s1=socialacceptance&status=success [L]

	# sectorial news
RewriteRule ^en(|-[a-z]{2})/sectorialnews/$ /innteresting/site/sectorialnews/sectorialnews.php?s1=sectorialnews [L]

    # technological
RewriteRule ^en(|-[a-z]{2})/technological/$ /innteresting/site/technological/technological.php?s1=technological [L]

    # technological -> study cases
RewriteRule ^en(|-[a-z]{2})/technological/cs1-novel-pitch-bearing/$ /innteresting/site/technological/case1.php?s1=technological [L]

    # technological -> study cases
RewriteRule ^en(|-[a-z]{2})/technological/cs2-next-generation-gearbox/$ /innteresting/site/technological/case2.php?s1=technological [L]

    # technological -> study cases
RewriteRule ^en(|-[a-z]{2})/technological/cs3-pitch-bearing-lifetime-extension/$ /innteresting/site/technological/case3.php?s1=technological [L]

    # relatedprojects
RewriteRule ^en(|-[a-z]{2})/relatedprojects/$ /innteresting/site/relatedprojects/relatedprojects.php?s1=relatedprojects [L]

	# Contact form (with query string[if ajax] and without querys string)
RewriteRule ^en(|-[a-z]{2})/contact/$ /innteresting/site/contact/contact.php?%{ENV:Qs}&s1=contact [L]
RewriteRule ^en(|-[a-z]{2})/contact/sent/$ /innteresting/site/contact/contact.php?%{ENV:Qs}&s1=contact&status=success [L]

    # Descargas Home
RewriteRule ^en(|-[a-z]{2})/downloads/$  /innteresting/site/downloads/downloads.php?%{ENV:Qs}&s1=downloads [L]
RewriteRule ^en(|-[a-z]{2})/downloads/([0-9]+)/$  /innteresting/site/downloads/downloads.php?%{ENV:Qs}&s1=downloads&page=$2 [L]

	# Descargas Fichero
RewriteRule ^en(|-[a-z]{2})/downloads/([^\.]+)\.([^/]+)$  /innteresting/site/downloads/downloads.php?s1=downloads&s2=download&slug=$1 [L]

	# Media corner Home
RewriteRule ^en(|-[a-z]{2})/media/$ /innteresting/site/news/news.php?s1=news [L]

	# Media corner Categoria
RewriteRule ^en(|-[a-z]{2})/news/category/([^/]+)/([0-9]+)/$ /innteresting/site/news/category.php?s1=news&s2=category&category=$2&page=$3 [L]

	# Media corner Paginacion
RewriteRule ^en(|-[a-z]{2})/news/category/([^/]+)/$ /innteresting/site/news/category.php?s1=news&s2=category&category=$2 [L]
RewriteRule ^en(|-[a-z]{2})/news/([0-9]+)/$ /innteresting/site/news/news.php?s1=news&page=$2 [L]

	# Media corner Ficha
RewriteRule ^en(|-[a-z]{2})/news/([^/]+)/$ /innteresting/site/news/add.php?s1=news&s2=new&slug=$2 [L]

	# Eventos Home
RewriteRule ^en(|-[a-z]{2})/events/$ /innteresting/site/events/events.php?s1=events [L]

	# Eventos Categoria
RewriteRule ^en(|-[a-z]{2})/events/category/([^/]+)/([0-9]+)/$ /innteresting/site/events/category.php?s1=events&s2=category&category=$2&page=$3 [L]

	# Eventos Paginacion
RewriteRule ^en(|-[a-z]{2})/events/category/([^/]+)/$ /innteresting/site/events/category.php?s1=events&s2=category&category=$2 [L]
RewriteRule ^en(|-[a-z]{2})/events/([0-9]+)/$ /innteresting/site/events/events.php?s1=events&page=$2 [L]

	# Eventos pasados
RewriteRule ^en(|-[a-z]{2})/events/past/$ /innteresting/site/events/events.php?s1=events&s2=past [L]
RewriteRule ^en(|-[a-z]{2})/events/past/([0-9]+)/$ /innteresting/site/events/events.php?s1=events&page=$2&s2=past [L]

	# Eventos Ficha
RewriteRule ^en(|-[a-z]{2})/events/([^/]+)/$ /innteresting/site/events/add.php?s1=events&s2=event&slug=$2 [L]
		
	# Legal Advice
RewriteRule ^en(|-[a-z]{2})/legal-notice/$ /innteresting/site/legal/legal.php?s1=legal [L]

	# Politica privacidad
RewriteRule ^en(|-[a-z]{2})/privacy-policy/$ /innteresting/site/privacy/privacy.php?s1=privacy [L]

	# Cookies Policy
RewriteRule ^en(|-[a-z]{2})/cookies-policy/$ /innteresting/site/cookies/cookies.php?s1=cookies [L]

	# PROJECTS MAPS JS
RewriteRule	 ^en(|-[a-z]{2})/assets/js/projects\.js$ /innteresting/assets/js/projects\.js\.php [L]
		
	# CONTACT MAPS JS
RewriteRule	 ^en(|-[a-z]{2})/assets/js/contact\.js$ /innteresting/assets/js/contact\.js\.php [L]

	# RECAPCHA3 JS
RewriteRule	 ^en(|-[a-z]{2})/assets/js/recaptcha3\.js$ /innteresting/assets/js/recaptcha3\.js\.php [L]

	# NEWSLETTER JS
RewriteRule	 ^en(|-[a-z]{2})/assets/js/newsletter\.js$ /innteresting/assets/js/newsletter\.js\.php [L]


# páginas en Español ----------------------------------------------------------

# páginas Français ----------------------------------------------------------

# Ruso ------------------------------------------------------------

# Türkçe ----------------------------------------------------------

# Euskara ----------------------------------------------------------

# General ----------------------------------------------------------
RewriteRule ^(en)(|-[a-z]{2})-php/(.*)$ /innteresting/site/php/$3 [L]
RewriteRule ^(en)(|-[a-z]{2})/(.+)$ /innteresting/site/$3 [L]
RewriteRule ^(en)(|-[a-z]{2})/$ /innteresting/site/index.php?s1=home [L]

