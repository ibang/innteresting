<?php return array (
  'domain' => NULL,
  'plural-forms' => 'nplurals=2; plural=(n != 1);',
  'messages' => 
  array (
    '' => 
    array (
      '' => 
      array (
        0 => 'Project-Id-Version: Durable
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2019-09-18 12:25+0200
PO-Revision-Date: 2019-09-18 12:25+0200
Language: en
X-Generator: Poedit 2.2
X-Poedit-Basepath: ../../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: cms/php
X-Poedit-SearchPath-2: assets/js
X-Poedit-SearchPathExcluded-0: .git
X-Poedit-SearchPathExcluded-1: alerts
X-Poedit-SearchPathExcluded-2: lang
X-Poedit-SearchPathExcluded-3: uploads
X-Poedit-SearchPathExcluded-4: assets/css
X-Poedit-SearchPathExcluded-5: assets/fonts
X-Poedit-SearchPathExcluded-6: assets/ico
X-Poedit-SearchPathExcluded-7: assets/img
X-Poedit-SearchPathExcluded-8: assets/less
X-Poedit-SearchPathExcluded-9: cms/alerts
X-Poedit-SearchPathExcluded-10: cms/assets
X-Poedit-SearchPathExcluded-11: cms/lang
X-Poedit-SearchPathExcluded-12: cms/logs
X-Poedit-SearchPathExcluded-13: cms/site
X-Poedit-SearchPathExcluded-14: cms/uploads
X-Poedit-SearchPathExcluded-15: backup
',
      ),
      'industria' => 
      array (
        0 => '',
      ),
      'Leer más' => 
      array (
        0 => '',
      ),
      'Challenge - DURABLE' => 
      array (
        0 => '',
      ),
      'The project responds to the need to shift the actual paradigm of the RE sector by transforming technological, institutional, industrial and social framework.' => 
      array (
        0 => '',
      ),
      'source: ESTIA' => 
      array (
        0 => '',
      ),
      'the CHALLENGE' => 
      array (
        0 => '',
      ),
      'DURABLE responds to the need to shift the actual paradigm of the RE sector by transforming technological, institutional, industrial and social framework in the Atlantic area.' => 
      array (
        0 => '',
      ),
      'overview' => 
      array (
        0 => '',
      ),
      'Renewable sources as means to a low carbon economy are a key issue for territorial sustainable development in the EU and for the mitigation of climate change that has strong consequences in Atlantic region. ' => 
      array (
        0 => '',
      ),
      'The Atlantic area is now below the average European Union’s share of energy from renewable sources in gross final energy consumption, still far from Paris Agreement 2020-2030 targets and below the reference European regions in a critical and strategic field. ' => 
      array (
        0 => '',
      ),
      'Looking at the electricity system as a whole, a total of about 28.1 GW of new power generation capacity were installed in the European Union last year. In terms of new net capacity, wind power was first with 15 GW, followed by solar PV 5.9 GW.' => 
      array (
        0 => '',
      ),
      'Climate & energy package' => 
      array (
        0 => '',
      ),
      '20% cut in <strong>greenhouse gas</strong> emissions (from 1990 levels)' => 
      array (
        0 => '',
      ),
      '20% of EU energy from <strong>renewables</strong>' => 
      array (
        0 => '',
      ),
      '20% improvement in <strong>energy efficiency</strong>' => 
      array (
        0 => '',
      ),
      'Climate & energy framework' => 
      array (
        0 => '',
      ),
      'At least 40% cuts in greenhouse gas emissions (from 1990 levels)' => 
      array (
        0 => '',
      ),
      'At least 32% share for renewable energy' => 
      array (
        0 => '',
      ),
      'At least 32.5% improvement in energy efficiency' => 
      array (
        0 => '',
      ),
      'Share of energy from renewable sources in the EU Member States (%, 2017)' => 
      array (
        0 => '',
      ),
      'Atlantic countries' => 
      array (
        0 => '',
      ),
      'M€ budget' => 
      array (
        0 => '',
      ),
      'partners' => 
      array (
        0 => '',
      ),
      'MONTHS (2019-2022)' => 
      array (
        0 => '',
      ),
      'associated entities' => 
      array (
        0 => '',
      ),
      'Objective' => 
      array (
        0 => '',
      ),
      'Atlantic regions need to shift the actual paradigm of the RE sector by transforming technological, institutional, industrial and social framework in the Atlantic area. DURABLE, led by ESTIA, will help to overcome one of main barriers in the use of renewables.' => 
      array (
        0 => '',
      ),
      'The goal of DURABLE is to accelerate the performance of renewable energies through the validation and demonstration of aeropsace technologies applied in robotics for operation and maintenance activities of wind and solar energy systems. The application of this technology will automate inspection and repair tasks, reducing costs and favoring production.' => 
      array (
        0 => '',
      ),
      'The final phase of the project will be focused on the design of pilot operations accounting for the final technologies used and tested in the other WPs and the characteristics of the host wind and solar installations. ' => 
      array (
        0 => '',
      ),
      '“We must do everything we can to accelerate renewables if we are to meet the climate objectives of the Paris Agreement”' => 
      array (
        0 => '',
      ),
      'Aerospace and robotic technologies together with composites and additive manufacturing are developing technologies that may help to produce full innovative solutions for more cost efficient energy production.' => 
      array (
        0 => '',
      ),
      'One of the main barriers for the acceleration of deployment of renewable energies related to the costs of Operation & Maintenance activities, which are estimated to be around 20-25% of the Levelized Cost of Energy (LCoE).' => 
      array (
        0 => '',
      ),
      'DURABLE seeks to transfer previous developed aerospace technologies applied in robotics for operation and maintenance activities in wind and solar energy systems.' => 
      array (
        0 => '',
      ),
      'KNOW MORE ABOUT DURABLE' => 
      array (
        0 => '',
      ),
      'Discover the work packages and the partners which are collaborating.' => 
      array (
        0 => '',
      ),
      'THE PROJECT' => 
      array (
        0 => '',
      ),
      'Contact | Durable' => 
      array (
        0 => '',
      ),
      'If you want to know more about DURABLE get in touch with us.' => 
      array (
        0 => '',
      ),
      'Contact' => 
      array (
        0 => '',
      ),
      'DURABLE' => 
      array (
        0 => '',
      ),
      'This project is co-financed by the Interreg Atlantic Area Programme through the European Regional Development Fund.' => 
      array (
        0 => '',
      ),
      'Challenge' => 
      array (
        0 => '',
      ),
      'Project' => 
      array (
        0 => '',
      ),
      'Roadmap' => 
      array (
        0 => '',
      ),
      'Technologies' => 
      array (
        0 => '',
      ),
      'News & events' => 
      array (
        0 => '',
      ),
      'Contact us' => 
      array (
        0 => '',
      ),
      'Follow us' => 
      array (
        0 => '',
      ),
      'Twitter' => 
      array (
        0 => '',
      ),
      'Durable' => 
      array (
        0 => '',
      ),
      'challenge' => 
      array (
        0 => '',
      ),
      'PROJECT' => 
      array (
        0 => '',
      ),
      'roadmap' => 
      array (
        0 => '',
      ),
      'technologies' => 
      array (
        0 => '',
      ),
      'news & events' => 
      array (
        0 => '',
      ),
      'Particular' => 
      array (
        0 => '',
      ),
      'Empresa' => 
      array (
        0 => '',
      ),
      'Regístrate' => 
      array (
        0 => '',
      ),
      'Home - DURABLE' => 
      array (
        0 => '',
      ),
      'DURABLE will apply drones and robots to accelerate the performance of wind and solar power in the Atlantic region.' => 
      array (
        0 => '',
      ),
      'source: IST' => 
      array (
        0 => '',
      ),
      'Advanced technology to boost the use of renewable energy.' => 
      array (
        0 => '',
      ),
      'The European project DURABLE will apply drones and robots to accelerate the performance of wind and solar power in the Atlantic region.' => 
      array (
        0 => '',
      ),
      'The CHALLENGE' => 
      array (
        0 => '',
      ),
      'Drones and maintenance robots for the promotion of renewable energies' => 
      array (
        0 => '',
      ),
      'DURABLE aims to boost the competitivity of the local renewable energy (RE) sector in the Atlantic area, by improving regional energy production conditions to mitigate global warming. To achieve that goal 13 European partners will work together to meet the objectives.' => 
      array (
        0 => '',
      ),
      'The challenge' => 
      array (
        0 => '',
      ),
      'TECHNOLOGIES' => 
      array (
        0 => '',
      ),
      'DURABLE will identify and gather the most suitable technologies for the improvement of the production of solar and wind energy, adapting them to tasks that reduce operational and maintenance costs.' => 
      array (
        0 => '',
      ),
      'Unmanned Aerial<br> Vehicle (UAV)' => 
      array (
        0 => '',
      ),
      'Nondestructive<br> Testing (NDT)' => 
      array (
        0 => '',
      ),
      'Additive<br> manufacturing<br> (AM)' => 
      array (
        0 => '',
      ),
      'Unmanned<br> Ground Vehicle<br> (UGV)' => 
      array (
        0 => '',
      ),
      'Augmented Reality<br> (AR) and Virtual<br> Reality (VR)' => 
      array (
        0 => '',
      ),
      'Automated<br> composites repair<br> technology' => 
      array (
        0 => '',
      ),
      'NEWS&EVENTS' => 
      array (
        0 => '',
      ),
      'Keep updated with our latest news section.' => 
      array (
        0 => '',
      ),
      'All news' => 
      array (
        0 => '',
      ),
      'New' => 
      array (
        0 => '',
      ),
      'Event' => 
      array (
        0 => '',
      ),
      'PARTNERS' => 
      array (
        0 => '',
      ),
      'Meet the partners participating in the project.' => 
      array (
        0 => '',
      ),
      'the PROJECT' => 
      array (
        0 => '',
      ),
      'Back to' => 
      array (
        0 => '',
      ),
      'News' => 
      array (
        0 => '',
      ),
      'Events' => 
      array (
        0 => '',
      ),
      'Share' => 
      array (
        0 => '',
      ),
      'Noticias sobre' => 
      array (
        0 => '',
      ),
      'Toda la información sobre' => 
      array (
        0 => '',
      ),
      'Home' => 
      array (
        0 => '',
      ),
      'Category' => 
      array (
        0 => '',
      ),
      'News about' => 
      array (
        0 => '',
      ),
      'Previous' => 
      array (
        0 => '',
      ),
      'Next' => 
      array (
        0 => '',
      ),
      'News & events - DURABLE' => 
      array (
        0 => '',
      ),
      'Keep up to date with our latest news and events.' => 
      array (
        0 => '',
      ),
      'news, events,' => 
      array (
        0 => '',
      ),
      'source: DCU' => 
      array (
        0 => '',
      ),
      'SECTORIAL NEWS & PROJECT EVENTS' => 
      array (
        0 => '',
      ),
      'UPCOMMING EVENTS' => 
      array (
        0 => '',
      ),
      'PAST EVENTS' => 
      array (
        0 => '',
      ),
      'SECTORIAL NEWS' => 
      array (
        0 => '',
      ),
      'PROJECT EVENTS' => 
      array (
        0 => '',
      ),
      'PAST&nbspEVENTS' => 
      array (
        0 => '',
      ),
      'Read more' => 
      array (
        0 => '',
      ),
      'Project - DURABLE' => 
      array (
        0 => '',
      ),
      'DURABLE brings together experts from different fields through seven work packages, with the objective of boosting renewable energy in the Atlantic area.' => 
      array (
        0 => '',
      ),
      'source: LORTEK' => 
      array (
        0 => '',
      ),
      'DURABLE brings together experts from different fields and countries through seven work packages, with the objective of boosting renewable energy in the Atlantic area.' => 
      array (
        0 => '',
      ),
      'Work packages' => 
      array (
        0 => '',
      ),
      'DURABLE is splitted in seven work packages: coordination (WP1), communication (WP2), capitalization (WP3), joint mapping of technologies and expected needs (WP4), adaptation of technologies for navigation and surveillance (WP5), adaptation of technologies for maintenance and repair (WP6) and, finally, Solar and Wind case studies (WP7).' => 
      array (
        0 => '',
      ),
      'WP1' => 
      array (
        0 => '',
      ),
      'Coordination' => 
      array (
        0 => '',
      ),
      'WP2' => 
      array (
        0 => '',
      ),
      'Communication' => 
      array (
        0 => '',
      ),
      'WP3' => 
      array (
        0 => '',
      ),
      'Capitalization' => 
      array (
        0 => '',
      ),
      'WP4' => 
      array (
        0 => '',
      ),
      'Joint mapping of technologies and expected need for O&M in solar and wind energy production' => 
      array (
        0 => '',
      ),
      'WP5' => 
      array (
        0 => '',
      ),
      'Adaptation of technologies for navigation and surveillance' => 
      array (
        0 => '',
      ),
      'Transfer of key enhance maintenance solutions <br> Lab and field tests with the technologies and combinations' => 
      array (
        0 => '',
      ),
      'WP6' => 
      array (
        0 => '',
      ),
      'Adaptation of technologies for maintenance and repair' => 
      array (
        0 => '',
      ),
      'WP7' => 
      array (
        0 => '',
      ),
      'Mock-up and solution operation on pilot´s sites: <strong>PV & wind case studies</strong>' => 
      array (
        0 => '',
      ),
      'The project is formed by a consortium that brings together 13 partners from the 5 Atlantic countries. In addition, other 6 associated entities participate through an Advisory Board.' => 
      array (
        0 => '',
      ),
      'www.estia.fr' => 
      array (
        0 => '',
      ),
      'Superior School of Advanced Industrial Technologies' => 
      array (
        0 => '',
      ),
      '92, allée Théodore Monod<br>
                Technopole Izarbel - Bâtiment ESTIA 1<br>
                64210 Bidart (FRANCE)' => 
      array (
        0 => '',
      ),
      '<p>ESTIA (Superior School of Advanced Industrial Technologies) is the Engineering School of the Chamber of Commerce and Industry (CCI) of the city of Bayonne, in the French Basque Country. ESTIA has been providing education and training for 20 years, in the area of industrial technologies.</p>
					 <p>ESTIA has strong partnerships with other European schools: University of Bordeaux I (France), School of Engineers of Bilbao, UPV-EHU, Mondragon University in Spain and the University of Cranfield, Wolverhampton, & Salford in the United Kingdom. It also maintains collaborations with several centres and networks.</p>
					 <p>ESTIATECH, is the entity in charge of value-added research and technology transfer, fosters relationships between ESTIA and businesses looking for technological solutions, skills and training for their innovative projects, and stimulates partner-oriented research.</p>
					 <p>ESTIATECH is a partner of the various ESTIA platforms; PEPSS (HMI and usage), ENERGEA (renewable energy), SIMECOMP (simulation and modelling). COMPOSITADOUR (technical platform specialized in automated processes for implementing composite materials), ADDIMADOUR is a technological centre of excellence, currently being set up, which is centred on Additive Manufacturing processes (specifically LMD-P, LMD-W).</p>
					 <p>Two platforms from ESTIA participate in the project:</p>
					 <p><strong>PEPSS</strong>, specialized in Human-Systems Interactions. It also develops research activities in augmented and virtual reality with a dedicated prototyping platform.</p> 
					 <p><strong>Compositadour</strong>, a technical platform specialized in the robotized processes for placement of composite materials.</p>
					 <p>ESTIA will be the <strong>coordinator of DURABLE,</strong> therefore it will lead the management of the project, ensuring the proper execution of the planned activities according to time schedule, expected results and budget allocated. It will also be <strong>responsible of capitalization</strong> of the project, guaranteeing an efficient dissemination and promotion for the outputs to assert the replicability and technology transfer and to maximize geographical coverage and visibility of the project achievements. Additionally, it will participate in the <strong>adaptation of technologies for navigation and surveillance</strong>, by leading an task focused on defining a virtual and augmented reality cockpit to control a UAV or a mobil robot proof of concept. It will also lead the activity related to adaptation of technologies for maintenance and repair. The main tasks includess:</p>
					 <ul>
						<li>Automated composites repair technology evaluation by analyse the potential use of automated composites repair technology (e.g. water jet technology) leaded by ESTIA.</li>
						<li>Additive manufacturing (AM) to repair of broken spare parts.</li>
						<li>Prediction algorithms for maintenance forecast.</li>
						<li>Virtual and augmented reality for using robots in persistent manual tasks leaded by ESTIA.</li>
						<li>Setting for an UAV platform for enlarged endurance in PV plants.</li>
					 </ul>' => 
      array (
        0 => '',
      ),
      'www.aleriontec.com' => 
      array (
        0 => '',
      ),
      'Alerion' => 
      array (
        0 => '',
      ),
      'Calle Etxaide 10 bajo<br>
                  20005 Donostia - San Sebastian,<br>Gipuzkoa (SPAIN)' => 
      array (
        0 => '',
      ),
      '<p>Alerion comprises engineers with research and industry experience in developing high performance drones for extreme environments, in high-performance computational software and in computer vision. Since 2016, Alerion has been offering custom automated industrial inspection solutions for wind energy infrastructures using its patent-pending laser navigation technology that permits high-precision navigation up-close to structures. It offers fully automated relative navigation and real-time data analytics around complex structures and high-wind environments.</p>
						<p>In DURABLE, Alerion <strong>will participate in the adaptation of technologies for maintenance repair and in the demo cases,</strong> since Alerion UAVs will be used in the case studies. The role in the project therefore will be Robot integrator and software provider for:</p>
						<ul>
							<li>Relative navigation software and embedded computation framework.</li>
							<li>Own SDK for seamless payload integration.</li>
							<li>Efficient test-driven development workflow for software development and simulations Outdoor facilities for testing in scale wind turbine.</li>
							<li>Design of embedded software framework for navigation, integration of different payloads, and data analysis.</li>
							<li>Integration of Alerion&acute;s autopilot with different payloads and robots.</li>
							<li>Testing and validation of different robots.</li>
						</ul>' => 
      array (
        0 => '',
      ),
      'www.catec.aero' => 
      array (
        0 => '',
      ),
      'Advanced Center for Aerospace Technologies (CATEC)' => 
      array (
        0 => '',
      ),
      'Parque Tecnológico y Aeronáutico de Andalucía <br> C/ Wilbur y Orville Wright 19 <br>41309 La Rinconada, Sevilla (SPAIN)' => 
      array (
        0 => '',
      ),
      '<p>FADA-CATEC is a Center for Advanced Aerospace Technologies with a large experience developing technological solutions and applications with UAS and aerial robotics, together with high expertise in NDT for inspection in multiple sectors. Its background is based on 3 main research areas: avionics and systems, robotics and automation, and materials and processes.</p>
						<p>In DURABLE, FADA-CATEC will lead tasks devoted to <strong>the adaptation of technologies for navigation and surveillance solutions to be implemented in O&amp;M inspections</strong>, apart of their contribution to the rest of the tasks of the project. FADA-CATEC is a key partner to develop and adapt successful technologies from aerospace sector to create robotic solutions for NDT inspections. Its main activities are as follows:</p>
						<ul>
							<li>Thermographic and visual inspection of wind blades using UAVs, including defects or damages.</li>
							<li>Contact inspection by drones for renewable energy infrastructure using ultrasonic and/or eddy-current techniques.</li>
							<li>Intelligent navigation to facilitate inspection activities with drones, for example automatic detection and avoidance obstacles.</li>
						</ul>' => 
      array (
        0 => '',
      ),
      'www.clusterenergia.com' => 
      array (
        0 => '',
      ),
      'Cluster Energía - Basque Energy Cluster' => 
      array (
        0 => '',
      ),
      'S. Vicente, 8. Edificio Albia II, 4ª plta B. Dcha.<br>48001 Bilbao,<br>Bizkaia (SPAIN)' => 
      array (
        0 => '',
      ),
      '<p>The Basque Energy Cluster (BEC) is a non-profit association with over 160 members, leading companies of the Basque energy sector, R&amp;D organisations and public administration bodies involved in the energy field, with a special focus on solar and wind energy.</p>
						<p>In DURABLE, BEC will be <strong>responsible for the implementation of the project&rsquo;s Communication Strategy</strong>, designed to make the best use of the project&rsquo;s results by assuring that its outputs are available to and used by the target audience. Among the different activities considered are:</p>
						<ul>
							<li>Communication plan development.</li>
							<li>Design of the corporate image and communication material.</li>
							<li>Development of website and social media.</li>
							<li>Media presence and publications.</li>
							<li>Organization of events with relevant stakeholders.</li>
							<li>Social awareness: Renewable energy acceptance campaigns for general public to foster clean energy adoption.</li>
							<li>Participation in conferences and events involving the whole value chain.</li>
							<li>BEC also has a significant participation in WP3 &lsquo;Project Capitalization&rsquo;, where it will leverage its renewable energy cluster network throughout Europe, and in WP4 &lsquo;Joint Mapping of Technologies&rsquo;, as focal point to interact with its several associates working in solar and wind O&amp;M.</li>
						</ul>' => 
      array (
        0 => '',
      ),
      'www.corporaciontecnologica.com' => 
      array (
        0 => '',
      ),
      'Corporación Tecnológica de Andalucía' => 
      array (
        0 => '',
      ),
      'C/Albert Einstein, s/n
                    <br>Edificio INSUR, 4ª pta. PCT Cartuja
                    <br>41092 Sevilla (SPAIN)' => 
      array (
        0 => '',
      ),
      '<p>Technological Corporation of Andalusia (CTA) is a private foundation that was born from a public-private partnership and for more than 13 years has been supporting R&amp;D activities through financing, mentoring and cooperation with main Andalusian stakeholders, emerging as a singular multi-sectorial, innovation cluster. For the past 13 years, more than 670 R&amp;D business projects have been funded by CTA with more than &euro;M168. Aerospace-Productive processes and Energy-Environment are two of the seven main industrial sectors in which CTA is focused.</p>
						 <p>CTA will participate in DURABLE by <strong>creating a joint mapping of technologies and expected needs for O&amp;M in solar and wind energy production</strong>. The aim is to identify and match necessities of the solar&amp;wind energy producers in O&amp;M actions and the most innovative solutions available (aerospace, robotics, composites and additive manufacturing) for sectorial transfer. It will therefore engage academia, Industry and Public ES partners. Additionally, CTA will also accomplish the following tasks:</p>
						 <ul>
							<li>Perform management activities.</li>
							<li>Dissemination and communication activities.</li>
							<li>Lead Technology Transfer activities to joint available technologies with solar and wind energy stakeholders needs.</li>
							<li>Participation at Project Capitalization activities.</li>
						 </ul>' => 
      array (
        0 => '',
      ),
      'www.dcu.ie' => 
      array (
        0 => '',
      ),
      'Dublin City University' => 
      array (
        0 => '',
      ),
      'Room S357, School of Electronic Engineering<br>
                      Dublin City University,<br>
                      Dublin 9 (IRELAND)' => 
      array (
        0 => '',
      ),
      '<p>DCU is a research university with experience in testing the feasibility of the use of Radio Emission Spectroscopy (RES) techniques combined with radio uplinks to drones. It has initial trials on commercial off-the-shelf units to be followed by custom design and build of low-power consumption systems.</p>
							 <p>In DURABLE, DCU <strong>will participate in the adaptation of technologies for navigation and surveillance</strong>, and it will develop different tasks:</p>
							 <ul>
								<li>Investigation of type of wind turbine control electronics, which will be targeted for the use of RES.</li>
								<li>Set up simplified version of the RES system, onshore trials of the system to confirm which spectral regions can be used to monitor RF emissions from turbine control electronics undergoing simulated false fault conditions.</li>
								<li>Testing of efficacy of antenna choices including vertical and horizontal antennas.</li>
								<li>Design of Experiment (DoE) trials on onshore turbine electronics systems.</li>
								<li>Confirmation by correlation with supplementary techniques Investigation and trials of data capture software for RF spectral analysis.</li>
								<li>Design and build of low-power consumption Field Programmable Gate Array (FPGA) board system for offshore.</li>
							 </ul>' => 
      array (
        0 => '',
      ),
      'espana.edp.com' => 
      array (
        0 => '',
      ),
      'EDP' => 
      array (
        0 => '',
      ),
      '<p>EDP &ndash; Energias de Portugal is an integrated energy utility, with a global presence that includes operations in Europe (especially focused in Portugal and Spain but with relevant positions in France, Belgium, Italy, Romania, Poland and UK), in the United States and in Brazil. Throughout its 40-year-history, EDP has transformed from Portugal&rsquo;s incumbent electricity company to a major multinational energy company, producing, distributing and marketing energy (electricity and gas) worldwide. EDP has experienced particularly remarkable growth in the area of renewable energy over the last 10 years to the point of currently standing as the world&rsquo;s fourth largest wind energy operator.</p>
							 <p>In electricity generation, EDP has a total installed capacity of 27 GW, 70% of which in Renewable Energy (wind, hydro and solar) which reflects the group&rsquo;s decarbonization efforts over the years. The company is currently serving more than 11 million customers (most of them in electricity) in the business of electricity supply in Portugal, Spain and Brazil.</p>
							 <p>EDP CNET, the partner in Project DURABLE, centralizes the Group&rsquo;s R&amp;D activities and is established inside EDP Labelec &ndash; EDP&rsquo;s laboratorial facilities and technical excellence center. Among other areas, EDP CNET is very active in the topics of smart grids and digitization, energy storage and energy management, renewable energies and flexibility in new energy systems. The Center has carried out work in several EU H2020 in all the energy value chain, adopting an integrated and sustainable approach towards disruptive solutions that empower its partners and bring value to the shareholders. EDP CNET has proven competencies in cross cutting topics, such as project management, use cases writing, architecture design, scalability and replicability analysis, new business models&rsquo; analysis and development, validation of technologies in laboratorial environment and demonstration in real conditions.</p>
							 <p>In DURABLE, EDP will perform the following tasks:</p>
							 <ul>
								<li>Perform project management activities, including progress assessment and monitoring and project meetings.</li>
								<li>Contribute to the definition of the future needs in O&amp;M in wind and solar plants.</li>
								<li>Focus on the validation/demonstration in the solar demo, providing access to a PV plant facility in Portugal for all the testing and support execution of the tests.</li>
								<li>Revise the conclusions and insights yielded from the field tests, contributing to the definition of the implementation of novel O&amp;M solutions.</li>
							 </ul>' => 
      array (
        0 => '',
      ),
      'www.lortek.es' => 
      array (
        0 => '',
      ),
      'Lortek' => 
      array (
        0 => '',
      ),
      'Arranomendia kalea 4A<br>
                      20240 Ordizia,<br>Gipuzkoa (SPAIN)' => 
      array (
        0 => '',
      ),
      '<p>IK4-LORTEK is a research center highly specialized in metal additive manufacturing processes using several high value materials for different applications. In the expertise of LORTEK different Metal Additive Manufacturing technologies are involved:</p>
							 <ul>
								<li>SLM (Selective Laser Melting)</li>
								<li>LMD (Laser Metal Deposition</li>
								<li>WAAM (Wire and Arc Additive Manufacturing)</li>
							 </ul>
							 <p>IK4-LORTEK is also expert in non-destructive testing (NDT) techniques based on thermography and acoustics to verify the integrity of the products and processes. Thus, LORTEK is an expert in advanced and digital manufacturing of joining and additive manufacturing process.</p>
							 <p>In DURABLE, the main tasks of IK4-LORTEK will be the <strong>study of the implementation of metal additive manufacturing</strong>, as well as the <strong>adaptation of technologies for surveillance</strong>. Its main tasks include:</p>
							 <ul>
							 	<li>Implementation of NDT based on thermography for the inspection of components to detect defects and its integration on UGV platforms. This contactless technique allows visualization of temperature distribution in the surfaces. This technique can be used to verify both surface defects in metals and inner defects in composites (Navigation and surveillance).</li>
							 	<li>Manufacturing and/or repairing of damaged or spare parts that need to be replaced using additive manufacturing processes such as SLM (Selective Laser Melting) or LMD (laser Metal Deposition) (Maintenance and repair).</li>
							 </ul>' => 
      array (
        0 => '',
      ),
      'www.ingeteam.com' => 
      array (
        0 => '',
      ),
      'Ingeteam S.A.' => 
      array (
        0 => '',
      ),
      'Parque Tecnológico, 106 <br>
                      48170 Zamudio,<br>Bizkaia (SPAIN)' => 
      array (
        0 => '',
      ),
      '<p>Ingeteam Service is the business unit (B.U.) taking part of this project and is part of the company &ldquo;Ingeteam Power Technology S.A.&rdquo;. Since 1999, Ingeteam Service has been providing all-inclusive O&amp;M (Operation and Maintenance) Services and special services to energy plants. At present, Ingeteam manages O&amp;M projects in 15 GW mainly in Onshore and Offshore Wind Farms (+5.600 wind turbines, 8,8 GW) and Photovoltaic Solar Plants (+550 plants, 6,1GW) all over the world in addition to other energy plants, employing more than 1.400 professionals. Ingeteam Service knowledge, coupled with agreements with manufacturers, enables the company to work on the implementation of maintenance programmes for assets under warranty, according to the technologist&rsquo;s specifications. Furthermore, being an independent supplier with multi-technology experience, Ingeteam Service has developed strategies based on Customised Services: the aim is to meet customer&rsquo;s requirements, by increasing plant availability and efficiency.</p>
							 <p>In Durable Project, the R&amp;D team from the Service Division in Ingeteam will work in several aspects of the projects offering the partners their extensive experience on Operation and Maintenance of renewable energy assets, but main research will focus on:</p>
							 <ul>
								<li>Guidance on the research partners for the focus on practical aspects and use for the novel technologies to develop.</li>
								<li>Research on prediction algorithms for maintenance forecast, mainly based on Complex Event Processing methods.</li>
								<li>On field assistance and collaboration for adaptation of prototypes for real condition applications.</li>
								<li>Data analysis and results assessments. Evaluation of effectiveness and advantages of the use of robotics over human workforce.</li>
							 </ul>' => 
      array (
        0 => '',
      ),
      'tecnico.ulisboa.pt' => 
      array (
        0 => '',
      ),
      'Instituto Superior Técnico' => 
      array (
        0 => '',
      ),
      'Institute for Systems and Robotics and Institute for Plasmas and Nuclear Fiusion<br>
                      Av. Rovisco Pais 1, 1049-001 Lisboa (PORTUGAL)' => 
      array (
        0 => '',
      ),
      '<p>IST, as part of Universidade de Lisboa, is the largest and most reputed school of engineering, R&amp;T in Portugal. The research will be conducted at the Institute for Systems and Robotics (ISR/IST), which develops state-of-the-art engineering solutions and projects with a societal impact.</p>
							 <p>IST will <strong>contribute on navigation and task planning for autonomous <u>ground</u> robots</strong>, as well as on cooperative systems, including fleets of heterogeneous unmanned vehicles. Active participant on transfer to commercial applications surveillance, inspection and cleaning of wind and solar farm plants.</p>
							 <p>UAVs can provide an aerial view of the solar farm stations, while UGVs can provide communication hubs and mobile recharging stations for UAV teams, besides contributing themselves for NDT inspection. IST-ID will use its prior knowledge on mobile robot (cooperative) navigation, motion and task planning and execution to transfer methods to surveillance and inspection of renewable energy plants. Its focus will be mostly on the UGVs and on the cooperation methods for consistent operation of the whole UAV + UGV team, by ensuring:</p>
							 <ul>
								<li>Coverage of the whole farm.</li>
								<li>Continuous and energy-wise autonomous operation of each UAV (by readily providing energy recharges on conveniently located dynamic and mobile ground UGV-based stations).</li>
								<li>Wide-area wireless communications by using the UGVs as repeaters in a reconfigurable communications network.</li>
							 </ul>' => 
      array (
        0 => '',
      ),
      'www.us.es' => 
      array (
        0 => '',
      ),
      'Universidad de Sevilla' => 
      array (
        0 => '',
      ),
      'Camino de los Descubrimientos, s/n<br> 41092 Sevilla,<br>Sevilla (SPAIN)' => 
      array (
        0 => '',
      ),
      '<p>The research Team and the University has long experience in research projects related to aerial robotics, and it shows a great expertise in drone technology, including inspection and maintenance applications. Even if it is mainly focused in the PV Plant inspections, it also offers expertise in safe navigation and compliant aerial manipulators (drone + arms) in contact inspection of wind turbines.</p>
							 <p>In DURABLE, US <strong>will participate in the <u>aerial</u> drone applications for inspections tasks</strong>, and it will carry out the following tasks:</p>
							 <ul>
								<li>Perception for navigation and for the near operation of the UAV to the inspected asset.</li>
								<li>Safe navigation for Contact Inspection Evolving embed of RTK Differential GNSS and its fusion with computer vision algorithms and/or LiDAR, to accommodate to the specific requirements of inspection.</li>
								<li>Obstacle detection and avoidance.</li>
								<li>Deployment of planning strategies on the assumption of non-attendant inspection with a multi-drone system, based on defining the right mechanisms for allocation of tasks, path planning, coordination and supervision.</li>
								<li>Digital Image Processing for failure detection in PV panels.</li>
							 </ul>' => 
      array (
        0 => '',
      ),
      'www.uwe.ac.uk' => 
      array (
        0 => '',
      ),
      'UWE Bristol' => 
      array (
        0 => '',
      ),
      'Frenchay Campus Coldharbour Lane<br>Bristol<br>BS16 1QYUnited Kingdom' => 
      array (
        0 => '',
      ),
      '<p>Bristol Robotics Laboratory (BRL) is the most comprehensive academic centre for multi-disciplinary robotics research in the UK. It is a collaborative partnership between the University of the West of England (UWE Bristol) and the University of Bristol, and home to a vibrant community of over 200 academics, researchers and industry practitioners. BRL has significant skills in many aspects of robotics relevant to DURABLE, including planning, navigation, haptics, remote sensing, swarm and multi-robot systems, remote operation, etc.</p>
							 <p>In DURABLE, UWE Bristol will participate in the aerial drone applications for inspections tasks. Their main role will concern the design, construction and deployment of bespoke UAV airframes and associated avionics and ground support equipment, including flight-test data logging and analysis capabilities to support the scientific goals of the project. More specifically, UWE Bristol shall provide:</p>
							 <ul>
								<li>Development of mission concepts of operation (CONOPS).</li>
								<li>Requirements capture and specification of system.</li>
								<li>Definition of UAV airframe, avionics, and sensors.</li>
								<li>Validation of these with the project partners.</li>
								<li>Construction of appropriate airframes and avionics systems.</li>
								<li>Initial flight-testing of UAVs and systems.</li>
								<li>Integration with project partner equipment.</li>
								<li>Development of appropriate processes and documentation in order to satisfy all regulatory requirements.</li>
								<li>Support of field trials and demonstrator systems.</li>
							 </ul>' => 
      array (
        0 => '',
      ),
      'www.valemo.fr' => 
      array (
        0 => '',
      ),
      'VALEMO S.A.S' => 
      array (
        0 => '',
      ),
      '213 cours Victor Hugo
                        <br>FR 33 323 Bègles cedex 
                        <br>(FRANCE)' => 
      array (
        0 => '',
      ),
      '<p>VALEMO (60 collaborators) is wind farm operator and service provider in the frame of renewable energy generation. VALEMO manages a portfolio of over 1000 MW of wind, photovoltaic and hydropower. With an extended background in onshore wind energy, VALEMO is now strongly committed into the growth of tidal and offshore wind energy production.</p>
								<p>VALEMO has started to develop R&amp;D activities in the field of Predictive Maintenance more than six years ago. Year after year this topic became strategical for the future development of VALEMO to become a major player in terms of renewable energy plant operator and maintenance services provider. Moreover, predictive maintenance and SHM tools become also essential in order to have the opportunity to reduce drastically Leverage Cost Of Energy supplied from on and off-shore power renewable plants. Therefore VALEMO provide services in terms of SHM design, monitoring and associated software development and deployment in renewable energy plant on and offshore.</p>
								<p>In DURABLE, VALEMO <strong>will participate </strong>by <strong>creating a joint mapping of technologies and expected needs for O&amp;M in solar and wind energy production </strong>with a more specific focus on wind farms. The objective is to identify through the different technologies developed and proposed among the partners those can propose an optimized service compared to existing technologies available on the market. The second step will consist in <strong>the adaptation of technologies for surveillance of the wind farms on one hand and for maintenance repair and in the demo cases</strong>.</p>
								<p>VALEMO will also participate as partner leading the <strong>evaluation of solutions on pilot sites</strong> with a specific focus on wind farms applying to:</p>
								<ul>
									<li>Use of UAV or robots for non destructive controls on tours, blades.</li>
									<li>Use of UAV for wind turbine plant survey, nacel misalignement.</li>
									<li>Use of UAV upfront for wind measurement.</li>
									<li>Use of UAV or robots for blade cleaning, deicing, repair and for tour cleaning, painting.</li>
								</ul>' => 
      array (
        0 => '',
      ),
      'PEOPLE' => 
      array (
        0 => '',
      ),
      'DURABLE connects a heterogeneous group of more than 30 professionals from different specialties.' => 
      array (
        0 => '',
      ),
      'KNOW MORE ABOUT THE ROADMAP' => 
      array (
        0 => '',
      ),
      'Discover the key phase of the project, where we will lead the way.' => 
      array (
        0 => '',
      ),
      'THE ROADMAP' => 
      array (
        0 => '',
      ),
      'Titulo  1' => 
      array (
        0 => '',
      ),
      'Titulo  2' => 
      array (
        0 => '',
      ),
      'Titulo  3' => 
      array (
        0 => '',
      ),
      'Titulo  4' => 
      array (
        0 => '',
      ),
      'Titulo  5' => 
      array (
        0 => '',
      ),
      'This is some lead body copy. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.' => 
      array (
        0 => '',
      ),
      'ul li' => 
      array (
        0 => '',
      ),
      'ol li' => 
      array (
        0 => '',
      ),
      'dl dt' => 
      array (
        0 => '',
      ),
      'dl dd' => 
      array (
        0 => '',
      ),
      'CANALETA RENFE ' => 
      array (
        0 => '',
      ),
      'En nuestras instalaciones fabricamos cunetas de hormigón en masa, así como de hormigón armado para mayor resistencia.' => 
      array (
        0 => '',
      ),
      '1992 - Expo de Sevilla' => 
      array (
        0 => '',
      ),
      'Autovía A92, Nacional N-IV, Ave Madrid-Sevilla, Aeropuerto de Sevilla y por supuesto el propio recinto de la expo92' => 
      array (
        0 => '',
      ),
      'Roadmap - DURABLE' => 
      array (
        0 => '',
      ),
      'A roadmap of the technologies and the needs of the project will be made in order then to identify, adapt and implement new prototypes in pilot tests.' => 
      array (
        0 => '',
      ),
      'ROADMAP' => 
      array (
        0 => '',
      ),
      'JOINT MAPPING' => 
      array (
        0 => '',
      ),
      'DURABLE will identify technologies with potential application to O&M of solar PV and wind energy, adapt them both to navigation & surveillance and maintenance & repair purposes, to finally implement them in two pilot sites for their validation. ' => 
      array (
        0 => '',
      ),
      'The project will target photovoltaic and wind energy sources to apply the use of robots supporting operation and maintenance activities. During the mapping in WP4, main aspects that affect O&M in solar and wind energy production technologies with potential application will be identified. ' => 
      array (
        0 => '',
      ),
      'Mapping the solar and wind energy production sector needs with energy the producers and identification of O&M targets in the near-medium future.' => 
      array (
        0 => '',
      ),
      'Screening and selection of the most suitable robotic and aerospace technologies with potential application to O&M in solar, on/offshore energy production. ' => 
      array (
        0 => '',
      ),
      'Validation of needs and technology providers ' => 
      array (
        0 => '',
      ),
      'Adaptation of technologies for navigation and surveillance ' => 
      array (
        0 => '',
      ),
      'Joint mapping of technologies and expected needs' => 
      array (
        0 => '',
      ),
      'Mock-up and solution operation on pilot´s sites' => 
      array (
        0 => '',
      ),
      'Adaptation of technologies for maintenance and repair
            ' => 
      array (
        0 => '',
      ),
      'KNOW MORE ABOUT TECHNOLOGIES' => 
      array (
        0 => '',
      ),
      'Discover the technologies we are going to develop and how we will implement them.' => 
      array (
        0 => '',
      ),
      'Technologies - DURABLE' => 
      array (
        0 => '',
      ),
      'Aerospace and robotic technologies together with composites and additive manufacturing, are developing technologies to reduce greenhouse gas emissions.' => 
      array (
        0 => '',
      ),
      'technological approach' => 
      array (
        0 => '',
      ),
      'Previous innovative developments and already available breakthrough technologies will be transferred and adapted to address the problematic of reducing inspection & repair costs in Solar and Wind infrastructures.' => 
      array (
        0 => '',
      ),
      'Predictive and preventive 0&M' => 
      array (
        0 => '',
      ),
      'During WP5, approaches for control and surveillance such as non-destructive testing (NDT) by robots (UAVs or UGVs), contact inspection (ultrasonic, thermographic), autonomous and intelligent navigation will be applied.' => 
      array (
        0 => '',
      ),
      'source: US' => 
      array (
        0 => '',
      ),
      'Unmanned Aerial Vehicle (UAV)' => 
      array (
        0 => '',
      ),
      'Inspection: Visual inspection ' => 
      array (
        0 => '',
      ),
      'Adaptation of required inspection methods and implementation onto the equipped UAV (thermography)' => 
      array (
        0 => '',
      ),
      'Aerial navigation for UAVs ' => 
      array (
        0 => '',
      ),
      'Non contact monitoring of RF frequency fields' => 
      array (
        0 => '',
      ),
      'source: FADA-CATEC' => 
      array (
        0 => '',
      ),
      'Nondestructive Testing (NDT) ' => 
      array (
        0 => '',
      ),
      'Thermography ' => 
      array (
        0 => '',
      ),
      'Ultrasonic techniques' => 
      array (
        0 => '',
      ),
      'Eddy-current techniques ' => 
      array (
        0 => '',
      ),
      'Radio Emission Spectroscopy techniques ' => 
      array (
        0 => '',
      ),
      'Proposed solution: Thermographic and visual inspections of wind blades using Unmanned Aerial Vehicles (UAVs)' => 
      array (
        0 => '',
      ),
      'Unmanned Ground Vehicle (UGV)' => 
      array (
        0 => '',
      ),
      'Cleaning' => 
      array (
        0 => '',
      ),
      'Inspection: Adaptation of NDTs to UGVs for inspection tasks.' => 
      array (
        0 => '',
      ),
      'Ground navigation technologies and ground support for aerial robots ' => 
      array (
        0 => '',
      ),
      'UAV power recharging on mobile UGV-based stations' => 
      array (
        0 => '',
      ),
      'Wide-area wireless communications by using the UGVs as repeaters' => 
      array (
        0 => '',
      ),
      'Augmented Reality (AR) and Virtual Reality (VR)' => 
      array (
        0 => '',
      ),
      'Definition of virtual and augmented reality environments, designed to help the operator to control robotic platforms, both aerial and on the ground. ' => 
      array (
        0 => '',
      ),
      'Virtual and augmented reality for robots control: both aerial and ground:  virtual and augmented reality cockpit to control a UAV or a mobile robot proof of concept' => 
      array (
        0 => '',
      ),
      'Corrective O&M' => 
      array (
        0 => '',
      ),
      'WP6 will analyze manufacturing technologies to enhance maintenance from the aerospace and manufacturing areas (water jet reparation, additive manufacturing, predictive algorithms, virtual and augmented reality). ' => 
      array (
        0 => '',
      ),
      'Additive manufacturing (AM)' => 
      array (
        0 => '',
      ),
      'Repair of damaged parts:' => 
      array (
        0 => '',
      ),
      'Material deposition processes  with LMD2 target parts of the nacelle or solar panels' => 
      array (
        0 => '',
      ),
      'Manufacturing of broken or spare parts:' => 
      array (
        0 => '',
      ),
      'High complexity geometries with SLM, including redesign possibilities' => 
      array (
        0 => '',
      ),
      'Automated composites repair technology' => 
      array (
        0 => '',
      ),
      'Analysis and adaptation of different damage regimes of a turbine blade' => 
      array (
        0 => '',
      ),
      'Demonstrator: Automated repair machining & persistent manual tasks of a turbine blade ' => 
      array (
        0 => '',
      ),
      'Laser-jet: automated composites repair technology (e.g. water jet technology) in Wind Turbine Blades' => 
      array (
        0 => '',
      ),
      'How to create a natural interaction with a robot arm to manipulate objects or realize a standard maintenance task. ' => 
      array (
        0 => '',
      ),
      'Virtual and augmented reality for using robots in persistent manual tasks ' => 
      array (
        0 => '',
      ),
      'WP7 will demonstrate the implementation feasibility of these pilot’s innovative solutions in at least 3-4 regions thanks to transnational cooperation.' => 
      array (
        0 => '',
      ),
      'The project will design pilot operations accounting for the final technologies used and tested in the other WPs and the characteristics of the host wind/solar installations.' => 
      array (
        0 => '',
      ),
      'source: ALERION' => 
      array (
        0 => '',
      ),
      'Wind demo: pilot validation / demonstration in wind farm ' => 
      array (
        0 => '',
      ),
      'source: INGETEAM' => 
      array (
        0 => '',
      ),
      'Solar demo: pilot validation / demonstration in solar farm' => 
      array (
        0 => '',
      ),
      'source: EDP' => 
      array (
        0 => '',
      ),
      ' ' => 
      array (
        0 => '',
      ),
      'KNOW MORE ABOUT NEWS & EVENTS' => 
      array (
        0 => '',
      ),
      'RELATED PROJECTS' => 
      array (
        0 => '',
      ),
      'The COREWIND project aims to achieve significant cost reductions and enhance performance of floating wind technology through the research and optimization of mooring and anchoring systems and dynamic cables.' => 
      array (
        0 => '',
      ),
      'ROMEO (Reliable O&M decision tools and strategies for high LCOE reduction on Offshore wind) seeks to reduce offshore O&M costs through the development of advanced monitoring systems and strategies, aiming to move from corrective and calendar based maintenance to a condition based maintenance, through analysing the real behaviour of the main components of wind turbines.' => 
      array (
        0 => '',
      ),
      'The SETWIND project supports the implementation of the SET-Plan Implementation Plan for Offshore Wind. It will update and work with the Implementation Plan to maintain it as a dynamic reference point for offshore wind energy research and innovation; it will monitor and report on progress towards the Implementation Plan targets of 1,090 million € to be invested in R&I in the offshore sector until 2030; it will strengthen policy coordination in European offshore wind energy R&I policy by supporting the work of the SET-Plan Implementation Group for Offshore Wind; and it will facilitate a breakthrough in the coordination across borders of nationally funded R&I projects.' => 
      array (
        0 => '',
      ),
      'The WATEREYE consortium will be designing an integrated solution that will allow wind farm operators to accurately predict future maintenance needs, thereby reducing operating and maintenance costs and increasing the amount of energy produced annually by offshore wind turbines.' => 
      array (
        0 => '',
      ),
      'The ambition of the TotalControl project is to develop the next generation of wind power plant (WPP) control tools, improving both WPP control itself and the collaboration between wind turbine (WT) and WPP control. To do this, TotalControl will use high-fidelity simulation and design environments that include detailed time resolved flow field modelling, nonlinear flexible multi-body representations of turbines, and detailed power grid models.' => 
      array (
        0 => '',
      ),
      'Installing, operating and maintaining marine wind turbines becomes more difficult and more expensive as water depth increases. The EU-funded FLOTANT project aims to build and demonstrate a scale model of an offshore floating platform to support a wind turbine in deep waters. The newly designed components will include a hybrid concrete-plastic platform, optimised mooring and anchoring systems, high-performance lightweight power cables, and a high-tech monitoring system for operation and maintenance. The project will further assess the technological, economic, environmental and social impact of the floating platform. Ultimately, it will contribute to the advancement of renewable technologies, and is estimated to reduce future capital and operational expenses by more than 50%.' => 
      array (
        0 => '',
      ),
      'News&events' => 
      array (
        0 => '',
      ),
    ),
  ),
);