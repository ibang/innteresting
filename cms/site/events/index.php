<?
require_once("../../php/init.php");
require_once("../../php/class.upload.php");
require_once("../../site/php/inc-events.php");
$section="events";
checkPermissions($section);
$title=$txt->events->title;
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="events">
<?require("{$DOC_ROOT}site/includes/header.php")?>
<div class="container">
	<div class="row">
		<div class="col-sm-8 col-md-8">
			<h1><?=$txt->events->h1?></h1>
		</div>
		<div class="col-sm-4 col-md-4">
			<p class="link link-add"><a class="btn btn-primary plus" href="<?=$URL_ROOT?>site/events/add.php"><?=$txt->events->link?></a></p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
            
			<?if (!empty($events)){?>
			<div class="table-responsive">
				<table class="table table-striped">
				     <thead>
				       <tr>
				         <th><?=$txt->events->table->title?></th>
				         <th class="d-none d-sm-table-cell"><?=$txt->events->table->{'date'}?></th>
				         <th class="d-none d-sm-table-cell"><?=$txt->events->table->type;?></th>
				        <!-- <th class="d-none d-sm-table-cell"><?=$txt->events->table->category?></th>-->
				         <th class="language d-none d-sm-table-cell"><?=$txt->events->table->language?></th>
				       </tr>
				     </thead>
				     <tbody>
						<?foreach ($events as $current){
                            if ( $current["type"] == 'events' ) {
                        ?>
				     	<tr>
				     	  <td><a href="<?=$URL_ROOT?>site/events/add.php?code=<?=$current["code"]?>"><?=$current["headline"]?></a></td>
				     	  <td class="d-none d-sm-table-cell"><?=$current["date"]?></td>
						  <td class="d-none d-sm-table-cell">Event</td>
				     	 <!-- <td class="d-none d-sm-table-cell"><?=$current["category"];?></td>-->
				     	  <td class="language d-none d-sm-table-cell"><?=$current["lang"]?></td>
				     	</tr>
                        <?
                            }
                        };
                        ?>
				     </tbody>
				</table>
			</div>
			<?};?>
		</div>
	</div>
	<? if ($actual_page>1 or $actual_page<$total_pages){?>
	<div class="row">
		<div class="col-md-12">
			<nav>
			  <ul class="pager list-unstyled justify-content-between pt-2 d-flex flex-row pb-0">
			    <? if ($actual_page<$total_pages){?><li class="previous"><a href="<?=$URL_ROOT?>site/events/<?='?page='.($actual_page+1)?>"><?=$txt->nav->prev?></a></li><?}?>
				 <? if ($actual_page>1){?><li class="next"><a href="<?=$URL_ROOT?>site/events/<?if (($actual_page-1)>1){echo '?page='.($actual_page-1);};?>"><?=$txt->nav->next?></a></li><?}?>
			  </ul>
			</nav>
		</div>
	</div>
	<?}?>
</div>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>