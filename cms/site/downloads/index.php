<?
require_once("../../php/init.php");
require_once("../../php/class.upload.php");
require_once("../../site/php/inc-downloads.php");
$section="downloads";
checkPermissions($section);
$title=$txt->downloads->title;
$js[]="drag.js";
$cnt=0;
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="front">
<?require("{$DOC_ROOT}site/includes/header.php")?>
<div class="container">
	<div class="row">
		<div class="col-sm-8 col-md-8">
			<h1><?=$txt->downloads->h1?></h1>
		</div>
		<div class="col-sm-4 col-md-4">
			<p class="link link-add"><a class="btn btn-primary btn-lg plus" href="<?=$URL_ROOT?>site/downloads/add.php"><?=$txt->downloads->link?></a></p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?if (!empty($downloads)){?>
			<div class="table-responsive">
				<table class="table table-striped">
				     <thead>
				       <tr>
				         <th><?=$txt->downloads->table->title?></th>
				         <th class="d-none d-md-table-cell"><?=$txt->downloads->table->filelanguage?></th>
				        <!-- <th class="d-none d-md-table-cell"><?=$txt->downloads->table->ispublic?></th> -->
				         <th class="language"><?=$txt->downloads->table->language?></th>
				       </tr>
				     </thead>
				     <tbody id="downloadsdragzone" data-start="<?=$start;?>" >
						<?foreach ($downloads as $download){?>
				     	<tr id="item-<?=$download["code"];?>" <?if(($actual_page<=$total_pages AND $actual_page>1 AND $cnt<$extraForOrder) OR ($actual_page>=1 AND $actual_page<$total_pages AND $cnt>=($page_rows-$extraForOrder))){?>class="extraorder"<?}?> >
				     	  <td <?$cnt++;if($cnt>$downloadspage){?>class="extrarow"<?}?> ><i class="fas fa-arrows-alt"></i> <span class="d-inline-block"><a href="<?=$URL_ROOT?>site/downloads/add.php?code=<?=$download["code"]?>"><?=$download["headline"]?></a><span class="d-none d-md-block small"><?=$download["category"]?></span></span></td>
				     	  <td class="d-none d-md-table-cell"><?=$download["filelanguage"]?></td>
				     	<!--  <td class="d-none d-md-table-cell"><?=$txt->downloads->ispublic->{$download["is_public"]}?></td> -->
				     	  <td class="language"><?=$download["lang"]?></td>
				     	</tr>
							<?};?>
				     </tbody>
				   </table>
			   </div>
			   <?};?>
		</div>
	</div>
	<? if ($actual_page>1 or $actual_page<$total_pages){?>
	<div class="row">
		<div class="col-md-12">
			<nav>
			  <ul class="pager list-unstyled justify-content-between pt-2 d-flex flex-row pb-0">
				 <? if ($actual_page>1){?><li class="previous"><a href="<?=$URL_ROOT?>site/downloads/?<?if (($actual_page-1)>1){echo '&page='.($actual_page-1);};?>"><?=$txt->nav->prev?></a></li><?}?>
				 <? if ($actual_page<$total_pages){?><li class="next"><a href="<?=$URL_ROOT?>site/downloads/?<?='&page='.($actual_page+1)?>"><?=$txt->nav->next?></a></li><?}?>
			  </ul>
			</nav>
		</div>
	</div>
	<?}?>
</div>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>