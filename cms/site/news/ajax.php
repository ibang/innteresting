<?php
require_once("../../php/init.php");
require_once("../../site/php/inc-general.php");
require_once("../../php/class.upload.php");
require_once("../../php/class.params.php");

if ($_SESSION['user']['perm_news']!='Y'){
	$response = array('error'=>'Private section');
	return json_encode($response);
}

$params = new Params();

// ------------- AJAX --------------
$functionName = $params->get('fn');
if ($functionName){
	echo call_user_func('_'.$functionName);
	return;
} 
function _fileUpload(){
	global $DOC_WEB_ROOT, $URL_WEB_ROOT;
	$response = array();
	
	if (empty($_POST['session_id'])){
		return json_encode(array('error'=>'No session id associated')); 
	}
	$session_id = $_POST['session_id'];
	
	if (empty($_POST['lang'])){
		return json_encode(array('error'=>'No lang id associated')); 
	}
	$lang = $_POST['lang'];
	
	if (empty($_POST['id'])){
		return json_encode(array('error'=>'No image id associated')); 
	}
	$id_media = $_POST['id'];
	
	if (empty($_FILES['imageattach']) AND empty($_FILES['fileattach'])) {
		return json_encode(array('error'=>'No files found for upload.')); 
		// or you can throw an exception 
	}
	
	// get the files posted
	$mediasarraytype = 'images';
	$mediatype = 'image';
	$mediatypeid = 'id-image';
	$mediatypeextra = 'alt-image';
	if ( empty($_FILES['fileattach'])){
		$medias = $_FILES['imageattach'];
		
	}
	else{
		$medias = $_FILES['fileattach'];
		$mediasarraytype = 'files';
		$mediatype = 'file';
		$mediatypeid = 'id-file';
		$mediatypeextra = 'link-file';
	}
	 
	// get user id posted
	$code = empty($_POST['code']) ? '' : $_POST['code'];
	 
	// a flag to see if everything is ok
	$success = null;
	 
	// file paths to store
	$paths= array();
	 
	// get file names
	$filenames = $medias['name'];
	 
	$targetFileName = array(); 
	$sourceFileName = array();
	// loop and process files
	for($i=0; $i < count($filenames); $i++){
		
		//
		$info = pathinfo($filenames[$i]);
		$ext = $info['extension'];
		$file_name =  url_slug(basename($filenames[$i],'.'.$ext), array('transliterate' => true));
		$sourceFileName[$i] = $file_name.".".$ext;
		//
		
		$targetFileName[$i] = md5(uniqid()) . "." . $ext;
		$target = $DOC_WEB_ROOT."uploads" . DIRECTORY_SEPARATOR . "tmp". DIRECTORY_SEPARATOR . $targetFileName[$i];
		if(move_uploaded_file($medias['tmp_name'][$i], $target)) {
			$success = true;
			$paths[] = $target;
		} else {
			$success = false;
			break;
		}
	}
	
	// check and process based on successful status 
	if ($success === true) {
		// call the function to save all data to database
		// code for the following function `save_data` is not 
		// mentioned in this example
		
		//save_data($userid, $username, $paths);
		
		$status = 'A';  // A => Added, M => Modified
		$prestatus = '';
		$id_modified = -1;
		$prefile ='';
		// falta el idioma
		if (isset($_SESSION[$session_id][$lang][$mediasarraytype])){
			foreach($_SESSION[$session_id][$lang][$mediasarraytype] as $i => $currentmedia){
				if ($currentimage[$mediatypeid] == $id_media){
					$status = 'M';
					$prestatus = $currentmedia['status'];
					$prefile = $currentmedia[$mediatype];
					$id_modified = $i;
					break;
				}
			}
		}
		
		switch ($status){
			case 'M':
				if ($prestatus=='M' OR $prestatus=='A'){ //if exists temporal file, remove it
					@unlink($DOC_WEB_ROOT."uploads" . DIRECTORY_SEPARATOR . "tmp". DIRECTORY_SEPARATOR .$prefile);
				}
				$_SESSION[$session_id][$lang][$mediasarraytype][$id_modified][$mediatype] = $targetFileName[0];
				$_SESSION[$session_id][$lang][$mediasarraytype][$id_modified]['status'] = ($prestatus=='A'?'A':'M');
				$_SESSION[$session_id][$lang][$mediasarraytype][$id_modified]['name'] = $sourceFileName[0];
				break;
			case 'A':
				$is_main = false;
				if (empty($_SESSION[$session_id][$lang][$mediasarraytype])){
					$is_main = true;
				}
				$_SESSION[$session_id][$lang][$mediasarraytype][] = array(
					'id'	=>	false,
					'code'	=>	false,
					'news_code'	=>	$code,
					'lang'	=>	$lang,
					$mediatype	=>	$targetFileName[0],
					$mediatypeextra	=> false,
					'added'	=>	false,
					'modified'	=>	false,
					'is-main'	=>	$is_main,
					'status'	=>	$status,
					$mediatypeid	=>	$id_media,
					'code-'.$mediatype	=>	false,
					'name'		=> $sourceFileName[0]
				);
			default:
				break;
		}
		
		// store a successful response (default at least an empty array). You
		// could return any additional response info you need to the plugin for
		// advanced implementations.
		$response = array(
			'initialPreview' => array(
				0 => ($mediatype=='image'?"<img src=\"".$URL_WEB_ROOT."uploads"."/tmp/".$targetFileName[0]."\" class=\"file-preview-image\" style=\"width:100%;height:auto\" />":"<a target=\"_blank\" href=\"".$URL_WEB_ROOT."uploads"."/tmp/".$targetFileName[0]."\" class=\"file-preview-file\" style=\"width:100%;height:auto\">".$filenames[0]."</a>")
			),
			'initialPreviewConfig' => array(
				0 => array('caption' => $filenames[0], 'width' => '100%', 'height' => 'auto', 'url' => 'ajax.php', 'key' => $id_media, 'extra' => array('code' => $code, 'fn' => 'fileDelete', 'session_id' => $session_id, 'lang' => $lang, 'file' => 'tmp/'.$targetFileName[0], 'id' => $id_media, 'type' => $mediatype)),
			),
			'append' => false
			);
	
		// for example you can get the list of files uploaded this way
		// $output = ['uploaded' => $paths];
	} elseif ($success === false) {
		$response = array('error'=>'Error while uploading media files. Contact the system administrator');
		// delete any uploaded files
		foreach ($paths as $file) {
			unlink($file);
		}
	} else {
		$response = array('error'=>'No files were processed.');
	}

	
	
	return json_encode($response);
}

function _fileDelete(){
	global $params, $DOC_WEB_ROOT;
	$response = array();
	$requestMethod=isset($_SERVER['REQUEST_METHOD'])?$_SERVER['REQUEST_METHOD']:'';
	
	$session_id = $params->get('session_id');
	if (empty($session_id)){
		return json_encode(array('error'=>'No session id associated')); 
	}
	
	$lang = $params->get('lang');
	if (empty($lang)){
		return json_encode(array('error'=>'No lang id associated')); 
	}
	
	$id_media = $params->get('id');
	if (empty($id_media)){
		return json_encode(array('error'=>'No media id associated')); 
	}
	
	$file = $params->get('file');
	if (empty($file)){
		return json_encode(array('error'=>'No files found for delete.')); 
	}
	
	$code = $params->get('code');
	
	$type = $params->get('type');
	if (empty($type)){
		$type = 'image';
	}
	
	// get the files posted
	$mediasarraytype = 'images';
	$mediatype = 'image';
	$mediatypeid = 'id-image';
	$mediatypeextra = 'alt-image';
	if ($type == 'file'){
		$mediasarraytype = 'files';
		$mediatype = 'file';
		$mediatypeid = 'id-file';
		$mediatypeextra = 'link-file';
	}
	
	$response = array();
	switch ($requestMethod){
		case 'DELETE':
		
			$status = 'D';
			$prestatus = '';
			$id_deleted = -1;
			$prefile = '';
			// falta el idioma
			if (isset($_SESSION[$session_id][$lang][$mediasarraytype])){
				foreach($_SESSION[$session_id][$lang][$mediasarraytype] as $i => $currentmedia){
					if ($currentmedia[$mediatypeid] == $id_media){
						$status = 'D';
						$prestatus = $currentmedia['status'];
						$prefile = $currentmedia[$mediatype];
						$id_deleted = $i;
						break;
					}
				}
			}
			
			if ($id_deleted==-1){ // not exist
				return json_encode(array('error'=>'No file uploaded for delete')); 
			}
			
			if ($prestatus == 'M' OR $prestatus == 'A'){ //previously modified or Added (temporal image, can delete)
				@unlink($DOC_WEB_ROOT."uploads" . DIRECTORY_SEPARATOR .$file);
			}
			$_SESSION[$session_id][$lang][$mediasarraytype][$id_deleted][$mediatype] = '';
			$_SESSION[$session_id][$lang][$mediasarraytype][$id_deleted]['status'] = $status;
			
			//$response = array('error'=>'No files found for remove fileDelete.');
			
			break;
		default:
			$response = array('error'=>'No files found for remove fileDelete.');
	}
	return json_encode($response);
}
// standard ajax methods
/*
$requestMethod=isset($_SERVER['REQUEST_METHOD'])?$_SERVER['REQUEST_METHOD']:'';
$response = array();
switch ($requestMethod){
	case 'DELETE':
		// $response = array('error'=>'No files found for remove.');
		break;
	default:
}
echo json_encode($response);
return;
*/
session_write_close();
?>