<?php
set_time_limit(0);

//------------------ ajax ----------------------------
$functionName=isset($_POST['fn'])?$_POST['fn']:'';
if ($functionName){
	echo call_user_func('_'.$functionName);
	exit();
}

// Función que ordena
function _categorysuggest(){
	global $txt, $db;
	$message='';
	$ok=false;
	$error=false;
	$error_arr='';
	$keyword = isset($_POST['query'])?$_POST['query']:'';
	$lang = isset($_POST['lang'])?$_POST['lang']:'';

	$categoryResults = array();
	$results = $db->get_categories_suggests_events($keyword, $lang);
	if (!empty($results)) {
		foreach($results as $row) {
			$categoryResults[] = $row["category"];
		}
	}
	return json_encode($categoryResults);
}

//
function subir_archivo($i,$folder){
	$ret = -2;
	$handle = new upload($i);
	if ($handle->uploaded) {
		$handle->allowed	= array('application/pdf'); // only pdf files
		$handle->file_max_size = '10M';
		
		$handle->process($folder);
		if ($handle->processed) {
			$ret	=	$handle->file_dst_name;
			$handle->clean();
		} else {
			addToLog('EVENTS File Error: ' . $handle->error);
			return  -3;	// proccess error
		}
	}
	return $ret;	// file destination name or -2 (upload error)
}

function mover_file($originFolderAndFile, $destinationFolder, $newDestinationFileName=''){
	$ret = -2;
	$handle = new upload($originFolderAndFile);
	if ($handle->uploaded) {
		if ($newDestinationFileName){
			$info = pathinfo($newDestinationFileName);
			$file_name =  basename($newDestinationFileName,'.'.$info['extension']);
			$handle->file_new_name_body   = $file_name; //without extension
		}
		$handle->file_auto_rename = true;
		$handle->file_overwrite = false;
		$handle->allowed	= array('application/pdf'); // only pdf files
		$handle->file_max_size = '10M';
		
		$handle->process($destinationFolder);
		if ($handle->processed) {
			$destinationFile	=	$handle->file_dst_name;
			$ret = $destinationFile;
			$handle->clean();
		} else {
			addToLog('EVENTS File Error: ' . $handle->error);
			return  -3;	// proccess error
		}
	}
	else{
		addTolog('EVENTS File UPLOAD ERROR:'.$handle->error);
		return $ret;
	}
	return $ret;	// file destination name or -2 (upload error)
}

function mover_imagen($originFolderAndImage, $destinationFolder, $newDestinationFileName=''){
	$thumbwidth = NEWSIMAGECROPHORIZONTAL;
	$thumbheight = NEWSIMAGECROPVERTICAL;
	$ret = -2;
	// -- load origin image file
	$image = new upload($originFolderAndImage);
	// -- add edit options like resize, crop etc..
	// -- end edit options 
	// -- save image in destinationFolder
	if ($image->uploaded) {
		if ($newDestinationFileName){
			$info = pathinfo($newDestinationFileName);
			$file_name =  basename($newDestinationFileName,'.'.$info['extension']);
			$image->file_new_name_body   = $file_name; //without extension
		}
		$image->file_auto_rename = true;
		$image->file_overwrite = false;
		$image->allowed	= array('image/*'); // o
		$image->file_max_size = '10M';
		
		$image->image_convert	= 'jpg';
		$image->image_resize	= true;
		$image->image_ratio_crop	= 'TL';
		$image->image_y	= $thumbheight;
		$image->image_x	= $thumbwidth;
		$image->jpeg_quality = 85;
		
		//addTolog('NEWS IMAGE PRE PROCCESS:'.$originFolderAndImage);
		$image->process($destinationFolder);
		//addTolog('NEWS IMAGE POST PROCCESS:'.$file_name);
		if ($image->processed) {
			$destinationFile	=	$image->file_dst_name;
			$ret = $destinationFile;
			$image->clean();
		} else {
			addTolog('EVENTS PROCESS ERROR:'.$image->error);
			return -3;
		}
	}
	else{
		addTolog('EVENTS UPLOAD ERROR:'.$image->error);
		return $ret;
	}
	return $ret;
}

$events = array();


$langs=$db->get_cms_active_langs();

if ($_GET["code"]){
	$code=$_GET["code"];
}

$lang_active='';
$error=0;
$warning=0;

if ($_GET["action"]=="delete"){
	$code=$_GET["code"];
	$lang=$_GET["lang"];
	$events_del=$db->get_events($code);
	$events_del=$db->get_id_events_images_list($events_del, false);
	$events_del=$db->get_id_events_files_list($events_del, false);
	if ($lang=="all"){
		foreach ($events_del as $img_del){
			if (!empty($img_del['images'])){
				foreach ($img_del['images'] as $current_image){
					@unlink ("{$DOC_WEB_ROOT}uploads/events/".$current_image["image"]);
				}
			}
			if (!empty($img_del['files'])){
				foreach ($img_del['files'] as $current_file){
					@unlink ("{$DOC_WEB_ROOT}uploads/events/".$current_file["file"]);
				}
			}
			/*
			if ($img_del["file"]!=""){
				@unlink ("{$DOC_WEB_ROOT}uploads/events/".$img_del["file"]);
			};
			*/
		}
		$db->delete_all_events_images($code);
		$db->delete_all_events_files($code);
		$db->delete_events($code);
		header('Location: '.$URL_ROOT.'site/events/');
	}else{
		$events_lang_del=$db->get_events_lang($lang,$code);
		$events_lang_del['images'] = $db->get_code_events_images($lang, $code, false);
		$events_lang_del['files'] = $db->get_code_events_files($lang, $code, false);
		if (count($events_del)==1){
			if (!empty($events_lang_del['images'])){
				foreach ($events_lang_del['images'] as $current_image){
					@unlink ("{$DOC_WEB_ROOT}uploads/events/".$current_image["image"]);
				}
			}
			if (!empty($events_lang_del['files'])){
				foreach ($events_lang_del['files'] as $current_file){
					@unlink ("{$DOC_WEB_ROOT}uploads/events/".$current_file["file"]);
				}
			}
			/*
			if ($events_lang_del["file"]!=""){
				@unlink ("{$DOC_WEB_ROOT}uploads/events/".$events_lang_del["file"]);
			};
			*/
			$db->delete_all_events_images($code);
			$db->delete_all_events_files($code);
		}
		$db->delete_events_lang ($code,$lang);
		$db->delete_events_lang_images($code,$lang);
		$db->delete_events_lang_files($code,$lang);
		header('Location: '.$URL_ROOT.'site/events/' );
	}
	
}


if ($_POST){
	$session_id = isset($_POST["session_id"])?$_POST["session_id"]:md5(uniqid()); 
	
	$_POST["headline"] = trim($_POST["headline"]);
	$form_lang_code = $db->get_lang_code($_POST["lang"]); // no transliteramos al alfabeto latino el ruso
	$events_form=array(
			"id"=>$_POST["id"],
			"code"=>$_POST["code"],
			"lang"=>$_POST["lang"],
			"headline"=>addslashes($_POST["headline"]),
			"slug"=> url_slug($_POST["headline"], array('transliterate' => ($form_lang_code!='ru'?true:false) )),
			"date"=>trim($_POST["date"]),
		//	"category"=>addslashes($_POST["category"]),
		//	"category_slug"=> url_slug($_POST["category"], array('transliterate' => ($form_lang_code!='ru'?true:false))),
			"intro"=>addslashes($_POST["intro"]),
			"body"=>addslashes(transformAllToInternationalLangUrl(cleanHTML($_POST["body"]))),
			"meta-title"=>addslashes($_POST["meta-title"]),
			"meta-description"=>addslashes($_POST["meta-desc"]),
			"meta-keywords"=>addslashes($_POST["meta-key"]),
			"location"=>(!empty($_POST["location"])?addslashes($_POST["location"]):''),
			"link"=>addslashes($_POST["link"]),
			"link-text"=>addslashes($_POST["link-text"]),
		//	"file-linktext"=>addslashes($_POST["file-linktext"]),
			"active"=>$_POST["active"],
			"type"	=>	$_POST["type"]
			);
		
	if ($_POST["id"]!=""){
		
		//------update events------//
		$events_org = $db->get_events_lang($events_form["lang"],$events_form["code"]);
		$events_org['images'] = $db->get_code_events_images($events_form["lang"], $events_form["code"]);
		$events_org['files'] = $db->get_code_events_files($events_form["lang"], $events_form["code"]);
		
		//--- events images ------//
		$events_form['images'] = $events_org['images'];
		$events_form['files'] = $events_org['files'];
		
		$events_images_cnt = 0;
		if (!empty($_SESSION[$session_id][$events_form['lang']]['images'])){
			$is_main = true;
			foreach ($_SESSION[$session_id][$events_form['lang']]['images'] as $i => $current_image){
				switch ($current_image['status']){
					case 'A':
						$new_image = array(
							'id'	=>	false,
							'code'	=>	false,
							'events_code'	=>	$events_form['code'],
							'lang'	=>	$events_form['lang'],
							'image'	=>	$current_image['image'],
							'path-image' => 'tmp/',
							'alt-image'	=> addslashes($_POST['alt-'.$current_image['id-image']]),
							'added'	=>	false,
							'modified'	=>	false,
							'is-main'	=>	$is_main,
							'status'	=>	$current_image['status'],
							'id-image'	=>	$current_image['id-image'],
							'code-image'	=>	false,
							'name'		=> $current_image['name']
						);
						$events_form['images'][] = $new_image;
						$is_main = false;
						$events_images_cnt++;
						break;
					case 'M':
						$modified_image = $events_org['images'][$current_image['code-image']];
						$modified_image['lang']	= $events_form['lang'];
						$modified_image['image'] = $current_image['image'];
						$modified_image['path-image'] = 'tmp/';
						$modified_image['alt-image'] = addslashes($_POST['alt-'.$current_image['id-image']]);
						$modified_image['is-main'] = $is_main;
						$modified_image['status'] = $current_image['status'];
						$modified_image['id-image'] = $current_image['id-image'];
						$modified_image['name'] = $current_image['name'];
						$events_form['images'][$current_image['code-image']] = $modified_image;
						$is_main = false;
						$events_images_cnt++;
						break;
					case 'D':
						if ($current_image['code-image']){
							$deleted_image = $events_org['images'][$current_image['code-image']];
							$deleted_image['lang']	= $events_form['lang'];
							$deleted_image['alt-image'] = '';
							$deleted_image['image'] = '';
							$deleted_image['path-image'] = '';
							$deleted_image['is-main'] = false;
							$deleted_image['name'] = '';
							$deleted_image['status'] = $current_image['status'];
							$deleted_image['id-image'] = $current_image['id-image'];
							$events_form['images'][$current_image['code-image']] = $deleted_image;
						}
						break;
					default:
						if ($current_image['code-image']){
							$modified_image = $events_org['images'][$current_image['code-image']];
							$modified_image['lang']	= $events_form['lang'];
							$modified_image['alt-image'] = addslashes($_POST['alt-'.$current_image['id-image']]);
							$modified_image['is-main'] = $is_main;
							$events_form['images'][$current_image['code-image']] = $modified_image;
							$is_main = false;
							$events_images_cnt++;
						}
						break;
				}
			}
        }
      
		if (!$events_images_cnt){
			$error++;
			$validate["images"]["hasnoelements"]=1;
		}
		
		/* FILES */
		$events_files_cnt = 0;
		if (!empty($_SESSION[$session_id][$events_form['lang']]['files'])){
			$is_main = true;
			foreach ($_SESSION[$session_id][$events_form['lang']]['files'] as $i => $current_file){
				switch ($current_file['status']){
					case 'A':
						$new_file = array(
							'id'	=>	false,
							'code'	=>	false,
							'events_code'	=>	$events_form['code'],
							'lang'	=>	$events_form['lang'],
							'file'	=>	$current_file['file'],
							'path-file' => 'tmp/',
							'link-file'	=> addslashes($_POST['link-'.$current_file['id-file']]),
							'added'	=>	false,
							'modified'	=>	false,
							'is-main'	=>	$is_main,
							'status'	=>	$current_file['status'],
							'id-file'	=>	$current_file['id-file'],
							'code-file'	=>	false,
							'name'		=> $current_file['name']
						);
						$events_form['files'][] = $new_file;
						$is_main = false;
						$events_files_cnt++;
						break;
					case 'M':
						$modified_file = $events_org['files'][$current_file['code-file']];
						$modified_file['lang']	= $events_form['lang'];
						$modified_file['file'] = $current_file['file'];
						$modified_file['path-file'] = 'tmp/';
						$modified_file['link-file'] = addslashes($_POST['link-'.$current_file['id-file']]);
						$modified_file['is-main'] = $is_main;
						$modified_file['status'] = $current_file['status'];
						$modified_file['id-file'] = $current_file['id-file'];
						$modified_file['name'] = $current_file['name'];
						$events_form['files'][$current_file['code-file']] = $modified_file;
						$is_main = false;
						$events_files_cnt++;
						break;
					case 'D':
						if ($current_file['code-file']){
							$deleted_file = $events_org['files'][$current_file['code-file']];
							$deleted_file['lang']	= $events_form['lang'];
							$deleted_file['link-file'] = '';
							$deleted_file['file'] = '';
							$deleted_file['path-file'] = '';
							$deleted_file['is-main'] = false;
							$deleted_file['name'] = '';
							$deleted_file['status'] = $current_file['status'];
							$deleted_file['id-file'] = $current_file['id-file'];
							$events_form['files'][$current_file['code-file']] = $deleted_file;
						}
						break;
					default:
						if ($current_file['code-file']){
							$modified_file = $events_org['files'][$current_file['code-file']];
							$modified_file['lang']	= $events_form['lang'];
							$modified_file['link-file'] = addslashes($_POST['link-'.$current_file['id-file']]);
							$modified_file['is-main'] = $is_main;
							$events_form['files'][$current_file['code-file']] = $modified_file;
							$is_main = false;
							$events_files_cnt++;
						}
						break;
				}
			}
		}
		if (!$events_files_cnt){
			//$error++;
			//$validate["files"]["hasnoelements"]=1;
		}
		
		/*
		//--- if file uploads --//
		if (!empty($_FILES["file"]) AND $_FILES["file"]["name"]!=NULL){
            $uploaded_file = subir_archivo($_FILES["file"],"{$DOC_WEB_ROOT}uploads/events/");
            if (!is_numeric($uploaded_file)){
                $events_form["file"] = $uploaded_file;
                if ($events_org["file"]!="" AND ($events_org["file"] != $uploaded_file) ){
                    @unlink ("{$DOC_WEB_ROOT}uploads/events/".$events_org["file"]);
                };
            }
            else{
                $events_form["file"] = $events_org["file"];
                $validate["file"]["upload"] = abs($uploaded_file); // 2 => upload error or 3 => proccess error;
                $error++;
            }
        }elseif(!empty($_FILES["file"]) AND empty($events_org["file"])){ // File name is null
            $events_form["file"] = '';
           // $validate["file"]["upload"]=1; // is required
           // $error++;
        }elseif(!empty($_POST["file-del"]) AND $_POST["file-del"]==$events_org["file"]){
            $events_form["file"] = '';
        }
		else{
			$events_form["file"] = $events_org["file"];
		}
		*/
		
		#--- Checks fields that not avoid update the project if is there an error on any of them.
		
		//--- check used headline --//
		if ((strtolower(addslashes($events_org["headline"]))!=strtolower($events_form["headline"])) or ($events_org["slug"]!=$events_form["slug"])){
			if ($db->headline_events_exist($events_form["headline"], $events_form["slug"], $events_form["lang"], $events_form["code"])){
				$error++;
				$validate["headline"]["duplicated"]=1;
			}
			if (preg_match('/^\d+$/D', $events_form["slug"])){
				$error++;
				$validate["headline"]["isnumber"]=1;
			}
		}
		
		//--- check date
		if (empty($events_form["date"]) OR !isDate($events_form["date"])){
			$error++;
			$validate["date"]["nodate"]=1;
		}
		
		//--- check location
		if (empty($events_form["location"]) AND $events_form['type']=='eventos'){
			$error++;
			$validate["location"]["required"]=1;
		}
		
		//--- check file-linktext
		/*
		if (empty($events_form["file-linktext"]) AND !empty($events_form["file"])){
			$error++;
			$validate["file-linktext"]["required"]=1;
		}
		*/
		
		//--- Is there is a error on any of the fields that not avoid update the project
		if (!empty($validate["headline"]) OR !empty($validate["date"]) OR !empty($validate["location"]) ){
			$events=$db->get_events($events_form["code"]);
			$events=$db->get_id_events_images_list($events);
			$events=$db->get_id_events_files_list($events);
			foreach ($events as $id => $current){
				if ($current['lang']==$events_form['lang']){
					$events[$id] = $events_form; // actualizo los datos con los del del formulario
					$events[$id]['lang_code'] = $current['lang_code'];
					$lang_active = $current['lang_code'];
					break;
				}
			}
			// reset error fields
			if (!empty($validate["headline"])){
				unset($events_form["slug"]);
				unset($events_form["headline"]);
			}
			if (!empty($validate["date"])){
				unset($events_form["date"]);
			}
			if (!empty($validate["location"])){
				unset($events_form["location"]);
			}
		}
		
		$db->update_events($events_form);
		if (!$error){	
			// -- proccess images --
			if (!empty($events_form['images'])){
				foreach ($events_form['images'] as $i => $current_image){
					switch ($current_image['status']){
						case 'A':
							$destinationFile = @mover_imagen($DOC_WEB_ROOT."uploads" . "/" . "tmp". "/" .$current_image['image'],"{$DOC_WEB_ROOT}uploads/events/", $current_image['name']);
							if (!is_numeric($destinationFile)){
								$events_form['images'][$i]['path-image'] = 'events/';
								$events_form['images'][$i]['image'] = $destinationFile;
							}
							else{
								unset($events_form['images'][$i]);
								$error++;
								$validate["images"]["imageerror"]= abs($destinationFile);
							}
							break;
						case 'M':
							$modified_image = $events_org['images'][$current_image['code-image']];
							$modified_image['lang']	= $events_form['lang'];
							$destinationFile = @mover_imagen($DOC_WEB_ROOT."uploads" . "/" . "tmp". "/" .$current_image['image'],"{$DOC_WEB_ROOT}uploads/events/", $current_image['name']);
							if (!is_numeric($destinationFile)){
								@unlink ("{$DOC_WEB_ROOT}uploads/events/".$modified_image["image"]);
								
								$events_form['images'][$i]['path-image'] = 'events/';
								$events_form['images'][$i]['image'] = $destinationFile;
							}
							else{
								unset($events_form['images'][$i]);
								$error++;
								$validate["images"]["imageerror"]= abs($destinationFile);
							}
							break;
						case 'D':
							if ($current_image['code-image']){
								$deleted_image = $events_org['images'][$current_image['code-image']];
								$deleted_image['lang']	= $events_form['lang'];
								@unlink ("{$DOC_WEB_ROOT}uploads/events/".$deleted_image["image"]);
							}
							break;
						default:
							break;
					}
				}
			}
			$db->update_events_images($events_form);
			
			// -- proccess files --
			if (!empty($events_form['files'])){
				foreach ($events_form['files'] as $i => $current_file){
					switch ($current_file['status']){
						case 'A':
							$destinationFile = @mover_file($DOC_WEB_ROOT."uploads" . "/" . "tmp". "/" .$current_file['file'],"{$DOC_WEB_ROOT}uploads/events/", $current_file['name']);
							if (!is_numeric($destinationFile)){
								$events_form['files'][$i]['path-file'] = 'events/';
								$events_form['files'][$i]['file'] = $destinationFile;
							}
							else{
								unset($events_form['files'][$i]);
								$error++;
								$validate["files"]["fileerror"]= abs($destinationFile);
							}
							break;
						case 'M':
							$modified_file = $events_org['files'][$current_file['code-file']];
							$modified_file['lang']	= $events_form['lang'];
							$destinationFile = @mover_file($DOC_WEB_ROOT."uploads" . "/" . "tmp". "/" .$current_file['file'],"{$DOC_WEB_ROOT}uploads/events/", $current_file['name']);
							if (!is_numeric($destinationFile)){
								@unlink ("{$DOC_WEB_ROOT}uploads/events/".$modified_file["file"]);
								
								$events_form['files'][$i]['path-file'] = 'events/';
								$events_form['files'][$i]['file'] = $destinationFile;
							}
							else{
								unset($events_form['files'][$i]);
								$error++;
								$validate["files"]["fileerror"]= abs($destinationFile);
							}
							break;
						case 'D':
							if ($current_file['code-file']){
								$deleted_file = $events_org['files'][$current_file['code-file']];
								$deleted_file['lang']	= $events_form['lang'];
								@unlink ("{$DOC_WEB_ROOT}uploads/events/".$deleted_file["file"]);
							}
							break;
						default:
							break;
					}
				}
			}
			$db->update_events_files($events_form);
			
			/*
			if(!empty($_POST["file-del"]) AND $_POST["file-del"]==$events_org["file"] AND $events_form["file"]!=$events_org["file"]){
				@unlink ("{$DOC_WEB_ROOT}uploads/events/".$events_org["file"]);
			}
			*/
		}
		elseif(empty($events) and ( !empty($validate["files"]) OR !empty($validate["images"]) ) ){ //-- there is only a 'images' error, initialize $events var
			$events=$db->get_events($events_form["code"]);
			$events=$db->get_id_events_images_list($events);
			$events=$db->get_id_events_files_list($events);
			foreach ($events as $id => $current){
				if ($current['lang']==$events_form['lang']){
					$events[$id] = $events_form; // actualizo los datos con los del del formulario
					$events[$id]['lang_code'] = $current['lang_code'];
					$lang_active = $current['lang_code'];
					break;
				}
			}
		}			
		
		
		if ($events_form['active']=='Draft'){ //borrador
			$warning++;
			$attention['active']['draft']=1;
		}
		$code=$events_form["code"];
		$lang=$events_form["lang"];
		if (!$error){
			if ($warning){
				$warn = '&warning='.$warning.'&attention='.json_encode($attention);
			}
			else{
				$warn = '&update=ok';
			}
			_emptySessions();
			session_write_close();
			header("Location: add.php?code=$code&lang=$lang".$warn);
			exit();
		}
	}else{
		//------nueva events------//
		
		//-- events images --//
		if ($events_form["code"]==""){
			$last_code=$db->get_events_last_code ();
			$events_form["code"]=$last_code+1;
		}
		else{
			$code = $events_form["code"];
		}
		
		if ($code){
			$events_org = $db->get_events_lang('general',$events_form["code"]);
			$events_org['images'] = $db->get_code_events_images($events_form['lang'], $events_form["code"]);
			$events_org['files'] = $db->get_code_events_files($events_form['lang'], $events_form["code"]);
		}
		
		// IMAGES
		$events_images_cnt = 0;
		if (!empty($_SESSION[$session_id][$events_form['lang']]['images'])){
			$is_main = true;
			foreach ($_SESSION[$session_id][$events_form['lang']]['images'] as $i => $current_image){
				switch ($current_image['status']){
					case 'A':
						$new_image = array(
							'id'	=>	false,
							'code'	=>	false,
							'events_code'	=>	$events_form['code'],
							'lang'	=>	$events_form['lang'],
							'image'	=>	$current_image['image'],
							'path-image' => 'tmp/',
							'alt-image'	=> addslashes($_POST['alt-'.$current_image['id-image']]),
							'added'	=>	false,
							'modified'	=>	false,
							'is-main'	=>	$is_main,
							'status'	=>	$current_image['status'],
							'id-image'	=>	$current_image['id-image'],
							'code-image'	=>	false,
							'name'		=> $current_image['name']
						);
						$events_form['images'][] = $new_image;
						$is_main = false;
						$events_images_cnt++;
						break;
					case 'M':
						$modified_image = $events_org['images'][$current_image['code-image']];
						$modified_image['lang']	= $events_form['lang'];
						$modified_image['image'] = $current_image['image'];
						$modified_image['path-image'] = 'tmp/';
						$modified_image['alt-image'] = addslashes($_POST['alt-'.$current_image['id-image']]);
						$modified_image['is-main'] = $is_main;
						$modified_image['status'] = $current_image['status'];
						$modified_image['id-image'] = $current_image['id-image'];
						$modified_image['name'] = $current_image['name'];
						$events_form['images'][$current_image['code-image']] = $modified_image;
						$is_main = false;
						$events_images_cnt++;
						break;
					case 'D':
						if ($current_image['code-image']){
							$deleted_image = $events_org['images'][$current_image['code-image']];
							$deleted_image['lang']	= $events_form['lang'];
							$deleted_image['alt-image'] = '';
							$deleted_image['image'] = '';
							$deleted_image['path-image'] = '';
							$deleted_image['name'] = '';
							$deleted_image['is-main'] = false;
							$deleted_image['status'] = $current_image['status'];
							$deleted_image['id-image'] = $current_image['id-image'];
							$events_form['images'][$current_image['code-image']] = $deleted_image;
						}
						break;
					default:
						if ($current_image['code-image']){
							
							$modified_image = $events_org['images'][$current_image['code-image']];
							$modified_image['alt-image'] = addslashes($_POST['alt-'.$current_image['id-image']]);
							$modified_image['is-main'] = $is_main;
							if ($modified_image['lang'] != $events_form['lang']){
								$modified_image['lang'] = $events_form['lang'];
								$modified_image['id'] = '';
								$modified_image['added'] = '';
								$modified_image['modified'] = '';
							}
							$events_form['images'][$current_image['code-image']] = $modified_image;
							$is_main = false;
							$events_images_cnt++;
						}
						break;
				}
			}
		}
		if (!$events_images_cnt){
			$error++;
			$validate["images"]["hasnoelements"]=1;
		}
		
		// FILES
		$events_files_cnt = 0;
		if (!empty($_SESSION[$session_id][$events_form['lang']]['files'])){
			$is_main = true;
			foreach ($_SESSION[$session_id][$events_form['lang']]['files'] as $i => $current_file){
				switch ($current_file['status']){
					case 'A':
						$new_file = array(
							'id'	=>	false,
							'code'	=>	false,
							'events_code'	=>	$events_form['code'],
							'lang'	=>	$events_form['lang'],
							'file'	=>	$current_file['file'],
							'path-file' => 'tmp/',
							'link-file'	=> addslashes($_POST['link-'.$current_file['id-file']]),
							'added'	=>	false,
							'modified'	=>	false,
							'is-main'	=>	$is_main,
							'status'	=>	$current_file['status'],
							'id-file'	=>	$current_file['id-file'],
							'code-file'	=>	false,
							'name'		=> $current_file['name']
						);
						$events_form['files'][] = $new_file;
						$is_main = false;
						$events_files_cnt++;
						break;
					case 'M':
						$modified_file = $events_org['files'][$current_file['code-file']];
						$modified_file['lang']	= $events_form['lang'];
						$modified_file['file'] = $current_file['file'];
						$modified_file['path-file'] = 'tmp/';
						$modified_file['link-file'] = addslashes($_POST['link-'.$current_file['id-file']]);
						$modified_file['is-main'] = $is_main;
						$modified_file['status'] = $current_file['status'];
						$modified_file['id-file'] = $current_file['id-file'];
						$modified_file['name'] = $current_file['name'];
						$events_form['files'][$current_file['code-file']] = $modified_file;
						$is_main = false;
						$events_files_cnt++;
						break;
					case 'D':
						if ($current_file['code-file']){
							$deleted_file = $events_org['files'][$current_file['code-file']];
							$deleted_file['lang']	= $events_form['lang'];
							$deleted_file['link-file'] = '';
							$deleted_file['file'] = '';
							$deleted_file['path-file'] = '';
							$deleted_file['name'] = '';
							$deleted_file['is-main'] = false;
							$deleted_file['status'] = $current_file['status'];
							$deleted_file['id-file'] = $current_file['id-file'];
							$events_form['files'][$current_file['code-file']] = $deleted_file;
						}
						break;
					default:
						if ($current_file['code-file']){
							
							$modified_file = $events_org['files'][$current_file['code-file']];
							$modified_file['link-file'] = addslashes($_POST['link-'.$current_file['id-file']]);
							$modified_file['is-main'] = $is_main;
							if ($modified_file['lang'] != $events_form['lang']){
								$modified_file['lang'] = $events_form['lang'];
								$modified_file['id'] = '';
								$modified_file['added'] = '';
								$modified_file['modified'] = '';
							}
							$events_form['files'][$current_file['code-file']] = $modified_file;
							$is_main = false;
							$events_files_cnt++;
						}
						break;
				}
			}
		}
		if (!$events_files_cnt){
			//$error++;
			//$validate["files"]["hasnoelements"]=1;
		}
		
		//--- check used headline --//
		if ($db->headline_events_exist($events_form["headline"], $events_form["slug"], $events_form["lang"])){
			$error++;
			$validate["headline"]["duplicated"]=1;
		}
		if (preg_match('/^\d+$/D', $events_form["slug"])){
			$error++;
			$validate["headline"]["isnumber"]=1;
		}
		
		//--- check date
		if (empty($events_form["date"]) OR !isDate($events_form["date"])){
			$error++;
			$validate["date"]["nodate"]=1;
		}
		
		//--- check location
		if (empty($events_form["location"]) AND $events_form['type']=='eventos'){
			$error++;
			$validate["location"]["required"]=1;
		}
		
		if ($error AND (!empty($validate["headline"]) OR !empty($validate["images"]) OR !empty($validate["date"]) OR !empty($validate["location"]))){
			$events_form["code"] = false;
			$events[0] = $events_form;
			$events[0]['lang_code'] = $db->get_lang_code($events[0]['lang']);
			$lang_active = $events[0]['lang_code'];
		}
		
		/*
		//die('add: '.print_r($events_form,true));	
		// [IMAGE - FILE] fields proccess (save, move, delete)
		if (!$error){
			if (!empty($_FILES["file"]) AND $_FILES["file"]["name"]!=NULL){
                //$tmp = time();
                $uploaded_file = subir_archivo($_FILES["file"],"{$DOC_WEB_ROOT}uploads/events/");
                if (!is_numeric($uploaded_file)){
                    $events_form["file"] = $uploaded_file;
                    if ($events_org["file"]!="" AND ($events_org["file"]!=$uploaded_file) ){
                        @unlink ("{$DOC_WEB_ROOT}uploads/events/".$events_org["file"]);
                    };
                }
                else{
                    $events_form["file"] = '';
                    $validate["file"]["upload"] = abs($uploaded_file); // 2 => upload error or 3 => proccess error;
                    $error++;
                }
            }else{
                $events_form["file"] = $events_org["file"];
                
                if (empty($events_form["file"])){
                   // $validate["file"]["upload"]=1; // required
                   // $error++;
                }
            }
		}
		*/
		
		if (!$error){
			
			$db->add_events ($events_form);
			
			/*
			//--- check file-linktext
			if (empty($events_form["file-linktext"]) AND !empty($events_form["file"])){
				$error++;
				$validate["file-linktext"]["required"]=1;
			}
			*/
		
			// -- proccess images --
			if (!empty($events_form['images'])){
				foreach ($events_form['images'] as $i => $current_image){
					switch ($current_image['status']){
						case 'A':
							$destinationFile = @mover_imagen($DOC_WEB_ROOT."uploads" . "/" . "tmp". "/" .$current_image['image'],"{$DOC_WEB_ROOT}uploads/events/", $current_image['name']);
							if(!is_numeric($destinationFile)){
								$events_form['images'][$i]['path-image'] = 'events/';
								$events_form['images'][$i]['image'] = $destinationFile;
							}else{
								unset($events_form['images'][$i]);
								$error++;
								$validate["images"]["imageerror"]= abs($destinationFile);
							}
							break;
						case 'M':
							$modified_image = $events_org['images'][$current_image['code-image']];
							$modified_image['lang']	= $events_form['lang'];
							$destinationFile = @mover_imagen($DOC_WEB_ROOT."uploads" . "/" . "tmp". "/" .$current_image['image'],"{$DOC_WEB_ROOT}uploads/events/", $current_image['name']);
							
							if (!is_numeric($destinationFile)){
								$events_form['images'][$i]['path-image'] = 'events/';
								$events_form['images'][$i]['image'] = $destinationFile;
								@unlink ("{$DOC_WEB_ROOT}uploads/events/".$modified_image["image"]);
							}
							else{
								unset($events_form['images'][$i]);
								$error++;
								$validate["images"]["imageerror"]= abs($destinationFile);
							}
							break;
						case 'D':
							if ($current_image['code-image']){
								$deleted_image = $events_org['images'][$current_image['code-image']];
								$deleted_image['lang']	= $events_form['lang'];
								@unlink ("{$DOC_WEB_ROOT}uploads/events/".$deleted_image["image"]);
							}
							break;
						default:
							break;
					}
				}
			}
			
			$db->update_events_images($events_form);
			
			// -- proccess files --
			if (!empty($events_form['files'])){
				foreach ($events_form['files'] as $i => $current_file){
					switch ($current_file['status']){
						case 'A':
							$destinationFile = @mover_file($DOC_WEB_ROOT."uploads" . "/" . "tmp". "/" .$current_file['file'],"{$DOC_WEB_ROOT}uploads/events/", $current_file['name']);
							if(!is_numeric($destinationFile)){
								$events_form['files'][$i]['path-file'] = 'events/';
								$events_form['files'][$i]['file'] = $destinationFile;
							}else{
								unset($events_form['files'][$i]);
								$error++;
								$validate["files"]["fileerror"]= abs($destinationFile);
							}
							break;
						case 'M':
							$modified_file = $events_org['files'][$current_file['code-file']];
							$modified_file['lang']	= $events_form['lang'];
							$destinationFile = @mover_file($DOC_WEB_ROOT."uploads" . "/" . "tmp". "/" .$current_file['file'],"{$DOC_WEB_ROOT}uploads/events/", $current_file['name']);
							
							if (!is_numeric($destinationFile)){
								$events_form['files'][$i]['path-file'] = 'events/';
								$events_form['files'][$i]['file'] = $destinationFile;
								@unlink ("{$DOC_WEB_ROOT}uploads/events/".$modified_file["file"]);
							}
							else{
								unset($events_form['files'][$i]);
								$error++;
								$validate["files"]["fileerror"]= abs($destinationFile);
							}
							break;
						case 'D':
							if ($current_file['code-file']){
								$deleted_file = $events_org['files'][$current_file['code-file']];
								$deleted_file['lang']	= $events_form['lang'];
								@unlink ("{$DOC_WEB_ROOT}uploads/events/".$deleted_file["file"]);
							}
							break;
						default:
							break;
					}
				}
			}
			
			$db->update_events_files($events_form);
			
			if ($_POST['active']=='Draft'){ //borrador
				$warning++;
				$attention['active']['draft']=1;
			}
			
			$code=$events_form["code"];
			$lang = $events_form["lang"];
			if ($warning){
				$warn = '&warning='.$warning.'&attention='.json_encode($attention);
			}
			else{
				$warn = '&insert=ok';
			}
			_emptySessions();
			session_write_close();
			header("Location: add.php?code=$code&lang=$lang".$warn);
			exit();
		}
		else{
			$events_form["code"] = false;
			
			//reseteamos las imágenes y demás elementos comunes que no habíamos comprobados antes.
			// ????? No tengo muy claro para que hago esto, porque no se usa luego. Si se hace, habría que añadirlo al events[0]['images']
			if (!empty($events_form["images"])){
				foreach ($events_form["images"] as $i	=>	$current_image){
					$events_form["images"][$i]['events_code'] = false;
				}
				
				$events[0]["images"] = $events_form["images"];
			}
			
			//reseteamos los ficheros y demás elementos comunes que no habíamos comprobados antes.
			// ????? No tengo muy claro para que hago esto, porque no se usa luego. Si se hace, habría que añadirlo al events[0]['files']
			if (!empty($events_form["files"])){
				foreach ($events_form["files"] as $i	=>	$current_file){
					$events_form["files"][$i]['events_code'] = false;
				}
				
				$events[0]["files"] = $events_form["files"];
			}
		}
	}
}
else{
	if (isset($_SESSION['id'])){
		_emptySessions();
	}
	// borro las sessiones de usuario
	foreach ($_SESSION as $var => $val){
		if ($var!='user'){
			unset($_SESSION[$var]);
		}
	}
	//
	$session_id = md5(uniqid());
	$_SESSION['id'] = $session_id;
	$_SESSION[$session_id] = array();
	foreach ($langs as $current_lang){
		$_SESSION[$session_id][$current_lang['id']] = array( 
			'code' => (int)$code,
			'images' => array(),
			'files' => array()
		);
	}
}

//------------- extraigo events en todos los idiomas en los que esté -----
$events_lang = array();
if ($_GET or ($_POST and $error)){
	if (!$error){
		$events=$db->get_events($code);
		$events=$db->get_id_events_images_list($events);
		$events=$db->get_id_events_files_list($events);
		if ($_GET["update"]){
			$update=$_GET["update"];
		
		}
		if ($_GET["insert"]){
			$insert=$_GET["insert"];
		
		}
	}
	else{ // form data error
		// en caso de error en el formulario no hay que hacer nada en la variable de sessión ya que contiene los datos que tiene que tener y
		// events estará actualizado con los datos del formulario erróneo
	}

	//------------- creo un array events_codigoidioma para cada idioma -----
	
	$default_lang = '';
	foreach ($events as $current){
		$events_lang[$current["lang_code"]]=array(
			"id"=>$current["id"],
			"code"=>$code,
			"lang"=>$current["lang"],
			"headline"=>$current["headline"],
			"date"=>$current["date"],
			"category"=>$current["category"],
			"intro"=>$current["intro"],
			"body"=>$current["body"],
			"meta-title"=>$current["meta-title"],
			"meta-description"=>$current["meta-description"],
			"meta-keywords"=>$current["meta-keywords"],
			"active"=>$current['active'],
			"images"=>$current['images'],
			"files"=>$current['files'],
			"type"=>$current["type"],
			"location"=>$current["location"],
			"link"=>$current["link"],
			"link-text"=>$current["link-text"],
		);
		
		
		
		$_SESSION[$session_id][$current["lang"]] = array(
			'code'=>$code, 
			'images'=> $current['images'],
			'files'=> $current['files']
		);
		$default_lang = $current["lang_code"];
	}
	
	if ($default_lang){
		// -- reset alt-image for the not defined lang --
		$imagesAlt = $events_lang[$default_lang]["images"];
		if (!empty($imagesAlt)){
			$imagesAltReseted = array_map(function ($item) {
				$item['alt-image'] = '';
				$item['id'] = false;
				$item['lang'] = false;
				$item['added'] = false;
				$item['modified'] = false;
				return $item;
			}, $imagesAlt);
		}
		else{
			$imagesAltReseted = $imagesAlt;
		}
		// -- reset link-file for the not defined lang --
		$filesLink = $events_lang[$default_lang]["files"];
		if (!empty($filesLink)){
			$filesLinkReseted = array_map(function ($item) {
				$item['link-file'] = '';
				$item['id'] = false;
				$item['lang'] = false;
				$item['added'] = false;
				$item['modified'] = false;
				return $item;
			}, $filesLink);
		}
		else{
			$filesLinkReseted = $filesLink;
		}
		foreach ($langs as $current_lang){
			if (empty($events_lang[$current_lang["lang"]])){
				$events_lang[$current_lang["lang"]] = $db->get_events_lang($current_lang["id"], $events_lang[$default_lang]["code"]);
				if (empty($events_lang[$current_lang["lang"]])){
					$events_lang[$current_lang["lang"]] = array();
				}
				$events_lang[$current_lang["lang"]]=array_merge($events_lang[$current_lang["lang"]], array(
					"code"=>$code,
					"lang"=>$current_lang["lang"],
					"date"=>$events_lang[$default_lang]["date"],
					"images"=> $imagesAltReseted,
					"files"=> $filesLinkReseted,
					"type"=>$events_lang[$default_lang]["type"],
					"location"=>$events_lang[$default_lang]["type"]
				));
				
				
				$_SESSION[$session_id][$current_lang["id"]] = array(
					'code'=>$code, 
					'images'=> $imagesAltReseted,
					'files'=> $filesLinkReseted
				);
			}
		}
	}
}

//load default keywords
$doc = new DOMDocument();
foreach ($langs as $current_lang){
	if (@$doc->load($DOC_WEB_ROOT."lang/".strtolower($current_lang["lang"]).".xml")){
		 $xPath = new DOMXPath($doc);
		foreach( $xPath->query('/txt/events/keywords') as $current_keywords ) {
			$events_lang[$current_lang["lang"]]['default-keywords'] = $current_keywords->nodeValue; // Will output bar1 and bar2 w\out whitespace
		}
	}
}

if (!$lang_active){
	if (isset($_GET['lang']) AND !empty($_GET['lang'])){
		$lang_active = $db->get_lang_code($_GET['lang']);
	}
	else{
		$lang_active= $db->get_cms_default_lang();;
		if (count($events)>1){
			$lang_active= $db->get_cms_default_lang();;
		}
		elseif (!empty($events[0])){
			$lang_active=$events[0]['lang_code'];
		}
	}
}

if (isset($_GET['warning']) AND !empty($_GET['warning'])){
	$warning = $_GET['warning'];
	$attention = json_decode($_GET['attention'], true);
}
session_write_close();
?>