<?php
//------------------ ajax ----------------------------
$functionName=isset($_GET['fn'])?$_GET['fn']:'';
if ($functionName){
	echo call_user_func('_'.$functionName);
	exit();
}

// Función que borra un suscriptor dado de la base de datos de suscriptores
function _reorderDownload(){
	global $txt, $db;
	$message='';
	$ok=false;
	$error=false;
	$error_arr='';
	$items = isset($_GET['item'])?$_GET['item']:array();
	$currentPosition = isset($_GET['start'])?$_GET['start']:0;
	if (!empty($items)){
		foreach ($items as $currentCode) {
			// Execute statement:
			$db->update_downloads_code_position($currentCode, $currentPosition);
			$currentPosition++;
		}
	}
	
	if (!$error){
		$ok=true;
		$result['ok']='ok';
	}
	else{
		$result['error']='error';
	}
	$result = json_encode($result);
	return $result;
}
//----------------------------------

/* INICIALIZO VARIABLES */
$_GET['page'] = isset($_GET['page'])?$_GET['page']:0;
//

//------------------ downloads List ------------------	
$activeLangsCode = $db->get_cms_active_langs_array('downloads');	
$downloadspage = CMSSMALLLISTINGPAGESIZE;;
$extraForOrder = CMSLISTEXTRAORDER;
if ($extraForOrder > $downloadspage){
	$extraForOrder = (int)($downloadspage/2);
}
$downloads_cnt = $db->count_downloads_list('all', true);
if ($_GET["page"]){$actual_page=$_GET["page"];}else{$actual_page=1;}
$total_pages=ceil($downloads_cnt/$downloadspage);

if($actual_page==1){ // actual_page is the first page
	$fromrow = 0;
	$torow = $downloadspage + $extraForOrder;
}
elseif($actual_page<$total_pages){ // actual_page is a inner page
	$fromrow = ($downloadspage*($actual_page-1)) - $extraForOrder;
	$torow = $downloadspage + (2*($extraForOrder));
}
else{ // actual_page is the last page
	$fromrow = ($downloadspage*($actual_page-1)) - $extraForOrder;
	$torow = $downloadspage + $extraForOrder;
}

$limit="LIMIT ".$fromrow." , ".$torow;

$codes = $db->get_downloads_limit_list($limit);
$start=0;
if (!empty($codes)){
	$firstElement = reset($codes);
	$start = $firstElement['order'];
	foreach ($codes as $code){
		
		$downloads_code=$db->get_downloads_list($code["code"]);
		$downloads[$code["code"]]["order"]=$downloads_code["0"]["order"];
		$downloads[$code["code"]]["headline"]=$downloads_code["0"]["headline"];
		$downloads[$code["code"]]["is_public"]=$downloads_code["0"]["is_public"];
		$downloads[$code["code"]]["date"]=$downloads_code["0"]["date"];
		$downloads[$code["code"]]["code"]=$downloads_code["0"]["code"];
		$downloads[$code["code"]]["filelanguage"] = $downloads_code["0"]["filelanguage"];
		$downloads[$code["code"]]["lang"]=(empty($downloads[$code["code"]]["lang"])?'':$downloads[$code["code"]]["lang"]);
		
		$downloads[$code["code"]]["category"] = $downloads_code["0"]["category"];
		
		$downloads[$code["code"]]["code"]=$downloads_code["0"]["code"];
		
		//print_r($downloads_code);
		foreach ($downloads_code as $current_code){
			$downloads[$code["code"]]["lang"]= $downloads[$code["code"]]["lang"].'<a class="btn btn-'.($current_code['active']=='Yes'?'primary':'default').' btn-xs'.(in_array($current_code["lang_code"], $activeLangsCode)?'':' disabled').'" href="'.$URL_ROOT.'site/downloads/add.php?code='.$code["code"].'&lang='.$current_code["lang"].'">'.$current_code["lang_code"].'</a> ';
		}
	}
}
$page_rows = 0;
if (!empty($downloads)){
	$page_rows = count($downloads);
}
?>