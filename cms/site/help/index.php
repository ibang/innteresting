<?
require_once("../../php/init.php");
require_once("../../site/php/inc-help.php");
$section="help";
$title=$txt->help->title;
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="help">
<?require("{$DOC_ROOT}site/includes/header.php")?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1><?=$txt->help->h1?></h1>
		</div>
	</div>
	<div class="row">
			<div class="col-md-7">
				<p class="subtitle"><?=$txt->help->subtitle?></p>
				<? if ($sent=="success"){?>
				<div class="alert alert-success">
					<p><?=$txt->help->form->alert->success->title?></p>
					<p><?=$txt->help->form->alert->success->text?></p>
				</div>
				<?};?>
				<? if ($error>0 ){?>
				<div class="alert alert-danger">
					<p><?=$txt->help->form->alert->error->title?></p>
					<p><?=$txt->help->form->alert->error->text?></p>
					<ol>
						<?if ($validate["firstname"]==1){?>
							<li><?=$txt->help->form->alert->error->label?> <strong><?=$txt->form->firstname->title?></strong> <?=$txt->help->form->alert->error->required?></li>
						<?}?>
						<?if ($validate["lastname"]==1){?>
							<li><?=$txt->help->form->alert->error->label?> <strong><?=$txt->form->lastname->title?></strong> <?=$txt->help->form->alert->error->required?></li>
						<?}?>
						<?if ($validate["email"]==1){?>
							<li><?=$txt->help->form->alert->error->label?> <strong><?=$txt->form->email->title?></strong> <?=$txt->help->form->alert->error->required?></li>
						<?}?>
						<?if ($validate["phone"]==1){?>
							<li><?=$txt->help->form->alert->error->label?> <strong><?=$txt->form->phone->title?></strong> <?=$txt->help->form->alert->error->required?></li>
						<?}?>
						<?if ($validate["message"]==1){?>
							<li><?=$txt->help->form->alert->error->label?> <strong><?=$txt->form->message->title?></strong> <?=$txt->help->form->alert->error->required?></li>
						<?}?>
					</ol>
				</div>
				<?}?>
				<form  role="form" action="<?=$URL_ROOT?>site/help/" method="post" id="form-contact" >
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
							  <label for="firstname" class="sr-only"><?=$txt->form->firstname->title?> <span>*</span></label>
							  <input type="text" class="form-control input-lg" name="firstname" id="firstname" placeholder="<?=$txt->form->firstname->holder?>" title="<?=$txt->form->firstname->title?>" value="<?=$contact["firstname"]?>" required>				    
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
							  <label for="lastname" class="sr-only"><?=$txt->form->lastname->title?> <span>*</span></label>
							  <input type="text" class="form-control input-lg" name="lastname" id="lastname" placeholder="<?=$txt->form->lastname->holder?>" title="<?=$txt->form->lastname->title?>" value="<?=$contact["lastname"]?>" required>				    
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
							  <label for="email" class="sr-only"><?=$txt->form->email->title?> <span>*</span></label>
							  <input type="email" class="form-control input-lg" name="email" id="email" placeholder="<?=$txt->form->email->holder?>" title="<?=$txt->form->email->title?>" value="<?=$contact["email"]?>" required>				    
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
							  <label for="phone" class="sr-only"><?=$txt->form->phone->title?> <span>*</span></label>
							  <input type="tel" class="form-control input-lg" name="phone" id="phone" placeholder="<?=$txt->form->phone->holder?>" title="<?=$txt->form->phone->title?>" value="<?=$contact["phone"]?>" required>				    
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
							  <label for="message" class="sr-only"><?=$txt->form->message->title?> <span>*</span></label>
							  <textarea class="form-control input-lg" rows="8" name="message" id="message" placeholder="<?=$txt->form->message->holder?>" title="<?=$txt->form->message->title?>" required><?=$contact["message"]?></textarea>	    
							</div>
							<div class="form-group">
							  <p class="required small"><span>*</span> <?=$txt->form->required->title?></p>
							  <button type="submit" class="btn btn-primary"><?=$txt->form->submit->sent?></button>		    
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="col-md-4 col-md-offset-1">
				<div class="contact-data">
					<p class="title"><strong>Dirección</strong></p>
					<p>Parque Empresarial Zuatzu.<br />
					Edificio Bidasoa Local 5 P.B.<br />
					20018 Donostia (Spain)</p>
					<p class="title"><strong>Teléfono</strong></p>
					<p class="phone"><span class="prefix">(+34)</span> <a href="tel:943306706" title="¡Llámanos!">943 306 706</a></p>
					<p class="title"><strong>Email</strong></p>
					<p class="email"><a href="mailto:promueve@promueve3.com">promueve@promueve3.com</a></p>
				</div>
			</div>
		</div>
</div>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>