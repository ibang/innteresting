<?
require_once("../../php/init.php");
require_once("../../site/php/inc-contacts-add.php");
$section="contact";
checkPermissions($section);
$lngtype = !empty($contacts['type'])?$contacts['type']:'all';
$title=$txt->contact->title;
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="contact">
<?require("{$DOC_ROOT}site/includes/header.php")?>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<p class="link"><a class="btn btn-default back" href="<?=$URL_ROOT?>site/contact/?type=<?=$type;?>"><?=$txt->contact->back?></a></p>
			<ul class="list">
				<? if(!empty($contacts['firstname'])){?>
				<li><span><?=$txt->contact->table->firstname?>:</span> <?=htmlspecialchars($contacts['firstname'])?></li>
				<?}?>
				<? if(!empty($contacts['lastname'])){?>
				<li><span><?=$txt->contact->table->lastname?>:</span> <?=htmlspecialchars($contacts['lastname'])?></li>
				<?}?>
				<? if(!empty($contacts['email'])){?>
				<li><span><?=$txt->contact->table->email?>:</span> <?=htmlspecialchars($contacts['email'])?></li>
				<?}?>
				<? if(!empty($contacts['phone'])){?>
				<li><span><?=$txt->contact->table->phone?>:</span> <?=htmlspecialchars($contacts['phone'])?></li>
				<?}?>
				<? if(!empty($contacts['country'])){?>
				<li><span><?=$txt->contact->table->country?>:</span> <?=htmlspecialchars($contacts['country'])?></li>
				<?}?>
				<? if(!empty($contacts['province'])){?>
				<li><span><?=$txt->contact->table->province?>:</span> <?=htmlspecialchars($contacts['province'])?></li>
				<?}?>
				<? if(!empty($contacts['company'])){?>
				<li><span><?=$txt->contact->table->company?>:</span> <?=htmlspecialchars($contacts['company'])?></li>
				<?}?>
                <? if(!empty($contacts['position'])){?>
				<li><span><?=$txt->contact->table->position?>:</span> <?=htmlspecialchars($contacts['position'])?></li>
				<?}?>
                <? if(!empty($contacts['advisory'])){?>
				<li><span><?=$txt->contact->table->group?>:</span> <?=htmlspecialchars($contacts['advisory'])?></li>
				<?}?>
				<? if(!empty($contacts['lang_name'])){?>
				<li><span><?=$txt->contact->table->language?>:</span> <?=htmlspecialchars($contacts['lang_name'])?><?=(($contacts["lang_name"] AND $contacts["location_name"])?' - ':'').htmlspecialchars($contacts['location_name'])?></li>
				<?}?>
				<? if(!empty($contacts['type'])){?>
				<li><span><?=$txt->contact->table->type?>:</span> <?=htmlspecialchars($contacts['type'])?></li>
				<?}?>
				<?if(!empty($contacts['about'])){?>
				<li><span><?=$txt->contact->table->about?>:</span> <?=htmlspecialchars($contacts['about'])?></li>
				<?}?>
				<!--
				<?if(!empty($contacts['gclid'])){?>
				<li><span><?=$txt->contact->table->adwords?>:</span> <?=htmlspecialchars($contacts['gclid'])?></li>
				<?}?>
				<?if(!empty($contacts['gmode'])){?>
				<li><span><?=$txt->contact->table->gmode?>:</span> <?=htmlspecialchars($contacts['gmode'])?></li>
				<?}?>
				-->
				<li><? if(!empty($contacts['message'])){?>
			<p><span><?=$txt->contact->table->message?>:</span> <?=$contacts['message']?></p>
			<?}?></li>
			</ul>
			
		</div>
	</div>
</div>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>