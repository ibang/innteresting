<?
require_once("../../php/init.php");
$section="faq";
$title=$txt->faq->title;
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="faq">
<?require("{$DOC_ROOT}site/includes/header.php")?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1><?=$txt->faq->h1?></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			  <div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="headingOne">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			        	<?=$txt->faq->languages->title?>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
			      <div class="panel-body">
			      	<div class="row">
			      		<div class="col-md-7">
			      			<?=$txt->faq->languages->desc?>
			      		</div>
			      		<div class="col-md-4 col-md-offset-1">
			      			<img class="img-responsive img-thumbnail" src="<?=$URL_ROOT?>assets/img/faq/1.png" alt="<?=$txt->faq->languages->title?>" />
			      		</div>
			      	</div>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="headingTwo">
			      <h4 class="panel-title">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			        	<?=$txt->faq->general->title?>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			      <div class="panel-body">
			        <div class="row">
			        	<div class="col-md-7">
			        		<?=$txt->faq->general->desc?>
			        	</div>
			        	<div class="col-md-4 col-md-offset-1">
			        		<img class="img-responsive img-thumbnail" src="<?=$URL_ROOT?>assets/img/faq/2.png" alt="<?=$txt->faq->general->title?>" />
			        	</div>
			        </div>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="headingThree">
			      <h4 class="panel-title">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			        	<?=$txt->faq->draft->title?>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
			      <div class="panel-body">
			        <div class="row">
			        	<div class="col-md-7">
			        		<?=$txt->faq->draft->desc?>
			        	</div>
			        	<div class="col-md-4 col-md-offset-1">
			        		<img class="img-responsive img-thumbnail" src="<?=$URL_ROOT?>assets/img/faq/3.png" alt="<?=$txt->faq->draft->title?>" />
			        	</div>
			        </div>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="headingFour">
			      <h4 class="panel-title">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
			        	<?=$txt->faq->colors->title?>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
			      <div class="panel-body">
			        <div class="row">
			        	<div class="col-md-7">
			        		<?=$txt->faq->colors->desc?>
			        	</div>
			        	<div class="col-md-4 col-md-offset-1">
			        		<img class="img-responsive img-thumbnail" src="<?=$URL_ROOT?>assets/img/faq/4.png" alt="<?=$txt->faq->colors->title?>" />
			        	</div>
			        </div>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="headingFive">
			      <h4 class="panel-title">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
			        	<?=$txt->faq->order->title?>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
			      <div class="panel-body">
			        <div class="row">
			        	<div class="col-md-7">
			        		<?=$txt->faq->order->desc?>
			        	</div>
			        	<div class="col-md-4 col-md-offset-1">
			        		<img class="img-responsive img-thumbnail" src="<?=$URL_ROOT?>assets/img/faq/5.png" alt="<?=$txt->faq->order->title?>" />
			        	</div>
			        </div>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
	</div>
</div>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>