<header role="banner">
	<div class="container">
		<div class="row pt-3">
		<div class="col-sm-4 navbar-header">
			<? if ($section=="home") {?><h1 id="logo" class="navbar-brand"><a href="<?=$URL_ROOT?>site/"><span><?=$txt->logo?></span></a></h1><? }
			else {?><p id="logo" class="navbar-brand"><a href="<?=$URL_ROOT?>site/"><span><?=$txt->logo?></span></a></p><? }?>
		</div>
		<div class="profile col-sm-8 mt-2">
			<div class="data float-left">
				<p class="name m-0"><?=$_SESSION['user']['nickname']?></p>
				<p class="exit"><a href="<?=$URL_ROOT?>site/login.php?action=logout" id="user_logout" class="text-secondary"><?=$txt->nav->exit?></a></p>
			</div>
			<div class="avatar float-left ml-2">
				<p><?if(!empty($_SESSION['user']['avatar'])){?><img src="<?=$URL_ROOT?>uploads/users/<?=$_SESSION['user']['avatar']?>" alt="<?=$_SESSION['user']['nickname']?>"/><?}else{?><img src="<?=$URL_ROOT?>uploads/users/default.png" alt="<?=$_SESSION['user']['nickname']?>"/><?}?></p>
			</div>
			<div class="website float-right">
				<p><a class="btn btn-default" href="<?=$txt->client->url?>"><?=$txt->nav->website?></a></p>
			</div>
		</div>
		</div>

  		<nav class="navbar menuPrincipal navbar-expand-lg navbar-light bg-lignt">
		<button type="button" class="navbar-toggler" data-toggle="collapse" data-target=".navbar-collapse"><span><?=$txt->nav->title?></span></button>
			<div class="collapse navbar-collapse " id="navbarCollapse">
				<div id="nav-main">
					<ul class="nav navbar-nav">
							<li><a <? if($section=="home"){?> class="active"<? }?> href="<?=$URL_ROOT?>site/"><?=$txt->nav->main->home?></a></li>

							<?if ($_SESSION['user']['perm_news']=='Y'){?>
							<li><a <? if($section=="news"){?> class="active"<? }?> href="<?=$URL_ROOT?>site/news/"><?=$txt->nav->main->media?></a></li>
							<?}?>

                            <?if ($_SESSION['user']['perm_events']=='Y'){?>
							<li><a <? if($section=="events"){?> class="active"<? }?> href="<?=$URL_ROOT?>site/events/"><?=$txt->nav->main->events?></a></li>
							<?}?>

                            <?if ($_SESSION['user']['perm_downloads']=='Y'){?>
                                <li><a <? if($section=="downloads"){?> class="active"<? }?> href="<?=$URL_ROOT?>site/downloads/"><?=$txt->nav->main->downloads->downloads?></a></li>
                            <?}?>
                            
                         

                            <?if ($_SESSION['user']['perm_contact']=='Y'){?>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle<? if($section=="contact"){?> active<? }?>" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$txt->nav->main->contact->title?></a>
								<ul class="dropdown-menu">
									<li><a <? if(empty($type)){?> class="active"<? }?> href="<?=$URL_ROOT?>site/contact/"><?=$txt->nav->main->contact->all?></a></li>
									<li><a <? if($type=="contact"){?> class="active"<? }?> href="<?=$URL_ROOT?>site/contact/?type=contact"><?=$txt->nav->main->contact->general?></a></li>
									<li><a <? if($type=="advisory group"){?> class="active"<? }?> href="<?=$URL_ROOT?>site/contact/?type=advisory group"><?=$txt->nav->main->contact->advisory?></a></li>
                                    <li><a <? if($type=="social acceptance"){?> class="active"<? }?> href="<?=$URL_ROOT?>site/contact/?type=social acceptance"><?=$txt->nav->main->contact->socialacceptance?></a></li>
                                    <li><a <? if($type=="newsletter"){?> class="active"<? }?> href="<?=$URL_ROOT?>site/contact/?type=newsletter"><?=$txt->nav->main->contact->newsletter?></a></li>
								</ul>
							</li>
							<?}?>

						</ul>
				</div><!-- / nav-main -->
			</div><!-- / navbar-collapse -->
		</nav>
	</div><!-- / container -->
</header>