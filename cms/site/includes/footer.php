<footer role="contentinfo">
	<div class="full-container nav-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-6 clearfix">
					<nav id="nav-tools">
						<ul>
							<?if ($_SESSION['user']['perm_users']=='Y'){?>
							<li><a<? if($section=="users"){?> class="active"<? }?> href="<?=$URL_ROOT?>site/users/"><?=$txt->nav->footer->users?></a></li>
							<?}?>
							<li><a<? if($section=="faq"){?> class="active"<? }?> href="<?=$URL_ROOT?>site/faq/"><?=$txt->nav->footer->faq?></a></li>
							<li><a<? if($section=="help"){?> class="active"<? }?> href="<?=$URL_ROOT?>site/help/"><?=$txt->nav->footer->help?></a></li>
						</ul>
					</nav>
				</div>
				<div class="col-md-6 clearfix">
					<nav id="nav-version">
						<ul>
							<li><?=$txt->nav->footer->version?></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12 clearfix">
				<div class="contact-info">
					<p class="logo"><a href="<?=$URL_ROOT.$language?>/"><span><?=$txt->logo?></span></a></p>
					<div class="content">
						<p class="address"><?=$txt->footer->address?></p>
						<p class="phone"><span><?=$txt->footer->phone?> <strong class="prefix">(+34)</strong> <a href="tel:943306706">943 306 706</a></span> <span><?=$txt->footer->email?> <a href="mailto:promueve@promueve3.com">promueve@promueve3.com</a></span></p>
						<p class="copy"><?=$txt->footer->copy?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<div id="overlay"></div>
<?if (SASSLESS=='LESS'){?>
<script src="<?=$URL_ROOT?>assets/js/jquery-2.1.3.min.js"></script>
<script src="<?=$URL_ROOT?>assets/js/bootstrap.min.js"></script>
<script src="<?=$URL_ROOT?>assets/js/jquery-ui.min.js"></script>
<?}elseif(SASSLESS=='SASS'){?>
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
<?}?>
<script src="<?=$URL_ROOT?>assets/js/fileinput.min.js?v=<?=CFGCURRENTVERSION;?>" ></script>
<script src="<?=$URL_ROOT?>assets/js/fileinput_locale_es.js?v=<?=CFGCURRENTVERSION;?>"></script>
<script src="<?=$URL_ROOT?>assets/js/bootbox.min.js?v=<?=CFGCURRENTVERSION;?>"></script>
<?if (!empty($js)){?>
	<?foreach($js as $cjs){?>
<script src="<?=$URL_ROOT?>assets/js/<?=$cjs?>?v=<?=CFGCURRENTVERSION;?>"></script>
	<?}?>
<?}?>
<script type="text/javascript">
$(document).ready(function(){
	// Hack para recalcular la altura de los tinymce de los tabs ocultos
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	  var target = $(e.target).attr("href") // activated tab
	  target = target.replace('#', '')
	  //alert(target);
	  
	 // if (target!='<?=$lang_active?>'){
		 
		//recalculo el tamaño de los tinymce (primero muestro el textarea, calculo el height y lo asigno al ifrma para finalmente volver a ocultarlo)
		$('#form-news-'+target+' .ckeditor, #form-projects-'+target+' .ckeditor').toggle(0, function(e){
			var textareaHeight = $(this).height();
			//redimensiono el tinymce
			$(this).parent().find('iframe').first().css('height', textareaHeight+'px');
			$(this).toggle(0);
		});
		
	  //}
	});
});
</script>