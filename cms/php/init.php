<?
// Uncomment this lines on production enviroment
// ini_set("display_errors", 0);
// ini_set("log_errors", 1);
error_reporting(E_ALL ^ E_NOTICE);
// server location
if (!function_exists('serverLocation')){
	function serverLocation($thisFileRelativePath=''){
		// Load the absolute server path to the directory the script is running in
		$fileDir = str_replace('\\','/',dirname(__FILE__)); //si es windows, sustituyo los \ por /

		// Make sure we end with a slash
		$fileDir = rtrim($fileDir, '/').'/';
		
		// Load the absolute server path to the document root
		$docRoot = $_SERVER['DOCUMENT_ROOT'];

		// Make sure we end with a slash
		$docRoot= rtrim($_SERVER['DOCUMENT_ROOT'],"/").'/';

		// find part before docRoot in fileDir (in case fileDir is a absolute Path and docRoot an equivalent Paht than no start with de same path)
		$beforeStr = strstr($fileDir, $docRoot, true); 
		$docRoot = $beforeStr.$docRoot;

		// Remove docRoot string from fileDir string as subPath string
		$subPath = preg_replace('~' . $docRoot . '~i', '', $fileDir);

		// Add a slash to the beginning of subPath string
		$subPath = '/' . $subPath;          

		// Test subPath string to determine if we are in the web root or not
		if ($subPath == '/') {
			// if subPath = single slash, docRoot and fileDir strings were the same
			//echo "We are running in the web foot folder of htt..://" . $_SERVER['SERVER_NAME'];
			return $subPath; // '/'
		} else {
			// Anyting else means the file is running in a subdirectory
			//echo "We are running in the '" . $subPath . "' subdirectory of htt..://" . $_SERVER['SERVER_NAME'];
			if ($thisFileRelativePath){
				$thisFileRelativePath = '/'.trim($thisFileRelativePath, '/').'/';
				$subPath = preg_replace('~' . $thisFileRelativePath . '~i', '', $subPath);
				$subPath = $subPath.'/';
			}
			return $subPath;
		}
	}
}
$URL_WEB_ROOT = serverLocation('cms/php'); // "/{Directory in witch documents are inside of, for local enviroment}/";

$URL_ROOT = $URL_WEB_ROOT."cms/";

// roots
$DOC_WEB_ROOT = rtrim($_SERVER['DOCUMENT_ROOT'],"/").$URL_WEB_ROOT;
$DOC_ROOT = rtrim($_SERVER['DOCUMENT_ROOT'],"/").$URL_ROOT;

// configuration file 
require_once($DOC_WEB_ROOT."php/config.php");
$URL_CMS = CMSDIRNAME."/";
$DOC_ROOT_CMS = $DOC_WEB_ROOT.$URL_CMS;

$language='en';

$URL_ROOT_BASE = $URL_ROOT . $language;

// General methods
require_once($DOC_ROOT_CMS."php/general.php");

// DB methods
require_once($DOC_ROOT_CMS."php/db.php");

// SESSION 

// check if session is already active based on php version 
if (version_compare(phpversion(), '5.4.0', '<')) { //menos que php 5.4.0
    // php version isn't high enough
	if(session_id() == '') {
		session_name(ADMINCOOKIENAME);
		@session_start();
	}
}
else{ // >= 5.4.0
	if (session_status() == PHP_SESSION_NONE) {
		session_name(ADMINCOOKIENAME);
		@session_start();
	}
}

if (!$is_public){
	if (!logged()){
		header("Location:".$URL_ROOT.'site/login.php');
		session_write_close();
		exit();
	}
}

// load xml
$txt=simplexml_load_file($DOC_ROOT."lang/".strtolower($language).".xml") or die("Error loading language");

// ajax querys
if (isset($_GET['fn']) and $_GET['fn']){
	if (function_exists('_'.$_GET['fn'])){
		echo call_user_func('_'.$_GET['fn']);
		exit();
	} 
}

// global maintenance actions
if ($_GET['actions']!='no'){ 
	
	// clear sessions if we are not in News/Projects/events section
	if(!preg_match('/\/(news|projects|events)\//',$_SERVER['REQUEST_URI'])){
		_emptySessions();
	}
	
}
?>