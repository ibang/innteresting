<?php 
// -- Initialize Section, Subsection, Subsubsection, s1..s10 Section Array ----
function initSectionArray(){
	for($i=1;$i<=10;$i++){
		global ${"s$i"};
	}
	$section_array = array();
	for($i=1;$i<=10;$i++){
		if (!empty(${"s$i"})) {
			$section_array[] = ${"s$i"};
		}
	}
	return $section_array;
}

// -- Add STRING to LOG File
if (!function_exists('addToLog')){
	function addToLog($errorStr='', $masInfoStr='', $errorNumber=3){
		if (LOGFILE!='' AND CANLOG=='1'){
			error_log( date('Y-m-d H:i:s').' - '.$errorStr . " - ".$masInfoStr."\n", $errorNumber, LOGFILE);
		}
		//echo '<pre>'.$errorStr.'</pre>';
	}
}

// -- Extract Year from date formated string
function getYear($date){
	$date = DateTime::createFromFormat("Y-m-d", $date);
	return $date->format("Y");
}

//----- Export to CSV ------------------------------------------------------------------

//--- UTF8 separados por punto y coma enclosure doble quote
function exportCSV($header_array, $data_array, $file='export.csv'){
	$delimiter = ";";
	if (!empty($data_array)){
		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename='.$file);

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		
		//UTF8 BOM para que excel lo pueda leer
		$text="\xEF\xBB\xBF"; 
		fputs($output, $text); 
		//fin BOM
		
		// output the column headings
		fputcsv($output, $header_array, $delimiter);
		
		foreach($data_array as $row){
			fputcsv($output, $row, $delimiter);
		}
		exit();
		return true;
	}
	return false;
}

// -- Convert UTF-8 Coded String To UTF-16LE
function codificarUTF16LE($string=''){
	return mb_convert_encoding($string, 'UTF-16LE', 'UTF-8');
}

// To open csv correctly with excel in MAC and WINDOWS, use Tab as separator and UTF-16L, doble quote enclosure
function exportCSV_UTF16LE($header_array, $data_array, $file='export.csv', $save_file_path=''){
	$delimiter = "\t";
	$newrow = "\n";
	if (!empty($data_array)){
		if (!$save_file_path){
			header ( 'HTTP/1.1 200 OK' );
			header ( 'Date: ' . date ( 'D M j G:i:s T Y' ) );
			header ( 'Last-Modified: ' . date ( 'D M j G:i:s T Y' ) );
			header ( 'Content-Type: application/vnd.ms-excel') ;
			header ( 'Content-Disposition: attachment;filename='.$file );

			
			//UTF16 BOM para que excel lo pueda leer
			print chr(255).chr(254); 
			//fin BOM
			
			
			
			// output the column headings
			$output = '';
			foreach($header_array as $field){
				$output.='"'.str_replace('"', '""',$field).'"'.$delimiter;
			}
			$output.=$newrow;
			
			foreach($data_array as $row){
				foreach ($row as $field){
					$output.='"'.str_replace('"', '""',$field).'"'.$delimiter;
				}
				$output.=$newrow;
			}
			print codificarUTF16LE($output);
			exit();
			return true;
		}
		else{
			$myfile = fopen(rtrim($save_file_path,"/")."/".$file, "w") or die("Unable to open file ".rtrim($save_file_path,"/")."/".$file."!");
			fwrite($myfile, chr(255).chr(254));
			
			// output the column headings
			$output = '';
			foreach($header_array as $field){
				$output.='"'.str_replace('"', '""',$field).'"'.$delimiter;
			}
			$output.=$newrow;
			
			foreach($data_array as $row){
				foreach ($row as $field){
					$output.='"'.str_replace('"', '""',$field).'"'.$delimiter;
				}
				$output.=$newrow;
			}
			$output = codificarUTF16LE($output);
			fwrite($myfile, $output);
			fclose($myfile);
			return true;
		}
	}
	return false;
}
//----------- END Export to CSV ---------------------------------------------------------------------------------------
/**
 * Microtime in float
 */
function microtime_string(){
    list($usec, $sec) = explode(" ", microtime());
	// 1000000  pow(10,6);
	$precision = precision((float)$usec + (float)$sec);
	if ($precision>6){
		$precision = 6;
	}
	$multiplicador = pow(10, $precision); // 1000000 
    return (int)(((float)$usec + (float)$sec)*$multiplicador);
}

//----------- Depende del Sistema Operativo, windows menos precisión que linux ------
function precision($num) { 
    return strlen(substr($num, strpos($num, '.')+1)); 
} 

if (!function_exists('microtime_dif')){
	function microtime_dif(&$startTime='', $endTime=''){
		$time_start = explode(' ', $startTime);
		$endTime = ($endTime?$endTime:microtime());
		$time_end = explode(' ', $endTime );
		$parse_time = number_format(($time_end[1] + $time_end[0] - ($time_start[1] + $time_start[0])), 3);
		if (!$startTime){
			$parse_time = '0';
		}
		$startTime = $endTime;
		return $parse_time;
	}
}

//----------- AJAX/MAINTENANCE DELETE SESSIONS ---------------------
function _emptySessions(){
	global $db, $DOC_WEB_ROOT, $_SESSION;
	$session_id = $_SESSION['id'];
	if (!empty($session_id) AND !empty($_SESSION[$session_id])){
		$langs=$db->get_cms_active_langs();
		foreach ($langs as $current_lang){
			if (!empty($_SESSION[$session_id][$current_lang['id']]['images'])){
				foreach($_SESSION[$session_id][$current_lang['id']]['images'] as $current_image){
					if ($current_image['status']=='A' OR $current_image['status']=='M'){
						@unlink($DOC_WEB_ROOT."uploads" . DIRECTORY_SEPARATOR . "tmp". DIRECTORY_SEPARATOR .$current_image['image']);
					}
				}
			}
			if (!empty($_SESSION[$session_id][$current_lang['id']]['files'])){
				foreach($_SESSION[$session_id][$current_lang['id']]['files'] as $current_file){
					if ($current_file['status']=='A' OR $current_file['status']=='M'){
						@unlink($DOC_WEB_ROOT."uploads" . DIRECTORY_SEPARATOR . "tmp". DIRECTORY_SEPARATOR .$current_file['file']);
					}
				}
			}
		}
	}
	if (!empty($session_id)){
		unset($_SESSION[$session_id]);
	}
	unset($_SESSION['id']);
	//session_write_close();
	$resultado['ok']='ok';
	$result = json_encode($result);
	return $result;
}

//http redirects with error code
function redirectTo($url, $errorCode='200'){
	header("Location:".$url);
	exit();
}

//user logged function
function logged(){
	global $db, $DOC_WEB_ROOT, $_SESSION;
	if (!empty($_SESSION['user']) AND !empty($_SESSION['user']['email']) AND !empty($_SESSION['user']['password'])){
		if ($user = $db->get_login_user($_SESSION['user']['email'], $_SESSION['user']['password'])){
			$_SESSION['user'] = array_merge($_SESSION['user'], $user);
			return true;
		}
	}
	return false;
}

//check if user has permissions
function checkPermissions($section){
	global $db, $DOC_WEB_ROOT, $URL_ROOT, $_SESSION;
	if (!empty($_SESSION['user']['perm_'.$section])){
		if ($_SESSION['user']['perm_'.$section]=='Y'){
			return true;
		}
	}
	session_write_close();
	header("Location:".$URL_ROOT.'site/');
	exit();
}

// geoIP localization - get Country
function getIPcountryIsoCode(){
	global $DOC_ROOT;
	//// GEO IP
	require_once($DOC_ROOT.'php/geoip2/geoip.inc');
	
	$ip = $_SERVER['REMOTE_ADDR'];
	//$ip = "150.125.20.10";
	//$ip = "5.154.40.138";
	addToLog("GEO IP DETECTION: CURRENT IP ".$ip);
	$es_ipv6=false;
	$es_ipv4=false;
	if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) === false) {
		$es_ipv6=true;
		//addToLog("GEO IP DETECTION: IS IPV6 IP");
	} elseif (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) === false) {
		$es_ipv4=true;
		//addToLog("GEO IP DETECTION: IS IPV4 IP");
	}
	if ($es_ipv6 OR $es_ipv4){
		$addressFound = false;
		if ($es_ipv6 ){
			$gi = geoip_open($DOC_ROOT.'php/geoip2/Database/'.'GeoIPv6.dat', GEOIP_MEMORY_CACHE);
			//$gi = geoip_open('GeoLiteCityv6.dat', GEOIP_MEMORY_CACHE);
		}
		else{
			$gi = geoip_open($DOC_ROOT.'php/geoip2/Database/'.'GeoIP.dat', GEOIP_MEMORY_CACHE);
			//$gi = geoip_open('GeoLiteCity.dat', GEOIP_MEMORY_CACHE);
		}
		try{
			//$record = geoip_record_by_addr($gi, $ip);
			$country['code'] = geoip_country_code_by_addr($gi, $ip);
			$country['name'] = geoip_country_name_by_addr($gi, $ip);
			addToLog("GEO IP DETECTION: RECORD ".print_r($country, true));
			geoip_close($gi);
			$addressFound = true;
		} catch (Exception $e) {
			//echo $e->errorMessage(); //Pretty error messages from PHPMailer
			addToLog("GEO IP DETECTION: EXCEPTION ".print_r($e, true));
		}
		if ($addressFound){
			addToLog("GEO IP DETECTION: COUNTRY ISO CODE ".$country['code'].' ('.$country['name'].')');
			//addToLog("GEO IP DETECTION: COUNTRY NAME ".$country['name']);
			return strtolower($country['code']);
		}
		else{
			addToLog("GEO IP DETECTION: NOT DATABASE IP");
			return false;
		}
	}
	else{
		addToLog("GEO IP DETECTION: NOT VALID IP (NOT IPV6 AND NOT IPV4)");
	}
	return false;
}

function url_slug($str, $options = array()) {
	// Make sure string is in UTF-8 and strip invalid UTF-8 characters
	$str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());
	$defaults = array(
	'delimiter' => '-',
	'limit' => null,
	'lowercase' => true,
	'replacements' => array(),
	'transliterate' => false,
	);
	// Merge options
	$options = array_merge($defaults, $options);
	$char_map = array(
	 
	// Russian
	'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
	'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
	'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
	'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',
	'Я' => 'Ya',
	'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
	'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
	'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
	'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
	'я' => 'ya',
	
	// Latin
	'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
	'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
	'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
	'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
	'ß' => 'ss',
	'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
	'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
	'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
	'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
	'ÿ' => 'y',
	
	// Latin symbols
	'©' => '(c)',
		
	// Turquish
	'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',
	'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g', 
	
	// Greek
	'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',
	'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',
	'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',
	'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',
	'Ϋ' => 'Y',
	'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',
	'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',
	'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',
	'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',
	'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',
	
	// Ukrainian
	'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',
	'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',
	
	// Czech
	'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U', 
	'Ž' => 'Z', 
	'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
	'ž' => 'z', 
	
	// Polish
	'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z', 
	'Ż' => 'Z', 
	'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
	'ż' => 'z',
	
	// Latvian
	'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N', 
	'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',
	'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',
	'š' => 's', 'ū' => 'u', 'ž' => 'z',
	
	);
	// Make custom replacements
	$str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);
	// Transliterate characters to ASCII
	if ($options['transliterate']) {
	$str = str_replace(array_keys($char_map), $char_map, $str);
	}
	// Replace non-alphanumeric characters with our delimiter
	$str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);
	// Remove duplicate delimiters
	$str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);
	// Truncate slug to max. characters
	$str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');
	// Remove delimiter from ends
	$str = trim($str, $options['delimiter']);
	//echo $str;
	return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
}

function inLanguageDomain(){
	$languageDomains = Constants::getLanguageDomains();
	if (!empty($languageDomains[$_SERVER['HTTP_HOST']])){
		return true;
	}
	return false;
}

function getLanguageDomain(){
	$languageDomains = Constants::getLanguageDomains();
	return $languageDomains[$_SERVER['HTTP_HOST']];
}

// store the las two visited pages
function addToPath($page='', $pathArray=''){
	if ($page){
		if(empty($pathArray)){
			$pathArray[0] = $page;
		}
		else{
			//add to beggin
			array_unshift($pathArray, $page);
		}
		// return only the two first values
		return array_slice($pathArray, 0, 2);
	}
	return $pathArray;
}
//------- AJAX DELETE A TINYMCE JBIMAGE PLUGIN UPLOADED IMAGE -----//
function _removeTinymceImage(){
	global $db, $DOC_WEB_ROOT;
	addToLog('AÑADE PARA BORRAR REMOVETINYMCEIMAGE: '.print_r($_SERVER,true));
	if (!empty($_GET['src'])){
		$image_src_path = str_replace(HTTPHOST , '', $_GET['src']);
		@unlink($image_src_path);
	}
	$resultado['ok']='ok';
	$result = json_encode($result);
	return $result;
}

function isVideoLink($url=''){
	if (empty($url)){
		return false;
	}
	if (preg_match('#^((http://)|(https://)){0,1}(www\.){0,1}(youtu\.be|youtube\.com|y2u\.be/)#', $url)){
		//youtube
		return 'youtube';
	}
	elseif (preg_match('#^((http://)|(https://)){0,1}((www\.)|(player\.)){0,1}(vimeo)#', $url)){
		//youtube
		return 'vimeo';
	}
	return false;
}

// return a random string value
function randVal(){
	//return rand();
	return uniqid('', true);
}

function isPoModified($dirName) {
	//addToLog('ISPOMODIFIED START');
	//addToLog('Dir: '.$dirName);
    $d = dir($dirName);
    $lastModified = 0;
	$currentModified = -1;
	$lastModifiedFile = '';
    while($entry = $d->read()) {
		//addToLog('Current File: '.$entry);
        if ($entry != "." && $entry != "..") {
			//addToLog('Is file or directory');
            if (!is_dir($dirName."/".$entry) AND preg_match('/^.*\.(po|php)$/i', $entry)) {
				//addToLog('Is file and is po or php');
                $currentModified = filemtime($dirName."/".$entry);
				//addToLog('Modified in: '.$currentModified);
            } 
            if ($currentModified > $lastModified){
				//addToLog('Is Newer');
                $lastModified = $currentModified;
				$lastModifiedFile = $entry;
            }
        }
		//addToLog('End File Treat.');
    }
    $d->close();
	//addToLog("Last Modified File is: ".$lastModifiedFile);
	if (preg_match('/\.po$/', $lastModifiedFile)){
		//addToLog('Is PO');
		return true;
	}
	//addToLog('No Is PO');
    return false;
}

//compare file extension of file+path
function is_file_extension($filename='', $ext=''){
	if (!empty($filename) AND !empty($ext)){
		$info = pathinfo($filename);
		if (strtolower($info['extension'])==strtolower($ext)){
			return true;
		}
	}
	return false;
}
?>