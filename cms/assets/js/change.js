function emptySessions() {
	// load provinces with ajax or from cache
	var result = $.getJSON("add.php?fn=emptySessions", '', "json").promise();
	
	result.done(function(data) {
	  // ok
	});
}

var dirty = false;
$(document).ready(function(){
	$(":input").change(function() {
	    //alert("form changed!")
	    dirty = true;
	});
	window.onbeforeunload = function() {
	    if (dirty) {
			//emptySessions();
	        return 'No has guardado los cambios.';
	    }
	};
	$(':submit').click(function(){
	window.onbeforeunload = false;
	});
	
	$('.alert').click(function(){
	window.onbeforeunload = false;
	});
	
	window.setTimeout(function() {
	  $(".alert-success, .alert-warning").fadeTo(400, 0).slideUp(200, function(){
	      $(this).remove();
	  });
	}, 1500);
});