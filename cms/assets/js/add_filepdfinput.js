$(document).ready(function(){
	// initialize with defaults
	$("pre.filepdfinfo").hide();
	var filepdfinputoptions = {
		'browseClass': 'selector',
		'browseIcon': '',
		'dropZoneEnabled': false,
		'uploadUrl': 'ajax.php',
		'deleteUrl': 'ajax.php',
		'clearFunction': function(){
			var self = this; 
			bootbox.dialog({
			title: "Borrar Documento",
			message: "¿Seguro que quieres borrar este documento?",
			onEscape: function() {},
			show: true,
			backdrop: true,
			closeButton: false,
			animate: true,
			className: "remove-file-modal",
			buttons: {
				cancel: {
				  label: "Cancelar",
				  className: "btn-default",
				  callback: function() {}
				},
				success: {   
				  label: "Confirmar",
				  className: "btn-success",
				  callback: function() { self.clear();}
				},
			  }
			
			});
		}, 
		'clearAjaxFunction': function(settings){
			bootbox.dialog({
			title: "Borrar Documento",
			message: "¿Seguro que quieres borrar este documento?",
			onEscape: function() {},
			show: true,
			backdrop: true,
			closeButton: false,
			animate: true,
			className: "remove-file-modal",
			buttons: {
				cancel: {
				  label: "Cancelar",
				  className: "btn-default",
				  callback: function() {}
				},
				success: {   
				  label: "Confirmar",
				  className: "btn-success",
				  callback: function() {$.ajax(settings);}
				},
				
				
			  }
			
		});
		},
		'maxFileCount': 1,
		'maxFileSize': $("pre.maxfilesize").first().text(),
		'showRemove': false,
		'showUpload': false, 
		'showCaption': false, 
		'showCancel': false, 
		'language':'es', 
		'allowedFileTypes':['pdf'], 
		'allowedFileExtensions':['pdf'], 
		'previewSettings':{
			'pdf': {
				'width': "100%", 
				'height': "auto"
			}
		}, 
		'layoutTemplates':{
			'footer':'<div class="file-thumbnail-footer">\n' +
            '    {actions}\n' +
            '</div>',
			'actions':'<div class="file-actions">\n' +
            '    <div class="file-footer-buttons">\n' +
            '       {delete}{other}' +
            '    </div>\n' +
            '    <div class="clearfix"></div>\n' +
            '</div>',
			'preview': '<p class="info">'+$("pre.infofile").first().text()+'</p><div class="file-preview {class}">\n' +
            '    <div class="{dropClass}">\n' +
            '    <div class="file-preview-thumbnails">\n' +
            '    </div>\n' +
            '    <div class="clearfix"></div>' +
            '    <div class="file-preview-status text-center text-success"></div>\n' +
            '    <div class="kv-fileinput-error"></div>\n' +
            '    </div>\n' +
            '</div>', 
			'main2':'{browse}\n{preview}\n{upload}\n'
		}, 
		'removeClass': 'btn btn-warning',
		'fileActionSettings':{'removeIcon': 'Borrar documento','removeClass': 'btn btn-default'}
	};		
	
	// Create a fileinput element with initialized image (preview)
	$("input[type=file].fileauto").not(".nofile").each(function(){
		var theForm = $(this).closest('form');
		var theFile = $(this);
		var theKey = theFile.attr('id');
		var theValue = theFile.attr('value');
		var fileinputoptionsespecifico = jQuery.extend(true, {}, filepdfinputoptions);
		// existing image data
		
		if (theValue!=''){
			fileinputoptionsespecifico.initialPreview = ["<a target='_blank' href='"+theFile.data('urlwebroot')+"uploads/"+theFile.attr('value')+"' class='file-preview-file'  style=\"width:100%;height:auto\" >"+theFile.data('name')+"</a>"];

			// options specific for existing image
			fileinputoptionsespecifico.initialPreviewShowDelete = true;
			fileinputoptionsespecifico.initialPreviewConfig = [{'caption': theFile.attr('value'), 'url':'ajax.php', 'width':'100%', 'height':'auto', 'key': theKey, 'extra': function(){
					return {
						code: theForm.find("input#code").val(),
						fn: 'fileDelete',
						session_id: theForm.find("input#session_id").val(),
						type:'file',
						file: theFile.attr('value'),
						lang: theForm.find("input#lang").val(),
						id: theFile.attr('id')
					};
				}
			}];
		}
		fileinputoptionsespecifico.deleteExtraData = function() {
			return {
                code: theForm.find("input#code").val(),
                fn: 'fileDelete',
				session_id: theForm.find("input#session_id").val(),
				type:'file',
				file: theFile.attr('value'),
				lang: theForm.find("input#lang").val(),
				id: theFile.attr('id')
            };
		};
		
		fileinputoptionsespecifico.uploadExtraData = function() {
            return {
                code: theForm.find('input#code').val(),
                fn: 'fileUpload',
				session_id: theForm.find("input#session_id").val(),
				type:'file',
				lang: theForm.find("input#lang").val(),
				id: theFile.attr('id')
            };
        };
		
		//create existing image		
		$(this).fileinput(fileinputoptionsespecifico).on("filebatchselected", function(event, files) {
			// trigger upload method immediately after files are selected
			$(this).fileinput("upload");
		}).on("fileuploaded", function(event, data, previewId, index){ //successfully uploaded
			dirty = true;
			var key = data.extra.id;
			theForm.find("input#link-"+key).attr("required", "Yes");
			//$('.progress-bar-success').fadeOut(1000);
			theForm.find(".kv-upload-progress").hide();
		}).on("filedeleted", function(event, key){
			theForm.find("input#link-"+key).val('').removeAttr('required');
			dirty = true; // -- form modified show alert on page exit --
		}).on("fileuploaderror", function(event, data, msg){
			theForm.find("#"+data.id+".file-preview-frame").hide();
			dirty = true; // ha habido una modificación en el formulario, lo indicamos para que se muestre la alerta en caso de que el usuario intente ir a otra página sin pulsar submit
		});
		
	});
	
	// Create fileinput empty(no preview, without image) element 
	$("input[type=file].fileauto.nofile").each(function(){
		var theFile = $(this);
		var theForm = $(this).closest('form');
		// Deep copy (copy object de manera independiente)
		var fileinputoptionsespecifico = jQuery.extend(true, {}, filepdfinputoptions);
		
		fileinputoptionsespecifico.deleteExtraData = function() {
			return {
				code: theForm.find("input#code").val(),
				fn: 'fileDelete',
				session_id: theForm.find("input#session_id").val(),
				type:'file',
				file: theFile.attr('value'),
				lang: theForm.find("input#lang").val(),
				id: theFile.attr('id')
			};
		};
		
		fileinputoptionsespecifico.uploadExtraData = function() {
			return {
				code: theForm.find('input#code').val(),
				fn: 'fileUpload',
				session_id: theForm.find("input#session_id").val(),
				type:'file',
				lang: theForm.find("input#lang").val(),
				id: theFile.attr('id')
			};
		};
		
		theFile.fileinput(fileinputoptionsespecifico).on("filebatchselected", function(event, files) {
			// trigger upload method immediately after files are selected
			$(this).fileinput("upload");
		}).on("fileuploaded", function(event, data, previewId, index){ //successfully uploaded
			 dirty = true;
			 var key = data.extra.id;
			 theForm.find("input#link-"+key).attr("required", "Yes");
			//$('.progress-bar-success').fadeOut(1000);
			theForm.find(".kv-upload-progress").hide();
		}).on("filedeleted", function(event, key){
			theForm.find("input#link-"+key).val('').removeAttr('required');
			dirty = true;
		}).on("fileuploaderror", function(event, data, msg){
				theForm.find("#"+data.id+".file-preview-frame").hide();
				dirty = true; // ha habido una modificación en el formulario, lo indicamos para que se muestre la alerta en caso de que el usuario intente ir a otra página sin pulsar submit
		});
	});
	
		
	// Create new image/file object when add new image button is clicked
	$(".js-addfilepdfupload").click( function (e) {
        e.preventDefault(e);
		
		var theForm = $(this).closest('form');
		var theFormLang = theForm.find("input#lang").val();
		// Create unique ID for the element
		var elid = new Date().getTime();
		// Assign the id and call the plugin
		$("#uploadFileList"+theFormLang+".js-filepdfuploads").append("<div class=\"form-group file\" id=\"form-group-filepdf-" + elid + "\"><label for=\"js-filepdf-" + elid + "\" class=\"gallerynextimage\">Documento</label><input type=\"file\" class=\"fileauto\" id=\"js-filepdf-" + elid + "\" name=\"fileattach[]\" accept=\"application/pdf\"></div><div class=\"form-group\" id=\"form-group-link-" + elid + "\"><label for=\"link-js-filepdf-" + elid + "\">Texto del enlace </label><input type=\"text\" class=\"form-control\" name=\"link-js-filepdf-" + elid + "\" id=\"link-js-filepdf-" + elid + "\" placeholder=\"Texto del enlace\" title=\"Texto del enlace\" value=\"\" /></div>");
		
		
		
		// Create fileinput element
		$("#js-filepdf-" + elid).each(function(){
			var theFile = $(this);
			// Deep copy (copy object de manera independiente)
			var fileinputoptionsespecifico = jQuery.extend(true, {}, filepdfinputoptions);
			
			fileinputoptionsespecifico.deleteExtraData = function() {
				return {
					code: theForm.find("input#code").val(),
					fn: 'fileDelete',
					session_id: theForm.find("input#session_id").val(),
					type:'file',
					file: theFile.attr('value'),
					lang: theForm.find("input#lang").val(),
					id: theFile.attr('id')
				};
			};
			
			fileinputoptionsespecifico.uploadExtraData = function() {
				return {
					code: theForm.find('input#code').val(),
					fn: 'fileUpload',
					session_id: theForm.find("input#session_id").val(),
					type:'file',
					lang: theForm.find("input#lang").val(),
					id: theFile.attr('id')
				};
			};
			
			$("#js-filepdf-" + elid).fileinput(fileinputoptionsespecifico).on("filebatchselected", function(event, files) {
				// trigger upload method immediately after files are selected
				$(this).fileinput("upload");
			}).on("fileuploaded", function(event, data, previewId, index){ //successfully uploaded
				 dirty = true;
				 var key = data.extra.id;
				 theForm.find("input#link-"+key).attr("required", "Yes");
				//$('.progress-bar-success').fadeOut(1000);
				theForm.find(".kv-upload-progress").hide();
			}).on("filedeleted", function(event, key){
				theForm.find("input#link-"+key).val('').removeAttr('required');
				dirty = true;
			}).on("fileuploaderror", function(event, data, msg){ // tamaño no permitido, y demás anteriores a mandar al ajax
				theForm.find("#"+data.id+".file-preview-frame").hide();
				dirty = true; // ha habido una modificación en el formulario, lo indicamos para que se muestre la alerta en caso de que el usuario intente ir a otra página sin pulsar submit
			});
		});
        return false;
    });
	
});